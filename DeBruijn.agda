{-# OPTIONS --without-K #-}

open import Relation.Nullary
open import Relation.Binary
open import Relation.Binary.PropositionalEquality hiding (Extensionality)

open import Data.Nat hiding (_⊔_)
open import Data.Nat.Properties 
open import Data.Product 
open import Data.Empty
open import Data.List
open import Data.List.Properties using (≡-dec; ∷-injectiveʳ)
open import Data.Maybe


open import Axiom.UniquenessOfIdentityProofs 
open import Ty 

checkEqEnv : (t1 : List Ty) -> (t2 : List Ty) -> Dec (t1 ≡ t2) 
checkEqEnv = Data.List.Properties.≡-dec checkEqTy 

checkEqEnv-diag : {ts1 ts2 : List Ty} -> (eq : ts1 ≡ ts2) -> checkEqEnv ts1 ts2 ≡ yes eq
checkEqEnv-diag = ≡-≟-identity checkEqEnv



data _∈_ : Ty -> List Ty -> Set where 
  Z : ∀ {a env}   -> a ∈ (a ∷ env)
  S : ∀ {a b env} -> a ∈ env -> a ∈ (b ∷ env)



data _≲_ : List Ty -> List Ty -> Set where 
  ≲-refl : ∀ {env} -> env ≲ env
  ≲-step : ∀ {env env' b} -> env ≲ env' -> env ≲ (b ∷ env') 


≲-weaken : ∀ {b env env'} -> (b ∷ env) ≲ env' -> env ≲ env' 
≲-weaken (≲-refl) = ≲-step ≲-refl
≲-weaken (≲-step wit) = ≲-step (≲-weaken wit) 

≲-trans : ∀ {env env' env''} -> env ≲ env' -> env' ≲ env'' -> env ≲ env''
≲-trans ≲-refl wit2 = wit2
≲-trans (≲-step wit1) ≲-refl        = ≲-step wit1
≲-trans (≲-step wit1) (≲-step wit2) = ≲-step (≲-trans (≲-step wit1) wit2) 


diffEnv : (w1 : List Ty) -> (w2 : List Ty) -> Maybe (w1 ≲ w2) 
diffEnv w1 w2 with checkEqEnv w1 w2 
diffEnv w1 .w1 | yes refl = just ≲-refl 
... | no  _ with w2 
diffEnv [] _ | no p | [] = ⊥-elim (p refl)
diffEnv (_ ∷ _) _ | no _ | [] = nothing
diffEnv w1 _ | no _ | _ ∷ w2' = diffEnv w1 w2' >>= λ rel -> just (≲-step rel)

diffEnv-≲ : ∀ w1 w2 {w} -> diffEnv w1 w2 ≡ just w -> w1 ≲ w2 
diffEnv-≲ w1 w2 diff-ok with checkEqEnv w1 w2 
diffEnv-≲ w1 _  diff-ok | yes refl = ≲-refl
diffEnv-≲ w1 w2 diff-ok | no ¬p with w2 
diffEnv-≲ [] w2 diff-ok | no ¬p | [] = ≲-refl 
... | _ ∷ w2' with diffEnv w1 w2' | inspect (diffEnv w1) w2' 
... | just r | [ diff-ok' ] with diffEnv-≲ w1 w2' diff-ok' 
... | w1≲w2' = ≲-trans w1≲w2' (≲-step ≲-refl) 


≲-diff : ∀ w1 w2 -> w1 ≲ w2 -> ∃ λ w -> diffEnv w1 w2 ≡ just w 
≲-diff w1 w2 wk with checkEqEnv w1 w2 
≲-diff w1 .w1 wk | yes refl = ≲-refl , refl
≲-diff w1 w2 wk | no ¬p with w2 
≲-diff [] w2 wk | no ¬p | [] = wk , (⊥-elim (¬p refl))
≲-diff .(_ ∷ w2') w2 ≲-refl | no ¬p | _ ∷ w2' = ⊥-elim (¬p refl)
≲-diff w1 w2 (≲-step wk) | no ¬p | _ ∷ w2' with ≲-diff w1 w2' wk
... | w , w-ok with diffEnv w1 w2' 
≲-diff w1 w2 (≲-step wk) | no ¬p | _ ∷ w2' | w , refl | .(just w) = ≲-step w , refl 

_≲?_ : (w1 : List Ty) -> (w2 : List Ty) -> Dec (w1 ≲ w2) 
w1 ≲? w2 with diffEnv w1 w2 | inspect (diffEnv w1) w2
... | just wit | _  = yes wit 
... | nothing  | [ eq ] = no (λ p ->   cntr (proj₂ (≲-diff w1 w2 p)) eq  )
  where
    cntr : ∀ {A : Set} {a : Maybe A} {b : A} -> a ≡ just b -> a ≡ nothing -> ⊥ 
    cntr refl ()



≲-length : ∀ w1 w2 -> w1 ≲ w2 -> length w1 ≤ length w2 
≲-length _ _ ≲-refl = ≤-refl
≲-length _ (_ ∷ w2') (≲-step wk) with ≲-length _ _ wk 
... | p = ≤-trans p ( Data.Nat.Properties.n≤1+n (length w2')) 


mycong : ∀ {ℓ'} {P : Set} -> {p q : P} -> p ≡ q -> (f : P -> Set ℓ') -> (a : f p) -> f q
mycong refl _ x = x


module _ where
  private 
    lemma : ∀ w w' -> (eq : w ≡ w') -> (wk : w ≲ w') -> mycong eq (λ v → v ≲ w') wk ≡ ≲-refl 
    lemma [] _ refl ≲-refl = refl
    lemma (x ∷ w) _ eq ≲-refl with Decidable⇒UIP.≡-irrelevant checkEqEnv eq refl 
    lemma (x ∷ w) .(x ∷ w) .refl ≲-refl | refl = refl

    lemma (x ∷ w) _ refl (≲-step wk) = ⊥-elim (1+n≰n (≲-length _ _ wk))

  ≲-canon : ∀ w1 w2 -> (wk1 wk2 : w1 ≲ w2) -> wk1 ≡ wk2 
  ≲-canon w1 .w1 ≲-refl wk2 with lemma _ _ refl wk2 
  ... | res = sym res
  ≲-canon .(_ ∷ _) .(_ ∷ _) (≲-step wk1) ≲-refl with ≲-length _ _ wk1 
  ... | p =  ⊥-elim (1+n≰n p)
  ≲-canon w1 .(_ ∷ _) (≲-step wk1) (≲-step wk2) rewrite ≲-canon _ _ wk1 wk2 = refl 

≲-is-preorder : IsPreorder (_≡_) (_≲_)
≲-is-preorder = record { isEquivalence =  isEquivalence ; reflexive = λ { refl -> ≲-refl } ; trans = ≲-trans } 

shift : ∀ {a env env'} -> a ∈ env -> env ≲ env' -> a ∈ env'
shift var ≲-refl = var
shift var (≲-step wit) = S (shift var wit) 

data DTerm  : (Γ : List Ty) -> Ty -> Set where 
  Var : ∀ { Γ τ } -> τ ∈ Γ -> DTerm Γ τ 
  App : ∀ { Γ σ τ } -> DTerm Γ (σ ~> τ) -> DTerm Γ σ -> DTerm Γ τ
  Abs : ∀ { Γ σ τ } -> DTerm (σ ∷ Γ) τ -> DTerm Γ (σ ~> τ)
