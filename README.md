What is this?
=============

This is an Agda implementation of the interconversion between polymorphic function representations (aka the build form or finally-tagless style) of a simply-typed &lambda;-calculus and de Bruijn term representations of it, proposed by [Robert Atkey "Syntax for Free: Representing Syntax with Binding Using Parametricity", TLCA 2009](https://doi.org/10.1007/978-3-642-02273-9_5).

Atkey's original work is already mechanized in Coq, and thus there is little technical novelty in this Agda implementation. My primal motivation for this implementation is mainly for my exercise of Agda programming. Another important motivation is to understand the interconversion proposed in the paper well by proving its correctness. Since it seems to me that the link to his implementation in the above-mentioned paper is now dead, it might be a good chance to do so. 

Highlights 
----------

There are some differences between his paper (NB: I failed to get his Coq implementation) and this implementation. 

  - I didn't implemented System F and postulated its parametricity. 
  - This implementation targets a simply-typed &lambda;-calculus, instead of untyped ones. I believe that types would be a good guide for implementation. 
  - `--type-in-type` is not used, because of postulated free-theorems. 

The following points may be worth noting.

  - The extensional equality of functions is postulated (as the original). 
  - For several places, I am required to use the uniqueness of a proof of `Γ ≾ Γ'`. Maybe, I could make use of proof irrelevance (i.e., dot annotation and `Prop`), but I am not sure. 
  - I am a Agda newbie, so there must be better ways to write proofs. 

Note
----

  - Currently, the repository also contains `p.agda` which proves the correspondence between PHOAS (`PHOAS.agda`) and the polymorphic encoding (`Poly.agda`).
  
