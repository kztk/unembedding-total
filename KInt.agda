{-# OPTIONS --without-K #-}

open import Level
open import Relation.Binary

module KInt (Carrier : Set) (_≤'_ : Carrier -> Carrier -> Set) (trans : Transitive _≤'_ ) where 
  open import Ty

  -- TODO: Remove levels or remove --type-in-type

  -- Kripke logical relations
  -- open Preorder


  -- _≤'_ : Carrier -> Carrier W -> Set 
  -- _≤'_ = Preorder._∼_ W

  record KRel {ℓ} (A : Set ℓ) :  Set (Level.suc ℓ) where
    field
      prop : Carrier -> A -> Set ℓ
      prop-kripke : ∀ w -> ∀ w' -> ∀ a -> w ≤' w' -> prop w a -> prop w' a

  record KRel₂ {ℓ} (A₁ : Set ℓ) (A₂ : Set ℓ) :  Set (Level.suc ℓ) where
    field
      prop : Carrier -> A₁ -> A₂ -> Set ℓ
      prop-kripke : ∀ w -> ∀ w' -> ∀ a₁ -> ∀ a₂ -> w ≤' w' -> prop w a₁ a₂ -> prop w' a₁ a₂
    

  _~~>_ : ∀ {ℓ ℓ'} {A B} -> KRel {ℓ} A -> KRel {ℓ'} B -> KRel {ℓ Level.⊔ ℓ'} (A -> B)
  k₁ ~~> k₂ = record { 
     prop = λ w f -> ∀ w' -> w ≤' w' -> ∀ a -> KRel.prop k₁ w' a -> KRel.prop k₂ w' (f a) ; 
     prop-kripke = λ w w' f w≤w' kf w'' w'≤w'' -> kf w'' 
                     (trans w≤w'  w'≤w'')  }

  _~~>₂_ : ∀ {ℓ ℓ'} {A₁ A₂ B₁ B₂} -> KRel₂ {ℓ} A₁ A₂ -> KRel₂ {ℓ'} B₁ B₂ -> KRel₂ {ℓ Level.⊔ ℓ'} (A₁ -> B₁) (A₂ -> B₂)
  k₁ ~~>₂ k₂ = record { 
     prop = λ w f₁ f₂ -> ∀ w' -> w ≤' w' -> ∀ a₁ a₂ -> KRel₂.prop k₁ w' a₁ a₂ -> KRel₂.prop k₂ w' (f₁ a₁) (f₂ a₂) ; 
     prop-kripke = λ w w' f₁ f₂ w≤w' kf w'' w'≤w'' -> kf w'' 
                     (trans w≤w'  w'≤w'')  }
  
  infixr 90 _~~>_
  infixr 90 _~~>₂_



  forallTy : ∀ {ℓ} {P : Ty -> Set ℓ} -> (∀ (A : Ty) -> KRel (P A)) -> KRel (∀ (A : Ty) -> P A)
  forallTy {P} k = record {
    prop = λ w f -> ∀ (A : Ty) -> KRel.prop (k A) w (f A)  ; 
    prop-kripke = λ w w' t w≤w' f A -> KRel.prop-kripke (k A) w w' (t A) w≤w' (f A) } 

  forallTy₂ : ∀{ℓ} {P₁ P₂ : Ty -> Set ℓ} -> (∀ (A : Ty) -> KRel₂ (P₁ A) (P₂ A)) -> KRel₂ (∀ (A : Ty) -> P₁ A) (∀ (A : Ty) -> P₂ A)
  forallTy₂ {P} k = record {
    prop = λ w f₁ f₂ -> ∀ (A : Ty) -> KRel₂.prop (k A) w (f₁ A) (f₂ A) ; 
    prop-kripke = λ w w' t₁ t₂ w≤w' f A -> KRel₂.prop-kripke (k A) w w' (t₁ A) (t₂ A) w≤w' (f A) } 

  forallTy1 : ∀ {ℓ ℓ'} {P : (Ty -> Set ℓ) -> Set ℓ'} -> 
              (∀ {T} -> (∀ A -> KRel (T A)) -> KRel (P T)) -> 
              KRel (∀ (T : Ty -> Set ℓ) -> P T) 
  forallTy1 {P} k = record {
    prop = λ w f -> ∀ {T} -> ∀ r -> KRel.prop (k r) w (f T)   ; 
    prop-kripke = λ w w' t w≤w' f r -> KRel.prop-kripke (k r) w w' (t _) w≤w' (f r)  }  

  forallTy1₂ : ∀{ℓ ℓ'} {P₁ P₂ : (Ty -> Set ℓ) -> Set ℓ'} -> 
               (∀ {T₁ T₂} -> (∀ A -> KRel₂ (T₁ A) (T₂ A)) -> KRel₂  (P₁ T₁) (P₂ T₂)) -> 
               KRel₂ (∀ (T : Ty -> Set ℓ) -> P₁ T) (∀ (T : Ty -> Set ℓ) -> P₂ T) 
  forallTy1₂ {P} k = record {
    prop = λ w f₁ f₂ -> ∀ {T₁ T₂} -> ∀ r -> KRel₂.prop (k r) w (f₁ T₁) (f₂ T₂)  ; 
    prop-kripke = λ w w' t₁ t₂ w≤w' f r -> KRel₂.prop-kripke (k r) w w' (t₁ _) (t₂ _) w≤w' (f r)  }  

    
  lam-term : ∀ {ℓ} (A : Ty) ->  
                 KRel (∀ (T : Ty -> Set ℓ) ->
                         (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                         (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
                         T A )
  lam-term {ℓ} a = forallTy1 λ t -> 
                   (forallTy λ a -> forallTy λ b -> (t a ~~> t b) ~~> (t (a ~> b)))
                   ~~> (forallTy λ a -> forallTy λ b -> (t (a ~> b)) ~~> t a ~~> t b)
                   ~~> (t a)
             

  lam-term₂ : ∀ {ℓ} (A : Ty) ->  
                 KRel₂ (∀ (T : Ty -> Set ℓ) ->
                         (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                         (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
                         T A )
                       (∀ (T : Ty -> Set ℓ) ->
                         (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                         (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
                         T A )
  lam-term₂ {ℓ} a = forallTy1₂ λ t -> 
                          (forallTy₂ λ a -> forallTy₂ λ b -> (t a ~~>₂ t b) ~~>₂ (t (a ~> b)))
                          ~~>₂ (forallTy₂ λ a -> forallTy₂ λ b -> (t (a ~> b)) ~~>₂ t a ~~>₂ t b)
                          ~~>₂ (t a)
