{-# OPTIONS --without-K --type-in-type #-}

open import Level
open import Relation.Binary
open import Relation.Binary.PropositionalEquality
open import Data.Maybe 
open import Data.Product
open import Data.List
open import Relation.Nullary
open import Data.Empty
open import Data.List.Properties using (≡-dec)

open import Function

open import Codata.Thunk 

open import Ty 
import KInt 

module _ where

Poly : ∀ A -> Set 
Poly A =  ∀ (T : Ty -> Set) -> 
          ∀ (M : Set -> Set) -> 
           (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
           (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
           (mark : ∀ A -> T A -> M (T A)) -> 
           (local : ∀ A -> M (T A) -> T A) -> 
           (return : ∀ (R : Set) -> R -> M R) -> 
           (bind : ∀ (R S : Set) -> M R -> (R -> M S) -> M S) -> 
           (mfix : ∀ (R : Set) -> (R -> M R) -> M R) -> 
           T A


module KInt' (Carrier : Set) (_≤'_ : Carrier -> Carrier -> Set) (trans : Transitive _≤'_ ) where 
  open KInt Carrier _≤'_ trans

  forallR : ∀ {ℓ ℓ'}
    {T : Set ℓ -> Set ℓ'} -> 
    (∀ {R} -> KRel R -> KRel (T R)) -> 
    KRel (∀ (τ : Set ℓ) -> T τ)
  forallR k = 
        record { prop = λ w i -> ∀ {τ} -> ∀ (r : KRel τ) ->  KRel.prop (k r) w (i τ) ; 
                 prop-kripke = λ w w' i w≤w' f r -> KRel.prop-kripke (k r) w w' (i _) w≤w' (f r)  } 

  forallR1 : ∀ {ℓ ℓ' ℓ''}
    {T : (Set ℓ -> Set ℓ') -> Set ℓ'' } -> 
    (∀ {M} -> (∀ {R} -> KRel R -> KRel (M R)) -> KRel (T M)) -> 
    KRel (∀ (M : Set ℓ -> Set ℓ') -> T M )
  forallR1  k = 
        record { prop = λ w i -> ∀ {M} -> ∀ (r : ∀ {R} -> KRel R -> KRel (M R)) ->  KRel.prop (k r) w (i M) ; 
                 prop-kripke = λ w w' i w≤w' f r -> KRel.prop-kripke (k r) w w' (i _) w≤w' (f r)  } 

  m-term : ∀{ℓ ℓ'} (A : Ty) -> 
           KRel (∀ (T : Ty -> Set ℓ) -> 
                   (M : Set ℓ -> Set ℓ') -> 
                   (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                   (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
                   (mark : ∀ A -> T A -> M (T A)) -> 
                   (local : ∀ A -> M (T A) -> T A) -> 
                   (return : ∀ R -> R -> M R) -> 
                   (bind : ∀ R S -> M R -> (R -> M S) -> M S) ->
                   (mfix : ∀ R -> (R -> M R) -> M R) -> 
                   T A)
  m-term A = forallTy1 λ t -> 
             forallR1 λ m -> 
             (forallTy λ a -> forallTy λ b -> (t a ~~> t b) ~~> t (a ~> b)) 
             ~~> (forallTy λ a -> forallTy λ b -> t (a ~> b) ~~> t a ~~> t b) 
             ~~> (forallTy λ a -> t a ~~> m (t a))
             ~~> (forallTy λ a -> m (t a) ~~> t a)
             ~~> (forallR λ r -> r ~~> m r)
             ~~> (forallR λ r -> forallR λ s -> m r ~~> (r ~~> m s) ~~> m s)
             ~~> (forallR λ r -> (r ~~> m r) ~~> m r)
             ~~> t A
  
  
module _ where
  data _∈_ : Ty -> List Ty -> Set where 
    Z : ∀ {a env}   -> a ∈ (a ∷ env)
    S : ∀ {a b env} -> a ∈ env -> a ∈ (b ∷ env)



  data _≲_ : List Ty -> List Ty -> Set where 
    ≲-refl : ∀ {env} -> env ≲ env
    ≲-step : ∀ {env env' b} -> env ≲ env' -> env ≲ (b ∷ env') 

  ≲-trans : ∀ {env env' env''} -> env ≲ env' -> env' ≲ env'' -> env ≲ env''
  ≲-trans ≲-refl wit2 = wit2
  ≲-trans (≲-step wit1) ≲-refl        = ≲-step wit1
  ≲-trans (≲-step wit1) (≲-step wit2) = ≲-step (≲-trans (≲-step wit1) wit2) 

  ≲-is-preorder : IsPreorder (_≡_) (_≲_)
  ≲-is-preorder = record { isEquivalence =  isEquivalence ; reflexive = λ { refl -> ≲-refl } ; trans = ≲-trans } 


  
  data M : (A : Set) -> Set 
  data DTerm : (Γ : List Ty) -> Ty -> Set

  {-# NO_POSITIVITY_CHECK #-}
  data M where
    Mark : ∀ {Γ τ} -> M (Maybe (DTerm Γ τ)) -> M (Maybe (DTerm Γ τ))
    Ret  : ∀ {R} -> R -> M R 
    Bind : ∀ {R S} -> M R -> (R -> M S) -> M S 
    MFix : ∀ {R} -> (R -> M R) -> M R

  data DTerm where 
    Var : ∀ { Γ τ } -> τ ∈ Γ -> DTerm Γ τ 
    App : ∀ { Γ σ τ } -> DTerm Γ (σ ~> τ) -> DTerm Γ σ -> DTerm Γ τ
    Abs : ∀ { Γ σ τ } -> DTerm (σ ∷ Γ) τ -> DTerm Γ (σ ~> τ)
    Local : ∀ { Γ τ} -> M (DTerm Γ τ) -> DTerm Γ τ

  checkEqEnv : (t1 : List Ty) -> (t2 : List Ty) -> Dec (t1 ≡ t2) 
  checkEqEnv = Data.List.Properties.≡-dec checkEqTy 


  shift : ∀ {a env env'} -> a ∈ env -> env ≲ env' -> a ∈ env'
  shift var ≲-refl = var
  shift var (≲-step wit) = S (shift var wit) 

  shiftVarGen : ∀ {a env env' env0} -> env ≲ env' -> a ∈ (env0 ++ env) -> a ∈ (env0 ++ env') 
  shiftVarGen {env0 = []} w≲w' x = shift x w≲w'
  shiftVarGen {env0 = _ ∷ env0} w≲w' Z = Z
  shiftVarGen {env0 = _ ∷ env0} w≲w' (S x) = S (shiftVarGen w≲w' x) 

  {-# TERMINATING #-}
  shiftExpGen : ∀ {a env env' env0} -> env ≲ env' -> DTerm (env0 ++ env) a -> DTerm (env0 ++ env') a 
  shiftExpGen w≲w' (Var x) = Var (shiftVarGen w≲w' x)
  shiftExpGen w≲w' (App dt dt₁) = App (shiftExpGen w≲w' dt) (shiftExpGen w≲w' dt₁)
  shiftExpGen {env' = env'} w≲w' (Abs {σ = σ} {τ} dt) = Abs (shiftExpGen w≲w' dt)
  shiftExpGen w≲w' (Local x) = Local (Bind x (λ y -> Ret (shiftExpGen w≲w' y))) 
  
  shiftExp : ∀ {a env env'} -> env ≲ env' -> DTerm env a -> DTerm env' a 
  shiftExp = shiftExpGen {env0 = []} 

  diffEnv : (w1 : List Ty) -> (w2 : List Ty) -> Maybe (w1 ≲ w2) 
  diffEnv w1 w2 with checkEqEnv w1 w2 
  diffEnv w1 .w1 | yes refl = just ≲-refl 
  ... | no  _ with w2 
  diffEnv [] _ | no p | [] = ⊥-elim (p refl)
  diffEnv (_ ∷ _) _ | no _ | [] = nothing
  diffEnv w1 _ | no _ | _ ∷ w2' = diffEnv w1 w2' >>= λ rel -> just (≲-step rel)
  
  ≲-diff : ∀ w1 w2 -> w1 ≲ w2 -> ∃ λ w -> diffEnv w1 w2 ≡ just w 
  ≲-diff w1 w2 wk with checkEqEnv w1 w2 
  ≲-diff w1 .w1 wk | yes refl = ≲-refl , refl
  ≲-diff w1 w2 wk | no ¬p with w2 
  ≲-diff [] w2 wk | no ¬p | [] = wk , (⊥-elim (¬p refl))
  ≲-diff .(_ ∷ w2') w2 ≲-refl | no ¬p | _ ∷ w2' = ⊥-elim (¬p refl)
  ≲-diff w1 w2 (≲-step wk) | no ¬p | _ ∷ w2' with ≲-diff w1 w2' wk
  ... | w , w-ok with diffEnv w1 w2' 
  ≲-diff w1 w2 (≲-step wk) | no ¬p | _ ∷ w2' | w , refl | .(just w) = ≲-step w , refl 

  module _ where
    open import Axiom.UniquenessOfIdentityProofs 
    open import Data.Nat
    open import Data.Nat.Properties

    private 
      ≲-length : ∀ w1 w2 -> w1 ≲ w2 -> length w1 ≤ length w2 
      ≲-length _ _ ≲-refl = ≤-refl
      ≲-length _ (_ ∷ w2') (≲-step wk) with ≲-length _ _ wk 
      ... | p = ≤-trans p ( Data.Nat.Properties.n≤1+n (length w2')) 

      lemma : ∀ w w' -> (eq : w ≡ w') -> (wk : w ≲ w') -> subst (λ v → v ≲ w') eq wk ≡ ≲-refl 
      lemma [] _ refl ≲-refl = refl
      lemma (x ∷ w) _ eq ≲-refl with Decidable⇒UIP.≡-irrelevant checkEqEnv eq refl 
      lemma (x ∷ w) .(x ∷ w) .refl ≲-refl | refl = refl

      lemma (x ∷ w) _ refl (≲-step wk) = ⊥-elim (1+n≰n (≲-length _ _ wk))

    ≲-canon : ∀ w1 w2 -> (wk1 wk2 : w1 ≲ w2) -> wk1 ≡ wk2 
    ≲-canon w1 .w1 ≲-refl wk2 with lemma _ _ refl wk2 
    ... | res = sym res
    ≲-canon .(_ ∷ _) .(_ ∷ _) (≲-step wk1) ≲-refl with ≲-length _ _ wk1 
    ... | p =  ⊥-elim (1+n≰n p)
    ≲-canon w1 .(_ ∷ _) (≲-step wk1) (≲-step wk2) rewrite ≲-canon _ _ wk1 wk2 = refl 



Sem : ∀ A -> Set
Sem A = (w : List Ty) -> M (Maybe (DTerm w A))

module LamSem where 
  var : ∀ A Γ (w : List Ty) -> Maybe (DTerm w A)
  var A Γ = λ Γ' -> diffEnv Γₐ Γ' >>= λ Γₐ≲Γ' -> just (Var (shift Z Γₐ≲Γ'))
    where
      Γₐ = A ∷ Γ 

  lamSem : (A B : Ty) -> (Sem A -> Sem B) -> Sem (A ~> B)
  lamSem A _ f Γ =  
   Bind (f (λ w -> Ret (var A Γ w)) (A ∷ Γ)) λ t -> Ret (t >>= λ tt -> just (Abs tt))

open LamSem using (lamSem)


appSem : (A B : Ty) -> Sem (A ~> B) -> Sem A -> Sem B 
appSem _ _ e1 e2 Γ = 
  Bind (e1 Γ) λ m1 -> 
  Bind (e2 Γ) λ m2 -> 
   Ret (m1 >>= λ t1 -> m2 >>= λ t2 -> just (App t1 t2))
 

MSem : Set -> Set 
MSem R = ∀ (w : List Ty) -> M R

markSem : (A : Ty) -> Sem A -> MSem (Sem A) 
markSem _ e = λ w ->  
   Bind (Mark (e w)) λ m -> 
     Ret (λ w' -> Ret (m >>= λ t -> diffEnv w w' >>= λ w≲w' -> just (shiftExp w≲w' t)))


localSem : (A : Ty) -> MSem (Sem A) -> Sem A 
localSem _ m = λ w -> 
 Bind (m w) λ r -> r w
  
retSem : (R : Set) → R → MSem R
retSem _ x = λ _ -> Ret x -- (just x)

bindSem : (R S : Set) → MSem R → (R → MSem S) → MSem S
bindSem _ _ m k w = 
  Bind (m w) λ r -> k r w

mfixSem : (R : Set) -> (R -> MSem R) -> MSem R 
mfixSem _ f w = MFix (λ r -> f r w)  


module _ where 
  open import Size 

  data Returns (i : Size) : ∀ {A} -> M A -> A -> Set where
    -- returns-mark : ∀ {A Γ} {t : DTerm Γ A} -> Mark t returns t
    returns-mark : 
      ∀ {A Γ} {m} {t : Maybe (DTerm Γ A)} -> (t' : DTerm Γ A) -> 
      Returns i m t -> 
      Returns i (Mark m) (just t')
    returns-ret : ∀ {A} {a : A} -> Returns i (Ret a) a 
    returns-bind : 
      ∀ {A B} {m : M A} {f : A -> M B} (a : A) {b : B} -> 
      Returns i m a -> Returns i (f a) b -> Returns i (Bind m f) b 
    returns-fix : 
      ∀ {A} {f : A -> M A} {a b : A} -> 
      Thunk (λ j -> Returns j (MFix f) a) i -> Returns i (f a) b -> 
      -- Bind (MFix f) f returns a ->
      Returns i (MFix f) b


  data ReturnsOk (i : Size) : ∀ {A} {a : A} {m : M A} -> Returns ∞ m a -> (A -> Set) -> Set where 
    returns-mark-ok : 
      ∀ {A Γ} {m} {t : Maybe (DTerm Γ A)} {t' : (DTerm Γ A)} (r : Returns ∞ m t) {P} ->
      ReturnsOk i r P -> P (just t') -> 
      ReturnsOk i (returns-mark t' r) P 
    returns-ret-ok : 
      ∀ {A} {a : A} {P} -> 
      P a -> 
      ReturnsOk i (returns-ret {a = a}) P
    returns-bind-ok : 
      ∀ {A B} {m : M A} {f : A -> M B} {a : A} {b : B} 
        (m-ret-a : Returns ∞ m a) (fa-ret-b : Returns ∞ (f a) b) {P Q} -> 
      ReturnsOk i m-ret-a Q -> 
      (Q a -> ReturnsOk i fa-ret-b P) -> 
      ReturnsOk i (returns-bind {f = f} a m-ret-a fa-ret-b) P 
    returns-fix-ok : 
      ∀ {A} {f : A -> M A} {a b : A} 
        (thunk : Thunk (λ j -> Returns j (MFix f) a) ∞) 
        (fa-ret-b : Returns ∞ (f a) b) {P} -> 
      Thunk[ j < i ] (ReturnsOk j (force thunk) P) ->
      (P a -> ReturnsOk i fa-ret-b P) -> 
      ReturnsOk i (returns-fix thunk fa-ret-b) P 

convert : ∀ A w -> Poly A -> M (Maybe (DTerm w A))
convert A w h = h _ MSem lamSem appSem markSem localSem retSem bindSem mfixSem w

prove_by_ : (A : Set) -> A -> A 
prove A by x = x 

module _ where 
  open import Size
  open import Codata.Delay 
  -- Prod : {ℓ₁ ℓ₂ ℓ₃ : Level} -> Preorder ℓ₁ ℓ₂ ℓ₃ -> Preorder ℓ₁ ℓ₂ ℓ₃ -> Preorder ℓ₁ ℓ₂ ℓ₃
  -- Prod p1 p2 = record { Carrier = P1.Carrier × P2.Carrier ; 
  --                       _≈_ = λ a b -> P1._≈_ (proj₁ a) (proj₁ b) × P2._≈_ (proj₂ a) (proj₂ b) ; 
  --                       _∼_ = λ a b -> P1._∼_ (proj₁ a) (proj₁ b) × P2._∼_ (proj₂ a) (proj₂ b) ; 
  --                       isPreorder = record { isEquivalence = record { refl =  P1.Eq.refl , P2.Eq.refl ; sym = λ r -> P1.Eq.sym (proj₁ r) , P2.Eq.sym (proj₂ r) ; trans = λ r1 r2 -> P1.Eq.trans (proj₁ r1) (proj₁ r2) , P2.Eq.trans (proj₂ r1) (proj₂ r2) } ; reflexive = λ r -> P1.reflexive (proj₁ r) , P2.reflexive (proj₂ r) ; trans =  λ r1 r2 -> P1.trans (proj₁ r1) (proj₁ r2) , P2.trans (proj₂ r1) (proj₂ r2)  } }
  --      where
  --        module P1 = Preorder p1
  --        module P2 = Preorder p2

  delay : ∀ {A} {i} {j : Size< ↑ i} -> Delay A i -> Delay A j 
  delay (now a) = now a 
  delay (later n) = later λ where .force -> delay (force n)

--  weaken-returns-fix : ∀ {A} {f : A -> M A} {a : A} {i} {j : Size< ↑ i} -> ReturnsFix f a i -> ReturnsFix f a j 

  weaken-returns : ∀ {A} {a : A} {m : M A} {i} {j : Size< ↑ i} -> Returns i m a -> Returns j m a   
  weaken-returns (returns-mark t' r) = returns-mark _ (weaken-returns r)
  weaken-returns returns-ret = returns-ret
  weaken-returns (returns-bind a r r₁) = returns-bind a (weaken-returns r) (weaken-returns r₁)
  weaken-returns (returns-fix x y) = returns-fix (λ { .force -> weaken-returns (force x)} ) (weaken-returns y)

--  weaken-returns-fix (returns-fix-iter b rf r) = returns-fix-iter b (λ { .force -> weaken-returns-fix (force rf) }) (weaken-returns r) 
  
  -- PEnv : Preorder Level.zero Level.zero Level.zero 
  -- PEnv = record { Carrier = List Ty ; _≈_ = _≡_ ; _∼_ = _≲_ ; isPreorder = ≲-is-preorder }

  data SizeDec : Size -> Size -> Set where
    sizedec : {i : Size} {j : Size< ↑ i} -> SizeDec i j 

  data SizeInc : Size -> Size -> Set where
    sizeinc : {i : Size} {j : Size< ↑ i} -> SizeInc j i 

  data SizeEq : Size -> Size -> Set where 
    sizerefl : {i : Size} -> SizeEq i i 

  -- PSize : Preorder Level.zero Level.zero Level.zero 
  -- PSize = record { Carrier = Size ; _≈_ = _≡_ ; _∼_ = SizeDec ; 
  --                  isPreorder = record { isEquivalence = isEquivalence ; reflexive = λ {refl -> sizedec _ _} ; 
  --                                        trans = λ { (sizedec i j) -> λ { (sizedec j k) -> sizedec i k }} } } 

  SizeDec-trans : Transitive SizeDec 
  SizeDec-trans (sizedec) (sizedec) = sizedec 

  SizeInc-trans : Transitive SizeInc
  SizeInc-trans sizeinc sizeinc = sizeinc

  SizeEq-trans : Transitive SizeEq 
  SizeEq-trans sizerefl sizerefl = sizerefl 

  _×rel_ : ∀ {A B} (R1 : Rel A _) -> (R2 : Rel B _) -> Rel (A × B) _ 
  R1 ×rel R2 = λ { (a , s) (b , t) → R1 a b × R2 s t}

  ×rel-trans : ∀ {A B} {R1 : Rel A _} {R2 : Rel B _} -> Transitive R1 -> Transitive R2 -> Transitive (R1 ×rel R2) 
  ×rel-trans t1 t2 (a≤b , s≤t) (b≤c , t≤u) = (t1 a≤b b≤c) , (t2 s≤t t≤u) 


  -- World = List Ty × Size

  -- _≤w_ : World -> World -> Set 
  -- _≤w_ = _≲_ ×rel SizeDec
  -- ≤w-trans : Transitive _≤w_ 
  -- ≤w-trans { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } (w₁≲w₂ , sizedec) (w₂≲w₃ , sizedec) = (≲-trans w₁≲w₂ w₂≲w₃) , sizedec -- ×rel-trans ≲-trans λ { (sizedec _ _) (sizedec _ _) -> sizedec _ _ }

  -- open KInt  World _≤w_ (λ { { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } -> ≤w-trans { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } })
  -- open KInt' World _≤w_ (λ { { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } -> ≤w-trans { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } })

  World = List Ty 

  _≤w_ : World -> World -> Set 
  _≤w_ = _≲_ 
  open KInt  World _≤w_ ≲-trans
  open KInt' World _≤w_ ≲-trans

  -- World = List Ty × Size
  -- open KInt  World (_≲_ ×rel _≡_) (λ { { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } -> ×rel-trans ≲-trans trans { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) }})
  -- open KInt' World (_≲_ ×rel _≡_) (λ { { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) } -> ×rel-trans ≲-trans trans { (w₁ , i₁) } { (w₂ , i₂)  } { (w₃ , i₃) }})

  -- _≤w_ : World -> World -> Set 
  -- _≤w_ = _≲_ ×rel _≡_
  

--   module W = Preorder World 
--   _≤w_ = W._∼_ 

  MyRel : (A : Ty) -> KRel (Sem A)
  MyRel A = record { 
          prop = λ w f -> ∀ w' -> w ≤w w' -> ∀ t -> (d : Returns ∞ (f w') t) -> ReturnsOk ∞ d (λ r -> ∃[ t ] (r ≡ just t)) ; 
          prop-kripke = λ { w  w' f w≲w' k w'' w'≲w'' -> k w'' (≲-trans w≲w' w'≲w'') } -- (W.trans w≲w' w'≲w'') 
        }
            
  MyMRel : ∀ {R} -> KRel R -> KRel (MSem R)
  MyMRel R = record { prop = λ w f -> ∀ w' -> w ≤w w' -> ∀ t -> (d : Returns ∞ (f w') t) -> ReturnsOk ∞ d (λ t -> KRel.prop R w' t) ; 
                      prop-kripke = λ { w w' f w≲w' k w'' w'≲w'' -> k w'' (≲-trans w≲w' w'≲w'')} } 

  module _ where
    private
      var-ok : ∀ {A} w₁ w₂ -> (A ∷ w₁) ≲ w₂ -> ∀ t -> (d : Returns ∞ (Ret (LamSem.var A w₁ w₂)) t) -> ReturnsOk ∞ d (λ r -> ∃[ t ] (r ≡ just t)) 
      var-ok {A} w₁ w₂ A∷w₁≲w₂ t returns-ret with diffEnv (A ∷ w₁) w₂ | ≲-diff (A ∷ w₁) w₂ A∷w₁≲w₂ 
      ... | _ | wit , refl = returns-ret-ok (Var (shift Z wit) , refl) 
  

    lam-ok : ∀ w -> KRel.prop (forallTy λ a -> forallTy λ b -> (MyRel a ~~> MyRel b) ~~> MyRel (a ~> b)) w lamSem
    lam-ok w A B w₁ w≲w₁ h h-ok w₂ w₁≲w₂ .(a >>= (λ tt → just (Abs tt))) (returns-bind a ret returns-ret) 
      with h-ok (A ∷ w₂) (≲-step w₁≲w₂) (λ w -> Ret (LamSem.var A w₂ w)) (λ { w' rel -> var-ok w₂ w' rel}) (A ∷ w₂) ≲-refl a ret
    ... | res  =  returns-bind-ok ret returns-ret res (λ { (t , refl) -> returns-ret-ok (Abs t , refl)}) -- returns-bind-ok ret returns-ret {!!} 

  app-ok : ∀ w -> KRel.prop (forallTy λ a -> forallTy λ b -> MyRel (a ~> b) ~~> MyRel a ~~> MyRel b) w appSem
  app-ok w A B w₂ w≲w₂ e1 e1-ok w₃ w₂≲w₃ e2 e2-ok w₄ w₃≲w₄ _ 
         (returns-bind a1 e1w₄-returns-a1 (returns-bind a2 e2w₄-returns-a2 returns-ret)) 
   with e1-ok w₄ (≲-trans w₂≲w₃ w₃≲w₄) a1 e1w₄-returns-a1
      | e2-ok w₄ w₃≲w₄ a2 e2w₄-returns-a2
  ... | p1 | p2 =  returns-bind-ok _ _ p1 λ { (r1 , refl) ->
                   returns-bind-ok _ _ p2 λ { (r2 , refl) -> returns-ret-ok (App r1 r2 , refl) } }
-- bind p1 λ { (t1 , refl) -> bind p2 λ { (t2 , refl) -> now (App t1 t2 , refl)} } 
--   -- app-ok w A B w₂ w≲w₂ e1 e1-ok w₃ w₂≲w₃ e2 e2-ok w₄ w₃≲w₄ 
--   --        _ (returns-bind a1 e1w₄-returns-a1 (returns-bind a2 e2w₄-returns-a2 returns-ret)) 
--   --        with e1-ok w₄ (≲-trans w₂≲w₃ w₃≲w₄) a1 e1w₄-returns-a1 
--   -- ... | t1 , refl with e2-ok w₄ w₃≲w₄ a2 e2w₄-returns-a2 
--   -- ... | t2 , refl = App t1 t2 , refl 


--   -- mark-ok : ∀ w -> KRel.prop (forallTy λ a -> MyRel a ~~> MyMRel (MyRel a)) w markSem 
--   -- mark-ok w A w₁ w≲w₁ a a-ok w₂ w₁≲w₂ _ (returns-bind m aw₂-returns-m returns-ret) w₃ w₂≲w₃ _ returns-ret 
--   --   with a-ok w₂ w₁≲w₂ m aw₂-returns-m 
--   -- ... | t , refl with diffEnv w₂ w₃ | ≲-diff w₂ w₃ w₂≲w₃ 
--   -- ... | _ | w₂≲w₃' , refl = shiftExpGen w₂≲w₃' t , refl 

  mark-ok : ∀ w -> KRel.prop (forallTy λ a -> MyRel a ~~> MyMRel (MyRel a)) w markSem 
  mark-ok w A w₁ w≲w₁ a a-ok w₂ w₁≲w₂ _ (returns-bind b (returns-mark t' aw₂-ret-t) returns-ret) 
    with a-ok w₂ w₁≲w₂ _ aw₂-ret-t 
  ... | p = returns-bind-ok (returns-mark t' aw₂-ret-t) returns-ret
            (returns-mark-ok aw₂-ret-t p (t' , refl))
            λ { (t , refl) -> 
                returns-ret-ok (λ {w₃ w₂≲w₃ r returns-ret -> returns-ret-ok ( shiftExp w₂≲w₃ t , lemma w₂≲w₃) }) } 
 -- returns-bind-ok (returns-mark t' aw₂-ret-t) returns-ret 
 --                            (returns-mark-ok aw₂-ret-t p refl) {!!} 
  -- mark-ok w A w₁ w≲w₁ a a-ok w₂ w₁≲w₂ _ (returns-bind b aw₂-ret-b returns-ret) 
  --    with a-ok w₂ w₁≲w₂ b aw₂-ret-b 
  -- ... | p =  returns-bind-ok {!!} _ p (λ {(t , refl) -> 
  --            returns-ret-ok (λ {w₃ w₂≲w₃ r returns-ret -> returns-ret-ok ( shiftExp w₂≲w₃ t , lemma w₂≲w₃) }) })
    where 
      lemma : ∀ {w w'} -> (w≲w' : w ≲ w') -> ∀ {A} {t : DTerm w A} -> (diffEnv w w' >>= λ rel -> just (shiftExpGen rel t)) ≡ just (shiftExpGen w≲w'  t)
      lemma {w} {w'} w≲w' with diffEnv w w' | ≲-diff _ _ w≲w' 
      ... | _ | rel , refl = cong (λ r -> just (shiftExpGen r _)) (≲-canon _ _ rel w≲w')  
  
--   -- local-ok : ∀ w -> KRel.prop (forallTy λ a -> MyMRel (MyRel a) ~~> MyRel a) w localSem
--   -- local-ok w A w₁ w≲w₁ a a-ok w₂ w₁≲w₂ r (returns-bind f aw₂-returns-f fw₂-returns-r) = 
--   --   a-ok w₂ w₁≲w₂ f aw₂-returns-f w₂ ≲-refl r fw₂-returns-r  

  local-ok : ∀ w -> KRel.prop (forallTy λ a -> MyMRel (MyRel a) ~~> MyRel a) w localSem
  local-ok w A w₁ w≲w₁ a a-ok w₂ w₁≲w₂ r (returns-bind f aw₂-returns-f fw₂-returns-r) =
    returns-bind-ok _ _ (a-ok w₂ w₁≲w₂ f aw₂-returns-f) λ prop -> prop w₂ ≲-refl r fw₂-returns-r 
--      bind (a-ok w₂ w₁≲w₂ f i aw₂-returns-f) (λ p -> p w₂ ≲-refl r i fw₂-returns-r)  

--   -- ret-ok : ∀ w -> KRel.prop (forallR λ r -> r ~~> MyMRel r) w retSem
--   -- ret-ok w R w₁ w≲w₁ a a-ok w₂ w₁≲w₂ .a returns-ret = KRel.prop-kripke R w₁ w₂ a w₁≲w₂ a-ok 

  ret-ok : ∀ w -> KRel.prop (forallR λ r -> r ~~> MyMRel r) w retSem
  ret-ok w R w₁ w≲w₁ a a-ok w₂ w₁≲w₂ .a returns-ret = returns-ret-ok (KRel.prop-kripke R w₁ w₂ a w₁≲w₂ a-ok)  

--   -- bind-ok : ∀ w -> KRel.prop (forallR λ r -> forallR λ s -> MyMRel r ~~> (r ~~> MyMRel s) ~~> MyMRel s) w bindSem
--   -- bind-ok w {A₁} R₁ {A₂} R₂ w₁ w≲w₁ m m-ok w₂ w₁≲w₂ f f-ok w₃ w₂≲w₃ t (returns-bind a mw₃-returns-a faw₃-returns-t) with m-ok w₃ (≲-trans w₁≲w₂ w₂≲w₃) a mw₃-returns-a 

  bind-ok : ∀ w -> KRel.prop (forallR λ r -> forallR λ s -> MyMRel r ~~> (r ~~> MyMRel s) ~~> MyMRel s) w bindSem
  bind-ok w {A₁} R₁ {A₂} R₂ w₁ w≲w₁ m m-ok w₂ w₁≲w₂ f f-ok w₃ w₂≲w₃ t (returns-bind a mw₃-returns-a faw₃-returns-t) 
   with m-ok w₃ (≲-trans w₁≲w₂ w₂≲w₃) a mw₃-returns-a 
  ... | p = returns-bind-ok _ _ p 
            (λ prop -> prove ReturnsOk ∞ faw₃-returns-t (KRel.prop R₂ w₃) 
                        by f-ok w₃ w₂≲w₃ a prop w₃ ≲-refl t faw₃-returns-t) 
   --  bind p (λ prop -> f-ok w₃ w₂≲w₃ a prop w₃ ≲-refl t i faw₃-returns-t) 


--   -- ... | p with f-ok w₃ w₂≲w₃ a p w₃ ≲-refl t faw₃-returns-t 
--   -- ... | q = q 

--   -- mfix-ok' : ∀ {τ} -> ∀ w R w₁ -> w ≲ w₁ ->  (f : τ -> List Ty -> M τ) -> KRel.prop (R ~~> MyMRel R) w₁ f -> ∀ w₂ -> w₁ ≲ w₂ -> (t : τ) -> ∀ {i} -> ReturnsFix (λ r -> f r w₂) t i -> KRel.prop R w₂ t
--   -- mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t (returns-fix-iter s {i} ret f-ret) = {!!} 
--   -- --       with ret . force 
--   -- -- ... | r with mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ s r 
--   -- -- ... | r' = {!!} 
--   -- --       with mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ s {!!} {! ret . force!}
--   -- -- ... | res = {!!} 
      
  mfix-ok' : ∀ {τ} {i} -> ∀ w R w₁ -> w ≲ w₁ ->  (f : τ -> List Ty -> M τ) -> KRel.prop (R ~~> MyMRel R) w₁ f -> ∀ w₂ -> w₁ ≲ w₂ -> (t : τ) -> (d : Returns ∞ (MFix (λ r -> f r w₂)) t) -> ReturnsOk i d (KRel.prop R w₂) 
  mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t (returns-fix rr f-ret) = 
    returns-fix-ok _ _ (λ { .force -> mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ _ (force rr) }) 
                        λ prop -> prove ReturnsOk ∞ f-ret (KRel.prop R w₂) 
                                   by f-ok w₂ w₁≲w₂ _ prop w₂ ≲-refl t f-ret    
--   mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t (returns-fix {i} rr f-ret) = -- (returns-fix-iter s {k} gd) = 
--    later λ where .force {j} -> 
--                     bind (mfix-ok'  w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t (force rr)) λ prop -> 
--                       (f-ok w₂ w₁≲w₂ t prop w₂ ≲-refl _  j (weaken-returns f-ret))    
--                       -- let f-ok' = KRel.prop-kripke (R ~~> MyMRel R) (w₁ , i) (w₁ , j) f (≲-refl , sizedec) f-ok 
--                       -- in case force rr of λ 
--                       --  { (ret , f-ret)  ->  
--                       --         bind (mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ s ret  ) λ prop -> 
--                       --          {!f-ok (w₂ , j) (w₁≲w₂ , sizedec) s prop (w₂ , _) (≲-refl , sizedec) _ f-ret!} } -- f-ok (w₂ , i) (w₁≲w₂ , sizedec) s prop (w₂ , j) (≲-
-- --                               refl , sizedec) _ f-ret   
--   --   with mfix-ok' w R w₁ w≲w₁ f (KRel.prop-kripke (R ~~> MyMRel R) (w₁ , i) (w₁ , _) f {!≲-refl , sizedec i _!} f-ok) w₂ w₁≲w₂ s {!force ret!}
--   -- ... | res = {!!}

  mfix-ok : ∀ w -> KRel.prop (forallR λ r -> (r ~~> MyMRel r) ~~> MyMRel r) w mfixSem 
  mfix-ok w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t rr -- (returns-fix rr) 
    = mfix-ok' w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t rr  




--   -- -- mfix-ok w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ t (returns-fix (returns-bind a mfix-returns-a faw₂-returns-t)) 
--   -- --    with mfix-ok w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ a mfix-returns-a 
--   -- -- ... | pa with f-ok w₂ w₁≲w₂ a pa w₂ ≲-refl t faw₂-returns-t 
--   -- -- ... | p = p 

  module _ (free-theorem : ∀ A w f -> KInt.KRel.prop (m-term A) w f ) where 

    convert-succeeds : ∀ A w -> (h : Poly A) -> ∀ r -> (d : Returns ∞ (convert A w h) r) -> ReturnsOk ∞ d (λ r -> ∃[ t ] (r ≡ just t))
    convert-succeeds A w h =  
      free-theorem A w h MyRel MyMRel w ≲-refl lamSem (lam-ok w)
                                      w ≲-refl appSem (app-ok w) 
                                      w ≲-refl markSem (mark-ok w) 
                                      w ≲-refl localSem (local-ok w) 
                                      w ≲-refl retSem (ret-ok w) 
                                      w ≲-refl bindSem (bind-ok w) 
                                      w ≲-refl mfixSem (mfix-ok w) 
                                      w ≲-refl 
    
