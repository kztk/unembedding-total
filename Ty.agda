{-# OPTIONS --without-K #-}

open import Relation.Nullary
open import Axiom.UniquenessOfIdentityProofs 
open import Relation.Binary.PropositionalEquality hiding (Extensionality)


data Ty : Set where
  TBool : Ty 
  _~>_ : Ty -> Ty -> Ty 

checkEqTy : (t1 : Ty) -> (t2 : Ty) -> Dec (t1 ≡ t2) 
checkEqTy TBool TBool = yes refl
checkEqTy TBool (t2 ~> t3) = no (λ ())
checkEqTy (t1 ~> t3) TBool = no (λ ())
checkEqTy (t1 ~> t3) (t2 ~> t4) with checkEqTy t1 t2 | checkEqTy t3 t4 
checkEqTy (t1 ~> t3) (.t1 ~> .t3) | yes refl | yes refl = yes refl
checkEqTy (t1 ~> t3) (.t1 ~> t4) | yes refl | no ¬p = no λ eq -> ¬p (res-eq eq)
  where
    res-eq : ∀ {t1 t2 t3 t4} -> (t1 ~> t2) ≡ (t3 ~> t4) -> t2 ≡ t4 
    res-eq refl = refl
checkEqTy (t1 ~> t3) (t2 ~> t4) | no ¬p | res2 = no λ eq -> ¬p (arg-eq eq)
  where
    arg-eq : ∀ {t1 t2 t3 t4} -> (t1 ~> t2) ≡ (t3 ~> t4) -> t1 ≡ t3
    arg-eq refl = refl
    

checkEqTy-diag : {ty1 ty2 : Ty} -> (eq : ty1 ≡ ty2) -> checkEqTy ty1 ty2 ≡ yes eq
checkEqTy-diag = ≡-≟-identity checkEqTy


