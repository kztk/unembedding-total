{-# OPTIONS --without-K #-}

open import Level 
open import Data.List as List using (List ; [] ; _∷_) renaming (_++_ to _++L_)

open import Data.Product using (_×_)
open import Data.Unit using (⊤)

data Env {ℓ ℓ'} (K : Set ℓ) (T : K -> Set ℓ') : List K ->  Set (ℓ ⊔ ℓ') where 
  [] : Env K T [] 
  _∷_ : ∀ {a as} -> T a -> Env K T as -> Env K T (a ∷ as)

infixr 5 _∷_

head : {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {t : K} {ts : List K} 
       -> Env K T (t ∷ ts) -> T t 
head (a ∷ _) = a 

tail : {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {t : K} {ts : List K} 
       -> Env K T (t ∷ ts) -> Env K T ts 
tail (_ ∷ as) = as

map : {ℓ ℓ' ℓ'' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {S : K -> Set ℓ''} {ts : List K} -> ((a : K) -> T a -> S a) -> Env K T ts -> Env K S ts
map f [] = []
map f (a ∷ as) = f _ a ∷ map f as 

zipWith : 
  {ℓ ℓ' ℓ'' ℓ''' : Level} {K : Set ℓ} 
  {T : K -> Set ℓ'} {S : K -> Set ℓ''} {U : K -> Set ℓ'''} {ts : List K} 
  -> ((a : K) -> T a -> S a -> U a)
  -> Env K T ts -> Env K S ts -> Env K U ts 
zipWith f [] [] = []
zipWith f (a ∷ as) (b ∷ bs) = f _ a b ∷ zipWith f as bs 

toList : {ℓ ℓ' : Level} {K : Set ℓ} {A : Set ℓ'} {ts : List K} -> Env K (λ _ -> A) ts -> List A 
toList [] = []
toList (a ∷ as) = a ∷ toList as 

mapToList : {ℓ ℓ' ℓ'' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {B : Set ℓ''} {ts : List K} -> ((a : K) -> T a -> B) -> Env K T ts -> List B
mapToList {B = B} f xs = toList (map {S = λ _ -> B} f xs)

fromList : {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} -> ((a : K) -> T a) -> (ts : List K) -> Env K T ts 
fromList f [] = []
fromList f (x ∷ ts) = f x ∷ fromList f ts

_++_ : {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {ts₁ ts₂ : List K}
       -> Env K T ts₁ -> Env K T ts₂ -> Env K T (ts₁ ++L ts₂) 
[] ++ bs = bs
(a ∷ as) ++ bs = a ∷ (as ++ bs)

module _ where 
  open import Data.List.Membership.Propositional using (_∈_)
  open import Data.List.Relation.Unary.Any using (there ; here)
  open import Relation.Binary.PropositionalEquality using (refl)

  lookup : {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {ts : List K} {t : K} -> Env K T ts -> t ∈ ts -> T t
  lookup (a ∷ _) (here refl) = a
  lookup (_ ∷ env) (there x) = lookup env x

module _ where 
  open import Relation.Binary.PropositionalEquality as Eq using (refl ; _≡_)

  ∷-injectiveˡ : 
    {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {t : K} {ts : List K} 
    {x y : T t} {xs ys : Env K T ts} 
    -> _≡_ {A = Env K T (t ∷ ts)} (x ∷ xs) (y ∷ ys) -> x ≡ y 
  ∷-injectiveˡ refl = refl

  ∷-injectiveʳ :
    {ℓ ℓ' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {t : K} {ts : List K} 
    {x y : T t} {xs ys : Env K T ts} 
    -> _≡_ {A = Env K T (t ∷ ts)} (x ∷ xs) (y ∷ ys) -> xs ≡ ys
  ∷-injectiveʳ refl = refl 
  open import Data.Product


Env⟦_⟧ : {ℓ ℓ' : Level} {K : Set ℓ} {ts : List K} -> Env K (λ _ -> Set ℓ') ts -> Set (Level.suc ℓ')
Env⟦ [] ⟧ = Lift _ ⊤ 
Env⟦ a ∷ as ⟧ = a × Env⟦ as ⟧

data AllEnv {ℓ ℓ' ℓ''} (K : Set ℓ) (T : K -> Set ℓ') (P : (a : K) -> T a -> Set ℓ'') : {ts : List K} -> Env K T ts -> Set (ℓ ⊔ ℓ' ⊔ ℓ'') where 
  [] : AllEnv K T P [] 
  _∷_ : ∀ {a as} {x : T a} {xs : Env K T as} -> P a x -> AllEnv K T P xs -> AllEnv K T P (x ∷ xs)

module _ where 
  open import Data.List.Relation.Unary.All 

  All-mapToList-AllEnv : 
    {ℓ ℓ' ℓ'' ℓ''' : Level}
    {K : Set ℓ} {T : K -> Set ℓ'} {ts : List K}
    {B : Set ℓ''}
    -> (f : (a : K) -> T a -> B)
    -> (P : B -> Set ℓ''') 
    -> (xs : Env K T ts)
    -> All P (mapToList f xs)
    -> AllEnv K T (λ a t -> P (f a t)) xs 
  All-mapToList-AllEnv f P [] all = []
  All-mapToList-AllEnv f P (_ ∷ xs) (px ∷ all) = px ∷ (All-mapToList-AllEnv f P xs all)


data EnvEnv {ℓ ℓ' ℓ'' : Level} (K : Set ℓ) (T : K -> Set ℓ') (S : (a : K) -> T a -> Set ℓ'') : (ts : List K) -> Env K T ts ->  Set (ℓ ⊔ ℓ' ⊔ ℓ'') where 
  [] : EnvEnv K T S [] [] 
  _#_∷_ : ∀ {a as} -> (b : T a) -> S a b -> ∀ {bs} -> EnvEnv K T S as bs -> EnvEnv K T S (a ∷ as) (b ∷ bs)

infixr 5 _#_∷_

fromEnv : 
  {ℓ ℓ' ℓ'' : Level} 
  {K : Set ℓ} {T : K -> Set ℓ'} {S : (a : K) -> T a -> Set ℓ''} {ts : List K}
  -> (f : (a : K) -> (b : T a) -> S a b)
  -> (as : Env K T ts) 
  -> EnvEnv K T S ts as 
fromEnv f [] = []
fromEnv f (a ∷ as) = _ # f _ a ∷ fromEnv f as
