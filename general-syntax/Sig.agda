{-# OPTIONS --without-K #-}

open import Data.List using (List)
open import Level using (Level)
open import Data.Nat using (ℕ)
open import Data.Fin using (Fin)
open import Data.Vec using (Vec ; [] ; _∷_) 

open import Env renaming (_++_ to _++ₑ_)

module Sig (Ty : ℕ -> Set)
           (tyVar : {n : ℕ} -> Fin n -> Ty n)
           (substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0) where 

-- Sig is a 'type' for arguments of a language construct 
-- 
-- For example, (t₁ ∷ t₂ ∷ t₃ ∷ []) ↝ t represents an argument of type
-- t that binds t₁, t₂ and t₃ typed variables. Such a construct may be
-- represented as:
--   * Term (t₁ ∷ t₂ ∷ t₃ ∷ env) t   in the de Bruijn indexed terms.
--   * (T t₁ -> T t₂ -> T t₃ -> T t) in HOAS

record Sig n : Set where 
  constructor _↝_ 
  field 
    src : List (Ty n)
    dst : Ty n

substTySig : {n : ℕ} -> Vec (Ty 0) n -> Sig n -> Sig 0 
substTySig tsub (as ↝ a) = Data.List.map (substTy tsub) as ↝ substTy tsub a

-- Similarly, ConstructSig is a signature of constructs.
-- 
-- For in a lambda calculus, appilcation has 
--   CSig 2 [ [] ↝ (a -> b) , [] ↝ a ] b 
-- and abstraction has
--   CSig 2 [ [a] ↝ b ] (a -> b)
-- where a = TyVar zero and b = TyVar (suc zero)
-- (Here, we used the Haskell's list notation for readability.)
 

record ConstructSig : Set where 
  constructor CSig 
  field
    nargs  : ℕ
    args   : List (Sig nargs)
    result : Ty nargs 

LangSig : Set 
LangSig = List ConstructSig

