{-# OPTIONS --without-K --sized-types #-}

open import Size

open import Env renaming (_++_ to _++ₑ_)

open import Data.Nat using (ℕ ; _≤_ ; _<_)
open import Data.Fin using (Fin)
open import Data.Vec using (Vec ; [] ; _∷_) 

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_ ; refl; sym; trans; cong ; subst )
open import Relation.Nullary using (Dec ; yes ; no)

module Term-HOAS-Term  
  (Ty : ℕ -> Set)
  (tyVar : {n : ℕ} -> Fin n -> Ty n)
  (substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0) 
  (tyEq : (t₁ t₂ : Ty 0) -> Dec (t₁ ≡ t₂))
  where 


import Sig 
open Sig Ty tyVar substTy 
import Syntax
open Syntax Ty tyVar substTy 

open Syntax.H2D Ty tyVar substTy tyEq

open import Data.List
open import Data.Maybe
import Data.Maybe.Properties 

module _ (L : LangSig) where 
  open import Data.List.Membership.Propositional using (_∈_)
  open import Data.List.Relation.Unary.Any using (Any; here; there)
  open import Data.List.Relation.Unary.All using (All; [] ; _∷_)
  import Data.List.Relation.Unary.All as All 
  open import Data.List.Relation.Unary.Any.Properties using (++⁺ʳ)

  open import Data.Product 


  env-ok : {sz : Size} -> (ts : List Ty0) 
           -> (θ : Env Ty0 (TSem L) ts)
           -> Set 
  env-ok {sz} ts θ = {t : Ty0} (x : t ∈ ts) -> ∀ ts'  -> (diff : ts ≲ ts') -> {tm : DeBruijnTerm L {sz} ts' t} -> Env.lookup θ x ts' ≡ just tm -> tm ≡ Var (weaken-var diff x) 

  env-ok-[] : env-ok [] []
  env-ok-[] () ts' diff eq

  open import Data.List.Properties using (++-assoc) 
  
  private
    lookup-of-construct-aux : 
      (csig : ConstructSig)
      -> (LL : List ConstructSig) 
      -> (index : csig ∈ LL)
      -> (f : List ConstructSig -> List ConstructSig) 
      -> (g : ∀ {x} -> x ∈ LL -> x ∈ f LL)
      -> (all-in : All (_∈ f LL) LL)
      -> (∀ {x} -> (index : x ∈ LL) -> All.lookup all-in (index) ≡ g index)
      -> Env.lookup (DeBruijn-Alg (f LL) LL all-in) index ≡ DeBruijn-Op (f LL)  csig (g index)
    lookup-of-construct-aux csig (.csig ∷ LL) (here refl) f g (px ∷ _) prop rewrite prop (here refl) = refl
    lookup-of-construct-aux csig (x ∷ LL) (there index) f g (_ ∷ all-in) prop =  lookup-of-construct-aux csig LL index (λ xs -> f (x ∷ xs)) (λ incl -> g (++⁺ʳ (x ∷ []) incl)) all-in λ ix -> prop (there ix)

    lookup-of-construct :
      (csig : ConstructSig)
      -> (index : csig ∈ L)
      -> (all-in : All (_∈ L) L)
      -> (∀ {x} -> (index : x ∈ L) -> All.lookup all-in index ≡ index)
      -> Env.lookup (DeBruijn-Alg L L all-in) index ≡ DeBruijn-Op L csig index
    lookup-of-construct csig index all-in prop = lookup-of-construct-aux csig L index (λ x -> x) (λ x -> x) all-in prop

  private 
      DeBruijn-Interp-success : 
        ∀ {n ss a ix tsub fs tenv tm}
        -> DeBruijn-Op L (CSig n ss a) ix tsub fs tenv ≡ just tm
        -> ∃[ tms ] (mapEnvMaybe (DeBruijn-Interp.makeArg L tenv) fs ≡ just tms 
                    × tm ≡ Con tsub ix tms)
      DeBruijn-Interp-success {n} {ss} {a} {ix} {tsub} {fs} {tenv} {tm} equal with mapEnvMaybe (DeBruijn-Interp.makeArg L tenv) fs in eq' 
      ... | just tms = tms , refl , sym (Data.Maybe.Properties.just-injective equal)

  open import Data.Nat.Induction

  -- _ = {!<-rec!} 

  module _ where 
    P : (n : ℕ) -> Set
    P n = 
      {t : Ty0} {ts : List Ty0}
      -> (term term' : DeBruijnTerm L ts t) 
      -> (termSize term ≤ n) 
      -> (θ : Env Ty0 (TSem L) ts)
      -> env-ok ts θ 
      -> deBruijn-to-HOAS L term (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ ts ≡ just term' 
      -> term' ≡ term

    term-HOAS-term-lemma : 
      (k : ℕ) -> (IH : (m : ℕ) -> m < k -> P m) -> P k
    term-HOAS-term-lemma k IH (Var x) term' termSize≤n θ θok eq = θok x _ ≲-refl eq 
    term-HOAS-term-lemma k IH {t} {ts} (Con {n = n} {ss} {a} tsub construct args) term' termSize≤n θ θok eq with DeBruijn-Interp-success {fs = fs} eq'  
      where
        prop : Env.lookup (DeBruijn-Alg L L (All-∈-self L)) construct ≡ DeBruijn-Op L _ construct
        prop = lookup-of-construct _ construct (All-∈-self L) (All-∈-self-canon)

        fs = (Env.map (deBruijn-to-HOAS-arg L (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ) args) 

        eq' : DeBruijn-Op L _ construct tsub fs ts ≡ just term' 
        eq' = trans (cong (λ h -> h tsub (Env.map (deBruijn-to-HOAS-arg L (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ) args) ts) (sym prop)) eq
    ... | tms , eqMapMaybe , refl = cong (Con tsub construct) (lemma args tms (termSize-step {ix = construct} args termSize≤n) eqMapMaybe)
      where 
        open import Data.Nat.Properties using (≤-refl) 
        open import Relation.Nullary using (yes ; no) 
        open import Data.Empty 


        makeX-succeed : 
          ∀ a as ts tm
          -> DeBruijn-Interp.makeX L a as ts ≡ just tm 
          -> Σ[ diff ∈ a ∷ as ≲ ts ] tm ≡ Var (weaken-var diff (here refl))
        makeX-succeed a as ts tm equal with a ∷ as ≲? ts 
        ... | yes diff = diff , sym (Data.Maybe.Properties.just-injective equal)  
        makeX-succeed a as ts tm () | no p

        lemma3 :
          ∀ as 
          -> env-ok (as ++ ts) (DeBruijn-Interp.makeXs L ts as ++ₑ θ) 
        lemma3 [] ix ts' ≲ts' equal = θok ix ts' ≲ts' equal
        lemma3 (a ∷ as) (here refl) (x ∷ ts') ≲ts' equal with makeX-succeed _ _ (x ∷ ts') _ equal 
        ... | diff , refl = cong (λ h -> Var (weaken-var h (here refl))) (≲-canon _ _ diff ≲ts')  
        lemma3 (a ∷ as) (there ix) ts' ≲ts' equal =  subst (λ h -> _ ≡ Var h) (sym (weaken-var-there ix ≲ts')) (lemma3 as ix ts' (≲-trans (≲-step ≲-refl) ≲ts') equal)

        lemma2 : 
          ∀ {K}
          -> (arg tm : opArgTerm (DeBruijnTerm L) ts K)
          -> (arg<k : termSizeOfArg K arg < k)
          -> DeBruijn-Interp.makeArg L ts K (deBruijn-to-HOAS-arg L  (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ K arg) ≡ just tm
          -> tm ≡ arg
        lemma2 {as ↝ a} arg tm arg<k equal = IH (termSize arg) arg<k arg tm ≤-refl (DeBruijn-Interp.makeXs L ts as ++ₑ θ) (lemma3 as) equal 
        
        lemma : 
          ∀ {ss} (args' tms' : Env (Sig 0) (opArgTerm (DeBruijnTerm L) ts) ss) 
          -> (args'<k : All (_< k) (Env.mapToList termSizeOfArg args'))
          -> mapEnvMaybe (DeBruijn-Interp.makeArg L ts) (Env.map (deBruijn-to-HOAS-arg L (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ) args') ≡ just tms'
          -> tms' ≡ args'
        lemma [] [] _ equal = refl
        lemma (_∷_ {K} arg args') (tm ∷ tms') (arg<k ∷ args'<k) equal with DeBruijn-Interp.makeArg L ts K (deBruijn-to-HOAS-arg L  (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ K arg) in equalArg
        ... | just x with mapEnvMaybe (DeBruijn-Interp.makeArg L ts) (Env.map (deBruijn-to-HOAS-arg L (TSem L) (DeBruijn-Alg L L (All-∈-self L)) θ) args') in equalArgs 
        ... | just xs with equal 
        ... | refl with lemma args' tms' args'<k equalArgs 
        ... | refl with lemma2 {K} arg x arg<k equalArg
        ... | refl = refl

    term-HOAS-term : 
      {t : Ty0} 
      -> (term term' : DeBruijnTerm L [] t)
      -> HOAS-to-deBruijn L (λ S interp -> deBruijn-to-HOAS L term S interp []) [] ≡ just term'  
      -> term' ≡ term
    term-HOAS-term term term' eq = <-rec P term-HOAS-term-lemma (termSize term) term term' ≤-refl [] env-ok-[] eq 
      where
        open import Data.Nat.Properties using (≤-refl)

