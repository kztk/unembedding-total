{-# OPTIONS --without-K --sized-types #-}

open import Size
open import Level renaming (zero to lzero; suc to lsuc)

open import Env renaming (_++_ to _++ₑ_)

open import Data.Nat using (ℕ ; _≤_ ; _<_)
open import Data.Fin as Fin using (Fin ; suc ; zero)
open import Data.Vec using (Vec ; [] ; _∷_) 

open import Data.List as L using (List ; [] ; _∷_ ; _++_)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_ ; refl; sym; trans; cong ; subst )
open import Relation.Nullary using (Dec ; yes ; no)


module H2DTotal
  (Ty : ℕ -> Set)
  (tyVar : {n : ℕ} -> Fin n -> Ty n)
  (substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0) 
  (tyEq : (t₁ t₂ : Ty 0) -> Dec (t₁ ≡ t₂))
  where

import Sig 
open Sig Ty tyVar substTy 
import Syntax
open Syntax Ty tyVar substTy 
open Syntax.H2D Ty tyVar substTy tyEq

module _ (L : LangSig) where 
  open import Data.List.Membership.Propositional using (_∈_)
  open import Data.List.Relation.Unary.Any using (Any; here; there)
  open import Data.List.Relation.Unary.All using (All; [] ; _∷_)
  import Data.List.Relation.Unary.All as All 
  open import Data.List.Relation.Unary.Any.Properties using (++⁺ʳ)

  open import Kripke Ty tyVar substTy as K 
  open K.Kripke (≲-preorder) 

  open import Data.Product
  open import Data.Maybe as Maybe using (Maybe ; just ; nothing )

  open import Data.Empty 
  open import Data.Unit

  open USem
  open KRel

  BaseI : USem 1 (Ty 0)
  BaseI .TInt _ = TSem L

  BaseRel : (A : Ty 0) -> KRel 1 (Ty 0) BaseI A 
  prop (BaseRel A) ts vs = ∀ ts' -> ts ≲ ts' -> Σ[ t ∈ DeBruijnTerm L ts' A ] vs zero ts' ≡ just t
  prop-kripke (BaseRel A) _ _ ts≲ts' vs p _ ts'≲ts'' = p _ (≲-trans ts≲ts' ts'≲ts'')

  open KIntL 1 BaseI BaseRel
  open Unary
--  open Unary.KIntL₁ BaseI (λ A -> Unary.KRel→KRel₁ (BaseRel A))

  postulate 
    free-theorem : 
      (t : Ty 0) (ts : List Ty0) 
      (f : Env ConstructSig (Val₁ CSigI) L -> Val₁ BaseI t) 
      -> prop₁ (ExpK L t) ts f

  -- Notice: here we have TInt CSigI zero csig ≡ opFun (TSem L) csig 

  module _ where 
    open import Axiom.Extensionality.Propositional

    postulate
      funext : Extensionality lzero lzero         

    alg : (ix : Fin 1) -> Env ConstructSig (TInt CSigI ix) L 
    alg _ = DeBruijn-Alg L L (All-∈-self L)

    HOAS-to-deBruijn-succeeds :
      (t : Ty 0) (ts : List Ty0)
      -> (f : Poly L t)
      -> Σ[ tm ∈ DeBruijnTerm L ts t ] (f (TSem L)) (alg zero) ts ≡ just tm
    HOAS-to-deBruijn-succeeds t ts f = free-theorem t ts (f (TSem L)) ts ≲-refl alg (lemma ts) ts ≲-refl where
        -- f' : (ix : Fin 1) -> Env ConstructSig (TInt CSigI ix) L -> TInt BaseI ix t 
        -- f' _ = adjust-type-f (f (TSem L))  
    
        -- convEnv : 
        --   {n : ℕ} (tsub : Vec Ty0 n) (ss : List (Sig n))
        --   -> Env (Sig n) (USem.TInt (SigI-n tsub) zero) ss 
        --   -> Env (Sig 0) (USem.TInt SigI zero) (L.map (substTySig tsub) ss)
        -- convEnv tsub [] [] = []
        -- convEnv tsub (s ∷ ss) (e ∷ es) = e ∷ (convEnv tsub ss es)

        individual-construct : 
          {ts : List Ty0}
          (csig : ConstructSig) (px : csig ∈ L)
          -> prop₁ (CSigK csig) ts (DeBruijn-Op L csig px)
        individual-construct (CSig n ss a) px tsub ts' ts≲ts' args pargs ts'' ts'≲ts''
          with 
               mapEnvMaybe (DeBruijn-Interp.makeArg L ts'') (args zero) 
             | mapEnvMaybe-succeeds (L.map (substTySig tsub) ss) args pargs 
          where
            makeX-succeeds :
              {a : Ty 0} {as : List (Ty 0)} (ts''' : List Ty0)
              -> a ∷ as ++ ts'' ≲ ts'''
              -> ∃[ t ] DeBruijn-Interp.makeX L a
                                              (as ++ ts'') ts'''
                        ≡ just t
            makeX-succeeds {a} {as} ts''' ts''≲ts''' with (a ∷ as) ++ ts'' ≲? ts'''
            ... | yes diff = Var (weaken-var diff (here refl)) , refl 
            ... | no  p    = ⊥-elim (p ts''≲ts''')
                                       

            apw : (as : List (Ty 0)) (bs : List (Ty 0)) 
                  -> bs ≲ as
                  -> allPointwise 1 Ty0 BaseI (as ++ ts'')
                     bs
                     (transpose-env 1 
                     (λ _ -> DeBruijn-Interp.makeXs L ts'' bs))
                     (fromList BaseRel bs)
            apw as [] _ = lift tt
            apw as (b ∷ bs) b∷bs≲as = (λ ts''' diff -> makeX-succeeds ts''' (≲-trans (≲-init b∷bs≲as) diff)) , apw as bs (≲-trans ≲-one b∷bs≲as)

            makeXs-succeeds : 
              {s : Sig 0}
              -> (v : Env (Ty 0) (TSem L) (Sig.src s)
                      -> TSem L (Sig.dst s))
              -> (pv : prop₁ (SigK s) ts' v)
              -> ∃[ t ] (v (DeBruijn-Interp.makeXs L ts'' (Sig.src s))
                            ((Sig.src s) ++ ts''))
                        ≡ just t 
            makeXs-succeeds {s}  v pv = pv (Sig.src s ++ ts'') (≲-trans ts'≲ts'' (≲-++ ≲-refl)) (λ _ -> DeBruijn-Interp.makeXs L ts'' (Sig.src s)) (apw (Sig.src s) (Sig.src s) ≲-refl) _ ≲-refl 

            mapEnvMaybe-succeeds : 
              (ss : List (Sig 0)) 
              -> (args : Fin 1 -> Env (Sig 0) (TInt (SigI) zero) ss)
              -> (pargs : allPointwise 1 (Sig 0) (SigI) ts' ss (transpose-env 1 args) (fromList (SigK) ss))
              -> ∃[ tms ] 
                    mapEnvMaybe (DeBruijn-Interp.makeArg L ts'') (args zero)                    
                    ≡ just tms 
            mapEnvMaybe-succeeds [] args pargs with args zero 
            ... | [] = [] , refl
            mapEnvMaybe-succeeds (s ∷ ss) args (pv , pargs) with args zero in equal 
            ... | v ∷ vs with v (DeBruijn-Interp.makeXs L ts'' (Sig.src s)) (Sig.src s ++ ts'') | makeXs-succeeds {s} v pv 
            ... | just x | t , refl with 
                  mapEnvMaybe (DeBruijn-Interp.makeArg L ts'') vs
                | mapEnvMaybe-succeeds ss (λ _ -> vs) (subst (λ z -> allPointwise 1 _ _ ts' ss (transpose-env 1 z) (fromList (SigK) ss)) (funext (λ where zero -> cong Env.tail equal)) pargs)
            ... | just xs | _ , refl = x ∷ xs , refl

        ... | just tm | _ , refl = Con tsub px tm , refl 


        lemma : (ts : List Ty0) -> prop (LangSigK L) ts alg -- adjusted-interps
        lemma ts = go L (All-∈-self L) 
          where
            go : 
              (LL : List ConstructSig)
              -> (all-in-L : All (_∈ L) LL) 
              -> allPointwise 1 ConstructSig CSigI ts 
                           LL 
                           (transpose-env 1 (λ _ -> DeBruijn-Alg L LL all-in-L))
                           (fromList CSigK LL)
            go [] [] = lift tt
            go (x ∷ LL) (px ∷ all-in-L) = individual-construct x px , (go LL all-in-L)
          
