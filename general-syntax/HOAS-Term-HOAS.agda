{-# OPTIONS --without-K --sized-types #-}

open import Size
open import Level renaming (zero to lzero; suc to lsuc)

open import Env renaming (_++_ to _++ₑ_)

open import Data.Nat using (ℕ ; _≤_ ; _<_)
open import Data.Fin as Fin using (Fin ; suc ; zero)
open import Data.Vec using (Vec ; [] ; _∷_) 

open import Data.List as L using (List ; [] ; _∷_ ; _++_)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_ ; refl; sym; trans; cong ; subst )
open import Relation.Nullary using (Dec ; yes ; no)

module HOAS-Term-HOAS 
  (Ty : ℕ -> Set) 
  (tyVar : {n : ℕ} -> Fin n -> Ty n) 
  (substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0) 
  (tyEq : (t₁ t₂ : Ty 0) -> Dec (t₁ ≡ t₂))
  where


0F : ∀ {n : ℕ} -> Fin (ℕ.suc n)
0F = zero

1F : ∀ {n : ℕ} -> Fin (ℕ.suc (ℕ.suc n))
1F = suc zero 

import Sig 
open Sig Ty tyVar substTy 
import Syntax
open Syntax Ty tyVar substTy 
open Syntax.H2D Ty tyVar substTy tyEq


module _ (L : LangSig)  where 
  open import Data.List.Membership.Propositional using (_∈_)
  open import Data.List.Relation.Unary.Any using (Any; here; there)
  open import Data.List.Relation.Unary.All using (All; [] ; _∷_)
  import Data.List.Relation.Unary.All as All 
  open import Data.List.Relation.Unary.Any.Properties using (++⁺ʳ)

  open import Relation.Binary 

  open import Data.Product

  module _ (S : Ty0 -> Set) (alg : SO-Alg L S) where 

    open import Data.Empty

    ValEnv : Set 
    ValEnv = Σ[ w ∈ List Ty0 ] (Env Ty0 S w)
    
    tyenv : ValEnv -> List Ty0
    tyenv (w , _) = w 

    valenv : (ve : ValEnv) -> Env Ty0 S (tyenv ve) 
    valenv (_ , θ) = θ

    data _▷_≲e_ : {w w' : List Ty0} -> w ≲ w' -> Env Ty0 S w -> Env Ty0 S w' -> Set where
      ≲e-refl : ∀ {w} {θ : Env Ty0 S w} -> ≲-refl ▷ θ ≲e θ
      ≲e-step : 
        ∀ {w w'} {θ₁ : Env Ty0 S w} {θ₂ : Env Ty0 S w'} {diff : w ≲ w'} {A} {v : S A} 
        -> diff ▷ θ₁ ≲e θ₂ 
        -> ≲-step diff ▷ θ₁ ≲e (v ∷ θ₂) 
    -- basically, _ ▷ θ₁ ≲e θ₂ means θ₂ = θ ++ θ₂ for some θ 

    _≲v_ : ValEnv -> ValEnv -> Set 
    (w₁ , θ₁) ≲v (w₂ , θ₂) = Σ[ diff ∈ w₁ ≲ w₂ ] (diff ▷ θ₁ ≲e θ₂)

    ≲v-refl : Reflexive _≲v_ 
    ≲v-refl = ≲-refl , ≲e-refl

    ≲e-trans : 
      ∀ {w₁ w₂ w₃} {diff : w₁ ≲ w₂} {diff' : w₂ ≲ w₃}
      {θ₁ : Env Ty0 S w₁} {θ₂ : Env Ty0 S w₂} {θ₃ : Env Ty0 S w₃}
      -> diff ▷ θ₁ ≲e θ₂ -> diff' ▷ θ₂ ≲e θ₃ -> (≲-trans diff diff' ▷ θ₁ ≲e θ₃)
    ≲e-trans ≲e-refl rel' = rel'
    ≲e-trans (≲e-step rel) ≲e-refl = ≲e-step rel
    ≲e-trans (≲e-step rel) (≲e-step rel') = ≲e-step (≲e-trans (≲e-step rel) rel') 

    ≲v-trans : Transitive _≲v_ 
    ≲v-trans (1≲2 , i≲j) (2≲3 , j≲k) =  ≲-trans 1≲2 2≲3 , ≲e-trans i≲j j≲k

    ≲v-isPreorder : IsPreorder _≡_ _≲v_ 
    IsPreorder.isEquivalence ≲v-isPreorder = Eq.isEquivalence
    IsPreorder.reflexive ≲v-isPreorder refl = ≲v-refl
    IsPreorder.trans ≲v-isPreorder = ≲v-trans

    ≲v-preorder : Preorder Level.zero Level.zero Level.zero 
    Preorder.Carrier ≲v-preorder = ValEnv
    Preorder._≈_ ≲v-preorder = _≡_
    Preorder._∼_ ≲v-preorder = _≲v_
    Preorder.isPreorder ≲v-preorder = ≲v-isPreorder

    ≲v-++ : {as : List Ty0} {θ : Env Ty0 S as} {as' : List Ty0} {θ' : Env Ty0 S as'} 
            (as'' : List Ty0) (θ'' : Env Ty0 S as'')
            -> (as , θ) ≲v (as' , θ') -> (as , θ) ≲v (as'' ++ as' , θ'' ++ₑ θ')
    ≲v-++ [] [] diffv = diffv
    ≲v-++ (x ∷ as'') (x₁ ∷ θ'') diffv =  ≲v-trans (≲v-++ as'' θ'' diffv) (≲-one , ≲e-step ≲e-refl)

    open import Kripke Ty tyVar substTy as K 
    open K.Kripke (≲v-preorder) 

    open import Data.Maybe as Maybe using (Maybe ; just ; nothing )

    open import Data.Empty 
    open import Data.Unit

    open USem 
    open KRel 


    BaseI : USem 2 (Ty 0) 
    BaseI .TInt zero = TSem L 
    BaseI .TInt (suc _) = S

    BaseRel : (A : Ty0) -> KRel 2 Ty0 BaseI A 
    prop (BaseRel A) ts v = 
      ∀ ts' -> ts ≲v ts' -> ∀ tm
      -> v zero (tyenv ts') ≡ just tm
      -> deBruijn-to-HOAS L tm S alg (valenv ts') ≡ v (suc zero)  
    prop-kripke (BaseRel A) _ _ w≲w' v f _ w'≲w'' = f _ (≲v-trans w≲w' w'≲w'')

    open KIntL 2 BaseI BaseRel
    open Binary
 
    module 
      Proof
      (free-theorem₂ : 
        (t : Ty0) (w : ValEnv)
        -> (f : TInt ExpI 0F (L , t)) (f' : TInt ExpI 1F (L , t))
        -> prop₂ (ExpK L t) w (f , f')) where 
          
      open import Axiom.Extensionality.Propositional

      postulate
        funext : Extensionality lzero lzero         

      open import Data.List.Properties using (++-assoc)
  
      term-alg : SO-Alg L (TSem L) 
      term-alg = DeBruijn-Alg L L (All-∈-self L)
      
      pargs-tail : 
        {ts : List Ty0} {θ : Env Ty0 S ts} 
        {a : Sig 0} (as : List (Sig 0)) {a₁ : TInt SigI 0F a} {args₁ : TInt (TupleI 2 SigI) 0F as} {a₂ : TInt SigI 1F a} {args₂ : TInt (TupleI 2 SigI) 1F as}
        -> prop (TupleK 2 (fromList SigK (a ∷ as))) (ts , θ) (toVal {I = TupleI 2 SigI} (a₁ ∷ args₁ , a₂ ∷ args₂)) 
        -> prop (TupleK 2 (fromList SigK as)) (ts , θ) (toVal {I = TupleI 2 SigI} (args₁ , args₂)) 
      pargs-tail {ts} {θ} as {a₁ = a₁} {args₁} {a₂ = a₂} {args₂} (fst , snd) = 
        subst (λ x -> allPointwise 2 (Sig 0) SigI (ts , θ) as (transpose-env 2 x) (fromList SigK as)) (funext eq) snd 
        where 
          eq : ∀ k -> tail (toVal {I = TupleI 2 SigI} (a₁ ∷ args₁ , a₂ ∷ args₂) k) ≡ toVal {I = TupleI 2 SigI} (args₁ , args₂) k 
          eq zero = refl
          eq (suc zero) = refl
  
      data SList (A : Set) : Set where 
        nil : SList A 
        snoc : SList A -> A -> SList A 

      data SEnv (K : Set) (T : K -> Set) : (as : SList K) -> Set where 
        nil  : SEnv K T nil
        snoc : ∀ {as : SList K} {a : K} -> SEnv K T as -> T a -> SEnv K T (snoc as a)

      appSList : {A : Set} -> SList A -> List A -> List A 
      appSList nil ys = ys 
      appSList (snoc xs x) ys = appSList xs (x ∷ ys) 

      SList→List : {A : Set} -> SList A -> List A 
      SList→List xs = appSList xs []

      appSList-++ : {A : Set} (as : SList A) (bs0 bs : List A) -> appSList as (bs0 ++ bs) ≡ appSList as bs0 ++ bs
      appSList-++ nil bs0 bs = refl
      appSList-++ (snoc as x) bs0 bs = appSList-++ as (x ∷ bs0) bs

      appSList-List : {A : Set} (as : SList A) (bs : List A) -> appSList as bs ≡ SList→List as ++ bs
      appSList-List as bs = appSList-++ as [] bs 


      appSEnv : {K : Set} {T : K -> Set} -> {as : SList K} {bs : List K} -> SEnv K T as -> Env K T bs -> Env K T (appSList as bs)
      appSEnv nil ys = ys
      appSEnv (snoc xs x) ys = appSEnv xs (x ∷ ys)

      SEnv→Env : {K : Set} {T : K -> Set} {as : SList K} -> SEnv K T as -> Env K T (SList→List as)
      SEnv→Env xs = appSEnv xs []

      lookup-lemma :
        ∀ {a as v θ bs θ'}
        -> (diffv : (a ∷ as , v ∷ θ) ≲v (bs , θ'))
        -> lookup θ' (weaken-var (proj₁ diffv) (here refl)) ≡ v
      lookup-lemma (≲-refl , ≲e-refl) = refl
      lookup-lemma (Syntax.≲-step diff , ≲e-step diffe) = lookup-lemma (diff , diffe)

      diff-remove : 
        {as0 : SList Ty0} {as : List Ty0}
        (θ₀ : SEnv Ty0 S as0) (θ : Env Ty0 S as) 
        (ash : List Ty0) (θh : Env Ty0 S ash) 
        (ts' : List Ty0) (θ' : Env Ty0 S ts') 
        -> (appSList as0 (ash ++ as) , appSEnv θ₀ (θh ++ₑ θ)) ≲v (ts' , θ')
        -> (ash ++ as , θh ++ₑ θ) ≲v (ts' , θ') 
      diff-remove {nil} nil θ ash θh ts' θ' diffv = diffv
      diff-remove {snoc as0 a} (snoc θ₀ v) θ ash θh ts' θ' diffv with diff-remove {as0} θ₀ θ (a ∷ ash) (v ∷ θh) ts' θ' diffv
      ... | (diffb , diffbθ)  = (≲-trans ≲-one diffb , ≲e-trans (≲e-step ≲e-refl) diffbθ) 

      lemma-x : 
        {a : Ty0} {v' : S a} {as : List Ty0} {θ' : Env Ty0 S as} 
        {ts₂ : List Ty0} {θ₂ : Env Ty0 S ts₂} 
        (as0 : SList Ty0) (θ₀ : SEnv Ty0 S as0) 
        (ts'' : List Ty0) (θ'' : Env Ty0 S ts'')
        -> (appSList as0 (a ∷ as ++ ts₂) , appSEnv θ₀ (v' ∷ (θ' ++ₑ θ₂))) ≲v (ts'' , θ'') 
        -> (tm : DeBruijnTerm L ts'' a)
        -> DeBruijn-Interp.makeX L a (as ++ ts₂) ts'' ≡ just tm 
        -> deBruijn-to-HOAS L tm S alg θ'' ≡ v'
      lemma-x {a} {v'} {as} {θ'} {ts₂} as0 θ₀ ts'' θ'' diff tm eq with a ∷ as ++ ts₂ ≲? ts'' 
      lemma-x {a} {v'} {as} {θ'} {ts₂} as0 θ₀ ts'' θ'' diffv .(Var (Syntax.weaken-var Ty tyVar substTy diff' (here refl))) refl | yes diff' with diff-remove θ₀ (v' ∷ (θ' ++ₑ _)) [] [] ts'' θ'' diffv 
      ... | (diffb , diffbθ) with ≲-canon _ _ diff' diffb 
      ... | refl = lookup-lemma (diffb , diffbθ)  

      lemma-xs : 
         (as : List Ty0) {ts₂ : List Ty0} {θ₂ : Env Ty0 S ts₂} 
         (θ' : Env Ty0 S as)
         -> (as0 : SList Ty0) -> (θ₀ : SEnv Ty0 S as0)
         -> allPointwise 2 (Ty 0) BaseI (appSList as0 (as ++ ts₂) , appSEnv θ₀ (θ' ++ₑ θ₂)) as 
            (transpose-env 2 (toVal {I = TupleI 2 BaseI} (DeBruijn-Interp.makeXs L ts₂ as , θ')))
            (fromList BaseRel as) 
      lemma-xs [] [] _ _ = lift tt
      lemma-xs (a ∷ as) {ts₂} {θ₂} (v' ∷ θ') as0 θ₀ = (λ ts'' diffv tm eq -> lemma-x {θ' = θ'} as0 θ₀ (proj₁ ts'') (proj₂ ts'') diffv tm eq ) ,  subst (λ x -> allPointwise 2 Ty0 BaseI (appSList as0 (a ∷ as ++ ts₂) , appSEnv θ₀ (v' ∷ (θ' ++ₑ θ₂))) as (transpose-env 2 x) (fromList BaseRel as)) (funext eqTail) (lemma-xs as θ' (snoc as0 a) (snoc θ₀ v')) 
        where
          eqTail : 
            ∀ ix -> 
            toVal {I = TupleI 2 BaseI} (DeBruijn-Interp.makeXs L ts₂ as , θ') ix
             ≡ tail (toVal{I = TupleI 2 BaseI} (DeBruijn-Interp.makeXs L ts₂ (a ∷ as) , v' ∷ θ') ix) 
          eqTail zero = refl 
          eqTail (suc zero) = refl
        

      lemma-arg : 
        {a : Ty0} {as : List Ty0} 
        {ts ts₂ : List Ty0} {θ : Env Ty0 S ts} {θ₂ : Env Ty0 S ts₂} 
        -> ( (ts , θ) ≲v (ts₂ , θ₂) ) 
        -> (arg₁ : Env Ty0 (TSem L) as -> TSem L a)
        -> (arg₂ : Env Ty0 S as -> S a) 
        -> (parg : prop (SigK (as ↝ a)) (ts , θ) (toVal {I = SigI} (arg₁ , arg₂)))
        -> (tm : DeBruijnTerm L (as ++ ts₂) a)
        -> (eq : arg₁ (DeBruijn-Interp.makeXs L ts₂ as) (as ++ ts₂) ≡ just tm) 
        -> ∀ θ' -> deBruijn-to-HOAS L tm S alg (θ' ++ₑ θ₂) ≡ arg₂ θ' 
      lemma-arg {a} {as} {ts} {ts₂} {θ} {θ₂} diff arg₁ arg₂ parg tm eq θ'  = 
        parg (as ++ ts₂ , θ' ++ₑ θ₂) (≲v-++ as θ' diff) (toVal {I = TupleI 2 BaseI} (DeBruijn-Interp.makeXs L ts₂ as , θ')) (lemma-xs as {θ₂ = θ₂} θ' nil nil ) _ ≲v-refl tm eq 
        -- parg _ diff (toVal {I = TupleI 2 BaseI} (DeBruijn-Interp.makeXs L ts₂ as , θ')) {!!} (as ++ ts₂ , θ' ++ₑ θ₂) {!!} tm eq

      lemma-args :
        {ts ts₂ : List Ty0} {θ : Env Ty0 S ts} {θ₂ : Env Ty0 S ts₂} 
        (as : List (Sig 0)) 
        -> ( (ts , θ) ≲v (ts₂ , θ₂) ) 
        -> (args : Val (TupleI 2 SigI) as) 
        -> (pargs : prop (TupleK 2 (Env.fromList SigK as)) (ts , θ) args) 
        -> (tms : Env (Sig 0) (opArgTerm (DeBruijnTerm L) ts₂) as)
        -> (eq : mapEnvMaybe (DeBruijn-Interp.makeArg L ts₂) (args 0F) ≡ just tms)
        -> Env.map (deBruijn-to-HOAS-arg L S alg θ₂) tms ≡ args 1F 
      lemma-args [] diff args pargs [] eqt with args 1F 
      ... | [] = refl 
      lemma-args {ts₂ = ts₂} (a ∷ as) diff args pargs tms eqt with args 0F in eq0 | args 1F in eq1
      lemma-args {ts₂ = ts₂} (a ∷ as) diff args pargs (tm ∷ tms) eqt | arg₁ ∷ args₁ | arg₂ ∷ args₂
        with DeBruijn-Interp.makeArg L ts₂ a arg₁ in eqMakeArg
      ... | just _ 
        with mapEnvMaybe (DeBruijn-Interp.makeArg L ts₂) args₁ in eqMakeArgs 
      lemma-args {ts} {ts₂ = ts₂} {θ} (a ∷ as) diff args (parg , pargs) (_ ∷ _) refl | arg₁ ∷ args₁ | arg₂ ∷ args₂ | just _ | just _  = Eq.cong₂ _∷_ (funext (lemma-arg diff arg₁ arg₂ parg _ eqMakeArg)) (lemma-args as diff (toVal {I = TupleI 2 SigI} ( args₁ , args₂ )) pargs' _ eqMakeArgs )
        where
          eqA : ∀ k -> tail (args k) ≡ toVal {I = TupleI 2 SigI} (args₁ , args₂) k
          eqA zero = cong tail eq0
          eqA (suc zero) = cong tail eq1
          pargs' : prop (TupleK 2 (fromList SigK as)) (ts , θ) (toVal {I = TupleI 2 SigI} (args₁ , args₂)) 
          pargs' rewrite eq0 = subst (λ x -> prop (TupleK 2 (fromList SigK as)) (ts , θ) x) (funext eqA) pargs   -- subst (λ x -> prop (TupleK 2 (fromList SigK (a ∷ as))) (ts , θ) x) {!!} pargs  



      lemma-op : 
        (csig : ConstructSig) (pcsig : csig ∈ L) (opF : opFun S csig) (lkup : lookup alg pcsig ≡ opF)
        -> prop (CSigK csig) ([] , []) (λ k -> toVal {I = CSigI} (DeBruijn-Op L csig pcsig  , opF) k)
      lemma-op csig pcsig opF lkup tsub (ts , θ) diff args pargs (ts₂ , θ₂) diff₂ tm eq 
        with mapEnvMaybe (DeBruijn-Interp.makeArg L ts₂) (args 0F) in eqt
      lemma-op csig pcsig opF refl tsub (ts , θ) diff args pargs (ts₂ , θ₂) diff₂ .(Con tsub pcsig tms) refl | just tms = cong (lookup alg pcsig tsub) (lemma-args (L.map (substTySig tsub) (ConstructSig.args csig)) (diff₂) args pargs tms eqt)

      lookups : {A : Set} {L : List A} {LL : List A} {F : A -> Set} -> Env A F L -> All (_∈ L) LL -> Env A F LL
      lookups e [] = []
      lookups e (px ∷ ixs) = lookup e px ∷ lookups e ixs

      lookups-lemma : lookups alg (All-∈-self L) ≡ alg 
      lookups-lemma = go (All-∈-self L) (λ x -> x) (λ x -> x) (λ ix xs -> refl) All-∈-self-canon alg
        where
          go : {LL : List ConstructSig} (all-in-LL : All (_∈ L) LL)
               -> (h : ∀ {x} -> x ∈ LL -> x ∈ L) (g : SO-Alg LL S -> SO-Alg L S)
               -> (∀ {x} -> (index : x ∈ LL) -> (xs : SO-Alg LL S) -> lookup (g xs) (h index) ≡ lookup xs index)
               -> (∀ {x} -> (index : x ∈ LL) -> All.lookup all-in-LL index ≡ h index) 
               -> ∀ salg -> lookups (g salg) all-in-LL ≡ salg
          go [] h g pgh p [] = refl
          go {_ ∷ LL} (px ∷ all-in-LL) h g pgh p (op ∷ alg)
            with go all-in-LL (λ ix -> h (there ix)) (λ xs -> g (op ∷ xs)) (λ ix xs -> pgh (there ix) (op ∷ xs)) p' alg
             where 
               p' : ∀ {x} -> (index : x ∈ LL) -> All.lookup all-in-LL index ≡ h (there index)
               p' index = p (there index)
          ... | eq with p (here refl) | pgh (here refl) (op ∷ alg)
          ... | refl | eqh = Eq.cong₂ _∷_ eqh eq

      lemma : prop (LangSigK L) ([] , []) (toVal {I = LangSigI} (term-alg , alg))
      lemma = go L (All-∈-self L) alg lookups-lemma
        where 
          go : (LL : List ConstructSig) 
               -> (all-∈-L : All (_∈ L) LL)
               -> (salg : SO-Alg LL S)
               -> (lkups : lookups alg all-∈-L ≡ salg)
               -> prop (LangSigK LL) ([] , []) (toVal {I = LangSigI} (DeBruijn-Alg L LL all-∈-L , salg))
          go [] all-∈-L salg _ = lift tt
          go (csig ∷ LL) (pcsig ∷ all-∈-L) (opF ∷ salg) lkups = 
               subst (λ x -> prop (CSigK csig) ([] , []) x) (sym eqHead) (lemma-op csig pcsig opF (Env.∷-injectiveˡ lkups))
             , subst (λ x -> allPointwise 2 _ _ ([] , []) LL (transpose-env 2 x) (fromList CSigK LL)) (sym eqTail) (go LL all-∈-L salg (Env.∷-injectiveʳ lkups))
            where
              eqTail : 
                   (λ k -> tail (toVal {I = LangSigI} (DeBruijn-Alg L (csig ∷ LL) (pcsig ∷ all-∈-L) , opF ∷ salg) k))
                   ≡ (λ k -> toVal {I = LangSigI} (DeBruijn-Alg L LL all-∈-L , salg) k) 
              eqTail = funext λ { zero -> refl ; (suc zero) -> refl } 

              eqHead : 
                   (λ k -> head (toVal {I = LangSigI} (DeBruijn-Alg L (csig ∷ LL) (pcsig ∷ all-∈-L) , opF ∷ salg) k))
                   ≡ (λ k -> toVal {I = CSigI} (DeBruijn-Op L csig pcsig  , opF) k)
              eqHead = funext λ { zero -> refl ; (suc zero) -> refl } 

      HOAS-term-HOAS : 
        {A : Ty0} (f : Poly L A) (tm : DeBruijnTerm L [] A)
        -> HOAS-to-deBruijn L f [] ≡ just tm 
        -> deBruijn-to-HOAS L tm S alg [] ≡ f S alg 
      HOAS-term-HOAS {A} f tm h2dEq = free-theorem₂ A ([] , []) (f (TSem L)) (f S) ([] , []) ≲v-refl (toVal {I = LangSigI} (term-alg , alg)) lemma ([] , []) ≲v-refl tm h2dEq 



    
                                
             
