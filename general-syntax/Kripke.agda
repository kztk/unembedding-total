{-# OPTIONS --without-K #-}

open import Data.Nat using (ℕ)
open import Data.Fin using (Fin) renaming (suc to fsuc ; zero to fzero) 
open import Data.Vec using (Vec ; [] ; _∷_) 

module Kripke
  (Ty : ℕ -> Set)
  (tyVar : {n : ℕ} -> Fin n -> Ty n)
  (substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0) 
  where 

private 
  Ty0 : Set 
  Ty0 = Ty 0 

open import Level renaming (suc to lsuc ; zero to lzero)


open import Env 
open import Sig Ty tyVar substTy as Sig

open import Relation.Binary.Bundles using (Preorder)
open import Data.List as L using (List; [] ; _∷_ )
open import Data.Product

module Kripke (WorldPO : Preorder lzero lzero lzero) where 
  open Preorder WorldPO renaming (_∼_ to _≲_)

  private
    variable
      ℓ ℓ' ℓ'' ℓᵢ ℓₚ : Level 

  data Tuple : {n : ℕ} (vs : Vec Set n) -> Set₁ where
    []  : Tuple []
    _∷_ : {n : ℕ} {v : Set} {vs : Vec Set n} -> v -> Tuple vs -> Tuple (v ∷ vs)

  module _ (arity : ℕ) where 

    record USem (Universe : Set ℓ) : Set (ℓ ⊔ lsuc ℓᵢ) where 
      field 
        TInt : Fin arity -> Universe -> Set ℓᵢ

      Val : Universe -> Set ℓᵢ
      Val A = (ix : Fin arity) -> TInt ix A 

    record KRel (U : Set ℓ) (I : USem {ℓ} {ℓᵢ} U) (A : U) : Set (ℓᵢ ⊔ lsuc ℓₚ)  where 
      open USem I 
      field 
        prop : Carrier -> Val A -> Set ℓₚ
        prop-kripke : ∀ (w w' : Carrier) -> w ≲ w' -> ∀ (a : Val A) -> prop w a -> prop w' a 

    TupleI : {U : Set ℓ} -> USem {ℓ} {ℓ'} U -> USem {ℓ} {ℓ ⊔ ℓ'}  (List U) 
    USem.TInt (TupleI {U = U} I) ix as = Env U (USem.TInt I ix) as 

    open import Data.Unit 
    open import Function using (case_of_)
    open import Relation.Binary.PropositionalEquality as Eq using (_≡_)


    transpose-env-aux :
      {U : Set ℓ} {I : USem {ℓ} {ℓ'} U} {As Bs : List U}
      (f : {T : U -> Set ℓ'} ->  Env U T As -> Env U T Bs)
      -> ((k : Fin arity) -> Env U (USem.TInt I k) As) -> Env U (λ A -> ∀ k -> USem.TInt I k A) Bs
    transpose-env-aux {Bs = []} f h = []
    transpose-env-aux {Bs = x ∷ Bs} f h = ( λ k -> Env.head (f (h k))  ) ∷ transpose-env-aux {Bs = Bs} (λ zs -> Env.tail (f zs)) h
    

    transpose-env : 
      {U : Set ℓ} {I : USem {ℓ} {ℓ'} U} {As : List U}
      -> ((k : Fin arity) -> Env U (USem.TInt I k) As) -> Env U (λ A -> ∀ k -> USem.TInt I k A) As
--    transpose-env h = transpose-env-aux (λ zs -> zs) h
    transpose-env {As = []} h = []
    transpose-env {As = A ∷ As} h = 
      (λ k -> Env.head (h k)) ∷ transpose-env {As = As} (λ k -> Env.tail (h k))

    allPointwise : (U : Set ℓ) (I : USem {ℓ} {ℓ'} U) -> (w : Carrier) -> (ts : List U) -> Env U (λ A -> ∀ k -> USem.TInt I k A) ts -> Env U (KRel {ℓ} {ℓ'} {ℓₚ} U I) ts -> Set ℓₚ 
    allPointwise _ _ w [] [] [] = Lift _ ⊤ 
    allPointwise _ _ w (t ∷ ts) (a ∷ as) (p ∷ ps) =  KRel.prop p w a × allPointwise _ _ w ts as ps 

    TupleK : {U : Set ℓ} {I : USem {ℓ} {ℓ'} U} {As : List U} -> Env _ (KRel {ℓ} {ℓ'} {ℓₚ} U I) As -> KRel {ℓ} {ℓ ⊔ ℓ'} {ℓₚ} (List U) (TupleI I) As 
    KRel.prop (TupleK {U = U} {I} krels) w as = allPointwise U I w _ (transpose-env as) krels 
    KRel.prop-kripke (TupleK {U = U} {I} krels) w w' w≲w' as = lemma _ (transpose-env as) krels 
      where
        lemma : 
          (Bs : List U) 
          -> (as' : Env U (λ A -> ∀ k -> USem.TInt I k A) Bs)
          -> (krels : Env U (KRel U I) Bs)
          -> allPointwise U I w Bs as' krels -> allPointwise U I w' Bs as' krels 
        lemma [] [] [] _ = lift tt
        lemma (B ∷ Bs) (a' ∷ as') (krel ∷ krels) (p , ps) = KRel.prop-kripke krel _ _ w≲w' a' p , lemma Bs as' krels ps  

--     IndexedTupleI : 
--       {K : Set} {U : K -> Set}
--       -> (I : (k : K) -> USem (U k))
--       -> (ks : List K) 
--       -> USem (Env K U ks)
--     USem.TInt (IndexedTupleI {K} {U} I ks) ix ts = EnvEnv K U (λ k u -> USem.TInt (I k) ix u) ks ts 

--     transpose-envenv : 
--       {K : Set} {U : K -> Set} {I : (k : K) -> USem (U k)} {ks : List K} {As : Env K U ks}
--       -> ((ix : Fin arity) -> EnvEnv K U (λ k -> USem.TInt (I k) ix) ks As)
--       -> EnvEnv K U (λ k A -> ∀ ix -> USem.TInt (I k) ix A) ks As
--     transpose-envenv {ks = []} {[]} h = []
--     transpose-envenv {ks = k ∷ ks} {A ∷ As} h = A # (λ ix -> case h ix of (λ where (_ # a ∷ _) -> a)) ∷ transpose-envenv {ks = ks} {As} (λ ix -> case h ix of (λ where (_ # _ ∷ as) -> as))

--     allPointwise-envenv : 
--       {K : Set} {U : K -> Set} {I : (k : K) -> USem (U k)} {ks : List K} {As : Env K U ks}
--       -> (w : Carrier)
--       -> EnvEnv K U (λ k A -> ∀ ix -> USem.TInt (I k) ix A) ks As
--       -> EnvEnv K U (λ k -> KRel (U k) (I k)) ks As
--       -> Set 
--     allPointwise-envenv w [] [] = ⊤
--     allPointwise-envenv w (_ # v ∷ vs) (_ # krel ∷ krels) = KRel.prop krel w v × allPointwise-envenv w vs krels


--     IndexedTupleK : 
--       {K : Set} {U : K -> Set}
--       -> (I : (k : K) -> USem (U k))
--       -> {ks : List K} 
--       -> {As : Env K U ks} 
--       -> EnvEnv K U (λ k -> KRel (U k) (I k)) ks As
--       -> KRel (Env K U ks) (IndexedTupleI I ks) As
--     KRel.prop (IndexedTupleK I krels) w h = allPointwise-envenv w (transpose-envenv h) krels
--     KRel.prop-kripke (IndexedTupleK {K} {U} I krels) w w' w≲w' h = lemma (transpose-envenv h) krels
--       where
--         lemma : 
--           {ks : List K} {As : Env K U ks} 
--           -> (vs : EnvEnv K U (λ k A -> ∀ ix -> USem.TInt (I k) ix A) ks As)
--           -> (krels : EnvEnv K U (λ k -> KRel (U k) (I k)) ks As)
--           -> allPointwise-envenv w vs krels -> allPointwise-envenv w' vs krels 
--         lemma [] [] allpointwise = tt
--         lemma (_ # v ∷ vs) (_ # krel ∷ krels) (p , allpointwise) = (KRel.prop-kripke krel w w' w≲w' v p) , (lemma vs krels allpointwise)

        

    ArrI : {U U' : Set ℓ} -> USem {ℓᵢ = ℓ'} U -> USem {ℓᵢ = ℓ'} U' -> USem {ℓᵢ = ℓ'} (U × U') 
    USem.TInt (ArrI {U} I I') ix (dom , cod) = USem.TInt I ix dom -> USem.TInt I' ix cod

    ArrK :
      {U U' : Set ℓ} {I : USem {ℓᵢ = ℓᵢ} U} {I' : USem {ℓᵢ = ℓᵢ} U'} {A : U} {B : U'}
      -> KRel {ℓₚ = ℓₚ} U I A -> KRel {ℓₚ = ℓₚ} U' I' B -> KRel {ℓₚ = ℓᵢ ⊔ ℓₚ} (U × U') (ArrI I I') (A , B)
    KRel.prop (ArrK {U} {I} k₁ k₂) w f = ∀ (w' : Carrier) -> w ≲ w' -> ∀ a -> KRel.prop k₁ w' a -> KRel.prop k₂ w' (λ ix -> f ix (a ix)) 
    KRel.prop-kripke (ArrK {U} {I} dkrel rkrel) w w' w≲w' f kf w'' w'≲w'' = kf w'' (trans w≲w' w'≲w'')

    PolyI : {B : Set ℓ} {U : Set ℓ'} -> (B -> USem {ℓᵢ = ℓᵢ} U) -> USem (B -> U) 
    USem.TInt (PolyI f) ix u = ∀ b -> USem.TInt (f b) ix (u b)

    PolyK : 
      {B : Set ℓ} {U : Set ℓ'} 
      -> (I : B -> USem {ℓᵢ = ℓᵢ} U)
      -> (P : B -> U)  
      -> ((b : B) -> KRel {ℓₚ = ℓₚ} U (I b) (P b))
      -> KRel {ℓₚ = ℓ ⊔ ℓₚ} (B -> U) (PolyI I) P
    KRel.prop (PolyK {U} I P krel) w h = ∀ b -> KRel.prop (krel b) w (λ ix -> h ix b) 
    KRel.prop-kripke (PolyK {U} I P krel) w w' w≲w' h hp b =  KRel.prop-kripke (krel b) w w' w≲w' (λ ix -> h ix b) (hp b)

    {-
      Originally, 
      forallTy1 : 
        ∀ {ℓ ℓ'} {P : (Ty -> Set ℓ) -> Set ℓ'} ->
        (∀ {T} -> (∀ A -> KRel (T A)) -> KRel (P T)) ->
        KRel (∀ (T : Ty -> Set ℓ) -> P T)

      - In our system, the information of T is played by I.
      - It models ∀ T. F[T] and this should be understood as a signature. 
       
    -}
    PolyI1 : {ℓ ℓᵢ : Level} {U : Set ℓ} -> USem {ℓᵢ = ℓ ⊔ lsuc ℓᵢ} (USem {ℓᵢ = ℓᵢ} U -> Fin arity -> Set (ℓ ⊔ lsuc ℓᵢ)) 
    USem.TInt (PolyI1 {U = U}) ix P = (I : USem U) -> P I ix 

    IdI : USem (Fin arity -> Set ℓ)
    IdI .USem.TInt ix A = A ix

    PolyK1 : 
      {ℓ ℓᵢ ℓₚ ℓₚ' : Level} {U : Set ℓ} 
      -> (P : USem {ℓᵢ = ℓᵢ} U -> Fin arity -> Set (ℓ ⊔ lsuc ℓᵢ)) 
      -> (K : (I : USem {ℓᵢ = ℓᵢ} U) ->  ((A : U) -> KRel {ℓₚ = ℓₚ} U I A) -> KRel {ℓₚ = ℓₚ'} (Fin arity -> Set (ℓ ⊔ lsuc ℓᵢ)) IdI (P I))
      -> KRel _ PolyI1 P
    KRel.prop (PolyK1 P K) w f = ∀ I r -> KRel.prop (K I r) w  λ ix -> f ix I 
    KRel.prop-kripke (PolyK1 P K) _ _ 1≲2 h ph I r = KRel.prop-kripke (K I r) _ _ 1≲2 (λ ix -> h ix I) (ph I r)

    -- PolyK1 : 
    --   {HI : (I : USem U) -> USem U'}
    --   -> (P : U -> U')
    --   -> (∀ {I} -> ((A : U) -> KRel U I A) -> KRel (U -> U))
    --   -> KRel (U -> U') (∀ I -> HI I) P 
  
-- --     module KInt (I : USem Ty0) (BaseInt : (A : Ty0) -> KRel Ty0 I A) where    

--     Sig-src : {n : ℕ} -> Sig n -> List (Ty n)
--     Sig-src (as ↝ _) = as 

--     Sig-dst : {n : ℕ} -> Sig n -> Ty n 
--     Sig-dst (_  ↝ a) = a 

    module KIntL {i} (I : USem {ℓᵢ = i} Ty0) (BaseRel : (A : Ty0) -> KRel {ℓₚ = i} Ty0 I A) where 

      SigK-impl : 
        (sig : Sig 0)
        -> KRel (List Ty0 × Ty0) (ArrI (TupleI I) I) (Sig.src sig , Sig.dst sig)
      SigK-impl (as ↝ a) = 
        ArrK (TupleK (Env.fromList BaseRel as)) (BaseRel a)

     
      SigI : USem (Sig 0)
      USem.TInt SigI ix (as ↝ a) = USem.TInt (ArrI (TupleI I) I) ix (as , a)

      SigK : (sig : Sig 0) -> KRel (Sig 0) SigI sig
      KRel.prop (SigK sig@(as ↝ a)) = KRel.prop (SigK-impl sig)
      KRel.prop-kripke (SigK sig@(as ↝ a)) = KRel.prop-kripke (SigK-impl sig)

      SigI-n : {n : ℕ} (tsub : Vec Ty0 n) -> USem (Sig n)
      USem.TInt (SigI-n tsub) ix sig = USem.TInt SigI ix (substTySig tsub sig) -- (ArrI (TupleI I) I) ix (L.map (substTy tsub) as , substTy tsub a)
      SigK-n : {n : ℕ} (tsub : Vec Ty0 n) (sig : Sig n)
               -> KRel (Sig n) (SigI-n tsub) sig
      KRel.prop (SigK-n tsub sig) = KRel.prop (SigK (substTySig tsub sig)) 
      KRel.prop-kripke (SigK-n tsub sig) = KRel.prop-kripke (SigK (substTySig tsub sig)) 

--       CSigI' : USem ConstructSig 
--       USem.TInt CSigI' ix (CSig n ss a) = USem.TInt (PolyI (λ tsub -> ArrI (TupleI (SigI-n tsub)) I)) ix (λ tsub -> ss , substTy tsub a)

--       module _ where 
--         open ConstructSig
--         private
--           CSigKImpl' :
--             (csig : ConstructSig)
--             -> KRel (Vec Ty0 (nargs csig) -> List (Sig (nargs csig)) × Ty 0)
--                     (PolyI (λ tsub -> ArrI (TupleI (SigI-n tsub)) I))
--                     (λ tsub -> args csig , substTy tsub (result csig))
--           CSigKImpl' (CSig n ss a) =
--             PolyK (λ tsub → ArrI (TupleI (SigI-n tsub)) I) _ 
--             λ tsub -> ArrK (TupleK (Env.fromList (SigK-n tsub) ss)) (BaseRel (substTy tsub a))  
 
--         CSigK' : (csig : ConstructSig) -> KRel ConstructSig CSigI' csig
--         KRel.prop (CSigK' (CSig n ss a)) w vs = KRel.prop (CSigKImpl' (CSig n ss a)) w vs
--         KRel.prop-kripke (CSigK' (CSig n ss a)) w w' w≲w' vs = KRel.prop-kripke (CSigKImpl' (CSig n ss a)) w w' w≲w' vs

      CSigI : USem ConstructSig 
      USem.TInt CSigI ix (CSig n ss a) = USem.TInt (PolyI (λ tsub -> ArrI (TupleI SigI) I)) ix (λ tsub -> (L.map (substTySig tsub) ss , substTy tsub a))

      module _ where 
        open ConstructSig 
        private
          CSigKImpl : 
            (csig : ConstructSig)
            -> KRel {ℓₚ = i} 
                    (Vec Ty0 (nargs csig) -> List (Sig 0) × Ty 0)
                    (PolyI (λ tsub -> ArrI (TupleI SigI) I))
                    (λ tsub -> L.map (substTySig tsub) (args csig) , substTy tsub (result csig))
          CSigKImpl (CSig n ss a) = 
            PolyK _ _ λ tsub -> ArrK (TupleK (Env.fromList SigK (L.map (substTySig tsub) ss))) (BaseRel (substTy tsub a)) -- (BaseRel (substTy tsub a))
      
        CSigK : (csig : ConstructSig) -> KRel ConstructSig CSigI csig
        KRel.prop (CSigK (Sig.CSig n ss a)) = KRel.prop (CSigKImpl (CSig n ss a)) 
        KRel.prop-kripke (CSigK (Sig.CSig n ss a)) = KRel.prop-kripke (CSigKImpl (CSig n ss a))

      LangSigI : USem (List ConstructSig) 
      LangSigI = TupleI CSigI

      LangSigK : (L : LangSig) -> KRel LangSig LangSigI L 
      LangSigK L = TupleK (Env.fromList CSigK L) 

      ExpI : USem (LangSig × Ty0)
      ExpI = ArrI LangSigI I 

      ExpK : (L : LangSig) -> (t : Ty0) -> KRel (LangSig × Ty0) ExpI (L , t)
      ExpK L t = ArrK (LangSigK L) (BaseRel t)

    -- PExpI : USem {!!}
    -- PExpI = PolyI1 

    -- PExpK : KRel _ PExpI {!!}
    -- PExpK = PolyK1 {!λ I ix -> USem.TInt (KIntL.ExpI I ?) ix!} {!!}

  module Unary where     

    Val₁ : {U : Set} -> (I : USem 1 U) -> U -> Set 
    Val₁ I A = USem.TInt I fzero A 

    fromVal : {U : Set} {I : USem 1 U} {A : U} -> USem.Val I A -> Val₁ I A 
    fromVal v = v fzero 

    toVal : {U : Set} {I : USem 1 U} {A : U} -> Val₁ I A -> USem.Val I A 
    toVal v = λ where fzero -> v 

    from-prop : {U : Set} {I : USem 1 U} {A : U} (P : Carrier -> USem.Val I A -> Set ℓₚ) -> Carrier -> Val₁ I A -> Set ℓₚ
    from-prop {I = I} P w v = P w  (toVal {I = I} v)
  
    to-prop : {U : Set} {I : USem 1 U} {A : U} (P : Carrier -> Val₁ I A -> Set ℓₚ) -> Carrier -> USem.Val I A -> Set ℓₚ
    to-prop {I = I} P w v = P w (fromVal {I = I} v)

    prop₁ : {U : Set} {I : USem 1 U} {A : U} -> KRel 1 {ℓₚ = ℓₚ} U I A -> Carrier -> Val₁ I A -> Set ℓₚ
    prop₁ {U = U} {I} k = from-prop {U = U} {I} (KRel.prop k) 

    prop₁-kripke : {U : Set} {I : USem 1 U} {A : U} -> (kr : KRel 1 {ℓₚ = ℓₚ} U I A) -> ∀ (w w' : Carrier) -> w ≲ w' -> ∀ (a : Val₁ I A) -> prop₁ kr w a -> prop₁ kr w' a 
    prop₁-kripke {I = I} k w w' w≲w' v pw = KRel.prop-kripke k w w' w≲w' (toVal {I = I} v) pw 


  module Binary where     

    Dom₁ : {U : Set} -> (I : USem 2 U) -> U -> Set 
    Dom₁ I A = USem.TInt I fzero A 

    Dom₂ : {U : Set} -> (I : USem 2 U) -> U -> Set 
    Dom₂ I A = USem.TInt I (fsuc fzero) A 

    Val₂ : {U : Set} -> (I : USem 2 U) -> U -> Set 
    Val₂ I A = USem.TInt I fzero A × USem.TInt I (fsuc fzero) A

    -- record KRel₂ (U : Set) (I : USem 2 U) (A : U) : Set₁  where 
    --   open USem I 
    
    --   field 
    --     prop₂ : Carrier -> Val₂ I A -> Set 
    --     prop-kripke₂ : ∀ (w w' : Carrier) -> w ≲ w' -> ∀ (a : Val₂ I A) -> prop₂ w a -> prop₂ w' a 

    fromVal : {U : Set} {I : USem 2 U} {A : U} -> USem.Val I A -> Val₂ I A 
    fromVal v = (v fzero  , v (fsuc fzero))

    toVal : {U : Set} {I : USem 2 U} {A : U} -> Val₂ I A -> USem.Val I A 
    toVal (v₁ , v₂) = λ where 
          fzero -> v₁ 
          (fsuc fzero) -> v₂ 

    from-prop : {U : Set} {I : USem 2 U} {A : U} (P : Carrier -> USem.Val I A -> Set ℓₚ) -> Carrier -> Val₂ I A -> Set ℓₚ
    from-prop {I = I} P w v = P w  (toVal {I = I} v)
  
    to-prop : {U : Set} {I : USem 2 U} {A : U} (P : Carrier -> Val₂ I A -> Set ℓₚ) -> Carrier -> USem.Val I A -> Set ℓₚ
    to-prop {I = I} P w v = P w (fromVal {I = I} v)

    prop₂ : {U : Set} {I : USem 2 U} {A : U} -> KRel 2 {ℓₚ = ℓₚ} U I A -> Carrier -> Val₂ I A -> Set ℓₚ
    prop₂ {U = U} {I} k = from-prop {U = U} {I} (KRel.prop k) 

    prop₁-kripke₂ : {U : Set} {I : USem 2 U} {A : U} -> (kr : KRel 2 {ℓₚ = ℓₚ} U I A) -> ∀ (w w' : Carrier) -> w ≲ w' -> ∀ (a : Val₂ I A) -> prop₂ kr w a -> prop₂ kr w' a 
    prop₁-kripke₂ {I = I} k w w' w≲w' v pw = KRel.prop-kripke k w w' w≲w' (toVal {I = I} v) pw 


    -- KRel₂→KRel : {U : Set} {I : USem 2 U} {A : U} -> KRel₂ U I A -> KRel 2 U I A 
    -- KRel.prop (KRel₂→KRel {I = I} k) v w = to-prop {I = I} (KRel₂.prop₂ k) v w
    -- KRel.prop-kripke (KRel₂→KRel {I = I} k) w w' w≲w' v pw = KRel₂.prop-kripke₂ k w w' w≲w' (fromVal {I = I} v) pw

    -- KRel→KRel₂ : {U : Set} {I : USem 2 U} {A : U} -> KRel 2 U I A -> KRel₂ U I A 
    -- KRel₂.prop₂ (KRel→KRel₂ {I = I} k) = from-prop {I = I} (KRel.prop k)
    -- KRel₂.prop-kripke₂ (KRel→KRel₂ {I = I} k) w w' w≲w' v pw = KRel.prop-kripke k w w' w≲w' (toVal {I = I} v) pw

    -- module KIntL₂ (I : USem 2 Ty0) (BaseRel : (A : Ty0) -> KRel₂ Ty0 I A) where  
    --   open KIntL 2 I (λ A -> KRel₂→KRel (BaseRel A))

    --   SigK₂ : (sig : Sig 0) -> KRel₂ (Sig 0) SigI sig
    --   SigK₂ sig = KRel→KRel₂ (SigK sig)

    --   CSigK₂ : (csig : ConstructSig) -> KRel₂ ConstructSig CSigI csig
    --   CSigK₂ csig = KRel→KRel₂ (CSigK csig)

    --   LangSigK₂ : (L : LangSig) -> KRel₂ LangSig LangSigI L 
    --   LangSigK₂ L = KRel→KRel₂ (LangSigK L)

    --   ExpI₂ : USem 2 (LangSig × Ty0)
    --   ExpI₂ = ArrI 2 LangSigI I 

    --   ExpK₂ : (L : LangSig) -> (t : Ty0) -> KRel₂ (LangSig × Ty0) ExpI₂ (L , t) 
    --   ExpK₂ L t = KRel→KRel₂ (ExpK L t)
      
