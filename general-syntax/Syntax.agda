{-# OPTIONS --without-K --sized-types #-}

open import Size

open import Env renaming (_++_ to _++ₑ_)

open import Data.Nat using (ℕ ; suc ; zero ; _<_ ; _≤_ )
open import Data.Fin using (Fin)
open import Data.Vec using (Vec ; [] ; _∷_) 

module Syntax (Ty : ℕ -> Set)
              (tyVar : {n : ℕ} -> Fin n -> Ty n)
              (substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0) where 

Ty0 : Set 
Ty0 = Ty 0 

open import Level
open import Sig Ty tyVar substTy 
open import Data.List as List using (_++_ ; [] ; _∷_ ; List) 



-- Sig-as-Term : ∀ {n : ℕ} (tsub : Vec Ty0 n) (TermT : List Ty0 -> Ty0 -> Set) -> List Ty0 -> Sig n -> Set 
-- Sig-as-Term tsub TermT tenv (ts ↝ t) = TermT (Data.List.map (substTy tsub) ts ++ tenv) (substTy tsub t)

opArgTerm : (TermT : List Ty0 -> Ty0 -> Set) -> List Ty0 -> Sig 0 -> Set 
opArgTerm TermT tenv (ts ↝ t) = TermT (ts ++ tenv) t 

-- opArgType : ∀ {ℓ} {n : ℕ} (sub : Vec Ty0 n) -> (S : Ty0 -> Set ℓ) -> Sig n -> Set ℓ 
-- opArgType tsub S (ts ↝ t) = Env Ty0 S (Data.List.map (substTy tsub) ts) -> S (substTy tsub t)

opArgFun : ∀ {ℓ} (S : Ty0 -> Set ℓ) -> Sig 0 -> Set ℓ 
opArgFun S (as ↝ a) = Env Ty0 S as -> S a 

open import Data.List.Membership.Propositional using (_∈_)

data DeBruijnTerm (L : LangSig) : {n : Size} -> List Ty0 -> Ty0 -> Set where 
  Var : 
    ∀ {a tenv} {s : Size}
    -> a ∈ tenv
    -> DeBruijnTerm L {s} tenv a 
  Con :
    ∀ {tenv} {n : ℕ} {ss : List (Sig n)} {a : Ty n} {s : Size}
    -> (tsub : Vec Ty0 n) 
    -> (construct : CSig n ss a ∈ L)
    -> (args : Env (Sig 0) (opArgTerm (DeBruijnTerm L {s}) tenv) (List.map (substTySig tsub) ss))
    -> DeBruijnTerm L {↑ s} tenv (substTy tsub a)

import Data.List.Extrema.Nat as Listℕ 

-- termSizeOfArg : ∀ {L : LangSig} {tenv : List Ty0} {n : ℕ} {tsub : Vec Ty0 n} {sz : Size} (a : Sig n) -> opArgTerm tsub (DeBruijnTerm L {sz}) tenv a -> ℕ 
termSizeOfArg : {L : LangSig} {tenv : List Ty0} {sz : Size} (a : Sig 0) -> opArgTerm (DeBruijnTerm L {sz}) tenv a -> ℕ
termSize : {L : LangSig} {ts : List Ty0} {t : Ty0} {sz : Size} -> DeBruijnTerm L {sz} ts t -> ℕ 

termSizeOfArg {sz = sz} (_ ↝ _) t = termSize {sz = sz} t 

termSize (Var _) = ℕ.suc ℕ.zero 
termSize {sz = .(↑ s)} (Con {s = s} _ _ args) = ℕ.suc (Listℕ.max ℕ.zero (Env.mapToList (termSizeOfArg {sz = s}) args))

module _ where
  open import Data.List.Relation.Unary.All as All 

  -- Property that says that the size of a proper subexpression must be smaller than its parent. 
  termSize-prop-All : 
    ∀ {tenv : List Ty0} {n} {ss : List (Sig n)} {a : Ty n} {tsub : Vec Ty0 n} {L : LangSig} {ix  : CSig n ss a ∈ L} 
    (args : Env (Sig 0) (opArgTerm (DeBruijnTerm L) tenv) (List.map (substTySig tsub) ss))
    -> All (_< termSize (Con _ ix args)) (Env.mapToList termSizeOfArg args)
  termSize-prop-All {ix = ix} args = lemma2 
    where
      lemma : All (_≤ Listℕ.max ℕ.zero (Env.mapToList termSizeOfArg args)) (Env.mapToList termSizeOfArg args)
      lemma = Listℕ.xs≤max ℕ.zero (Env.mapToList termSizeOfArg args) 

      lemma2 : All (_< termSize (Con _ ix args)) (Env.mapToList termSizeOfArg args)
      lemma2 = All.map (λ x≤ → Data.Nat.s≤s x≤) lemma 

  termSize-prop : 
    ∀ {tenv : List Ty0} {n} {ss : List (Sig n)} {a : Ty n} {tsub : Vec Ty0 n} {L : LangSig} {ix  : CSig n ss a ∈ L} 
    (args : Env (Sig 0) (opArgTerm (DeBruijnTerm L) tenv) (List.map (substTySig tsub) ss))
    -> AllEnv _ _ (λ a t -> termSizeOfArg a t < termSize (Con tsub ix args)) args 
  termSize-prop {ix = ix} args = Env.All-mapToList-AllEnv termSizeOfArg (_< termSize (Con _ ix args)) args (termSize-prop-All {ix = ix} args)

  termSize-step : 
    ∀ {tenv : List Ty0} {n} {ss : List (Sig n)} {a : Ty n} {tsub : Vec Ty0 n} {L : LangSig} {ix  : CSig n ss a ∈ L} {k : ℕ}
    (args : Env (Sig 0) (opArgTerm (DeBruijnTerm L) tenv) (List.map (substTySig tsub) ss))
    -> termSize (Con _ ix args) ≤ k 
    -> All (_< k) (Env.mapToList termSizeOfArg args)
  termSize-step {ix = ix} args rel = All.map (λ x -> ≤-trans x rel) (termSize-prop-All {ix = ix} args)
    where 
      open import Data.Nat.Properties

-- ConstructSig-as-HOAS : ∀ {ℓ} (S : Ty0 -> Set ℓ) -> ConstructSig -> Set ℓ 
-- ConstructSig-as-HOAS S (CSig n ss t) = (tsub : Vec Ty0 n) -> Env (Sig n) (Sig-as-HOAS tsub S) ss -> S (substTy tsub t)

opFun : ∀ {ℓ} (S : Ty0 -> Set ℓ) -> ConstructSig -> Set ℓ 
opFun S (CSig n ss t) = (tsub : Vec Ty0 n) -> Env (Sig 0) (opArgFun S) (List.map (substTySig tsub) ss) -> S (substTy tsub t) 

SO-Alg : {ℓ : Level} -> (L : LangSig) -> (S : Ty0 -> Set ℓ) -> Set ℓ 
SO-Alg L S = Env ConstructSig (opFun S) L 

Poly : (L : LangSig) -> Ty0 -> Set₁
Poly L t = (S : Ty0 -> Set) -> SO-Alg L S -> S t 

open import Data.Unit using (⊤)
open import Data.Product using (_×_)

-- -- HOASRep : {ℓ : Level} -> (L : LangSig) -> (S : K -> Set ℓ) -> Env ConstructSig (λ _ -> Set ℓ) L
-- -- HOASRep [] S = []
-- -- HOASRep (csig ∷ cs) S = ConstructSig-as-HOAS S csig ∷ HOASRep cs S 


deBruijn-to-HOAS : 
  {ℓ : Level} {ts : List (Ty 0)} {t : Ty 0} {sz : Size}
  -> (L : LangSig)
  -> DeBruijnTerm L {sz} ts t
  -> (S : Ty 0 -> Set ℓ)
  -> SO-Alg L S -> Env (Ty 0) S ts -> S t 
deBruijn-to-HOAS-arg :
  {ℓ : Level} {ts : List (Ty 0)} {sz : Size} 
  (L : LangSig) (S : Ty 0 -> Set ℓ) (interp : SO-Alg L S) (θ : Env (Ty 0) S ts) (s : Sig 0) 
  -> opArgTerm (DeBruijnTerm L {sz}) ts s
  -> opArgFun S s

deBruijn-to-HOAS-arg L S interp θ (as ↝ a) t θ' = deBruijn-to-HOAS L t S interp (θ' ++ₑ θ)

deBruijn-to-HOAS L (Var x) S _ θ = Env.lookup θ x
deBruijn-to-HOAS L (Con tsub construct args) S interp θ =  Env.lookup interp construct tsub (Env.map (deBruijn-to-HOAS-arg L S interp θ) args)

open import Relation.Nullary using (Dec; yes; no ; ¬_)
open import Relation.Binary.PropositionalEquality as Eq using (_≡_ ; refl ; sym ; trans; cong; subst)
 
open import Data.Empty

import Data.List.Properties 

open import Function 
open import Data.Product

import Data.List.Relation.Unary.All as All
open All using (All ; [] ; _∷_ )
import Data.List.Relation.Unary.Any as Any 
open Any using (here; there)


module _ where
  open import Data.List.Properties using (++-assoc)

  open import Data.List.Relation.Unary.Any.Properties using (++⁺ʳ)        

  All-∈-self-step : 
    {ℓ : Level} {A : Set ℓ}
    -> (xs : List A) (f : List A -> List A)
    -> (g : ∀ {x} -> x ∈ xs -> x ∈ f xs)
    -> All (_∈ f xs) xs
  All-∈-self-step [] f g = []
  All-∈-self-step (x ∷ xs) f g = (g (here refl)) ∷ All-∈-self-step xs (λ zs -> f (x ∷ zs)) (λ ix -> g (++⁺ʳ (x ∷ []) ix))

  All-∈-self : {ℓ : Level} {A : Set ℓ} -> (xs : List A) -> All (_∈ xs) xs 
  All-∈-self xs = All-∈-self-step xs (λ x -> x) (λ x -> x)


  private 
    All-∈-self-canon-step : 
      {ℓ : Level} {A : Set ℓ} {x : A}
      (xs : List A) (f : List A -> List A) (g : ∀ {x} -> x ∈ xs -> x ∈ f xs) (index : x ∈ xs)
      -> All.lookup (All-∈-self-step xs f g) index ≡ g index
    All-∈-self-canon-step (x ∷ xs) f g (here refl)   = refl
    All-∈-self-canon-step (x ∷ xs) f g (there index) = All-∈-self-canon-step xs (λ zs -> f (x ∷ zs)) (λ ix -> g (++⁺ʳ (x ∷ []) ix)) index

  All-∈-self-canon :
    {ℓ : Level} {A : Set ℓ} {x : A} {xs : List A}
    -> (index : x ∈ xs)
    -> All.lookup (All-∈-self xs) index ≡ index 
  All-∈-self-canon index = All-∈-self-canon-step _ (λ x -> x) (λ x -> x) index 

data _≲_ (env : List Ty0) : List Ty0 -> Set where
  ≲-reflexive : ∀ {env'} -> env ≡ env' -> env ≲ env'
  ≲-step : ∀ {t env'} -> env ≲ env' -> env ≲ (t ∷ env') 

pattern ≲-refl = ≲-reflexive refl 

infix 4 _≲_ 

≲-one : ∀ {t env} -> env ≲ t ∷ env 
≲-one = ≲-step ≲-refl 

≲-trans : ∀ {env env' env''} -> env ≲ env' -> env' ≲ env'' -> env ≲ env''
≲-trans ≲-refl wit2 = wit2
≲-trans (≲-step wit1) ≲-refl        = ≲-step wit1
≲-trans (≲-step wit1) (≲-step wit2) = ≲-step (≲-trans (≲-step wit1) wit2) 

≲-++ : ∀ {env env' ts} -> env ≲ env' -> env ≲ (ts ++ env')
≲-++ {ts = []} wit = wit
≲-++ {ts = x ∷ ts} wit = ≲-step (≲-++ {ts = ts} wit)

≲-init : ∀ {env env' ts} -> env ≲ env' -> env ++ ts ≲ env' ++ ts
≲-init ≲-refl = ≲-refl
≲-init (≲-step diff) = ≲-step (≲-init diff)

module _ where
  open import Relation.Binary.Bundles 
  open import Relation.Binary.Structures

  ≲-preorder : Preorder Level.zero Level.zero Level.zero 
  Preorder.Carrier ≲-preorder = List Ty0
  Preorder._≈_ ≲-preorder = _≡_
  Preorder._∼_ ≲-preorder = _≲_
  IsPreorder.isEquivalence (Preorder.isPreorder ≲-preorder) = Eq.isEquivalence
  IsPreorder.reflexive (Preorder.isPreorder ≲-preorder) = λ where refl -> ≲-refl
  IsPreorder.trans (Preorder.isPreorder ≲-preorder) = ≲-trans

weaken-var : {x : Ty0} {env env' : List Ty0} -> env ≲ env' -> x ∈ env -> x ∈ env' 
weaken-var ≲-refl x = x
weaken-var (≲-step diff) x = there (weaken-var diff x)

weaken-var-there :
  ∀ {t t' env env'} 
  (ix : t ∈ env) 
  -> (diff : t' ∷ env ≲ env') 
  -> weaken-var diff (there ix) ≡ weaken-var (≲-trans ≲-one diff) ix
weaken-var-there ix ≲-refl = refl
weaken-var-there ix (≲-step diff) = cong there (weaken-var-there ix diff)

module _ where 
  open Data.Nat using (_+_ ; z≤n ; s≤s )
  open import Data.Nat.Properties

  -- Copied from the latest version of Data.Nat.Properties, as 1.7.1 does not provide the following.
  my-m≤n⇒m≤1+n : ∀ {m n} → m ≤ n → m ≤ 1 + n
  my-m≤n⇒m≤1+n z≤n       = z≤n
  my-m≤n⇒m≤1+n (s≤s m≤n) = s≤s (my-m≤n⇒m≤1+n m≤n)
 
  length-≲ : ∀ {env env'} -> env ≲ env' -> List.length env ≤ List.length env' 
  length-≲ ≲-refl = ≤-refl
  length-≲ (≲-step diff) = my-m≤n⇒m≤1+n (length-≲ diff)

  ≲-step-prop : ∀ {env t env' diff'} -> (diff : env ≲ t ∷ env') -> diff ≡ ≲-step diff' -> env ≡ t ∷ env' -> ⊥ 
  ≲-step-prop (≲-step diff) eq refl = ⊥-elim (1+n≰n (length-≲ diff))

--   diff-extensional : ∀ {t : Ty0} {env env'} {x : t ∈ env} -> (diff₁ diff₂ : env ≲ env') -> weaken-var diff₁ x ≡ weaken-var diff₂ x
--   diff-extensional (≲-reflexive p₁) (≲-reflexive p₂) = {!!}
--   diff-extensional (≲-reflexive refl) (≲-step diff₂) = ⊥-elim (1+n≰n (length-≲ diff₂))
--   diff-extensional (≲-step diff₁) (≲-reflexive refl) = ⊥-elim (1+n≰n (length-≲ diff₁)) 
--   diff-extensional (≲-step diff₁) (≲-step diff₂) = cong there (diff-extensional diff₁ diff₂)


open import Data.Maybe

mapEnvMaybe : {ℓ ℓ' ℓ'' : Level} {K : Set ℓ} {T : K -> Set ℓ'} {S : K -> Set ℓ''} {ts : List K} -> (∀ (a : K) -> T a -> Maybe (S a)) -> Env K T ts -> Maybe (Env K S ts)
mapEnvMaybe f [] = just []
mapEnvMaybe {ℓ'' = ℓ''} f (x ∷ xs) with f _ x 
... | nothing = nothing 
... | just y with mapEnvMaybe f xs 
... | nothing = nothing 
... | just ys = just (y ∷ ys)

module H2D (_≡?_ : (t t' : Ty0) -> Dec (t ≡ t')) where 
  -- assuming we can compare two types. 

  is-same-tenv : (env env' : List Ty0) -> Dec (env ≡ env') 
  -- is-same-tenv [] [] = yes refl 
  -- is-same-tenv [] (e ∷ _) = no λ ()
  -- is-same-tenv (e ∷ es) [] = no λ () 
  -- is-same-tenv (e ∷ es) (e' ∷ es') with e ≡? e' 
  -- ... | no  p = no λ pp -> p (Data.List.Properties.∷-injectiveˡ pp) 
  -- ... | yes refl with is-same-tenv es es' 
  -- ... | no p = no λ pp -> p (Data.List.Properties.∷-injectiveʳ pp)
  -- ... | yes refl = yes refl 
  is-same-tenv = Data.List.Properties.≡-dec _≡?_ 

  infix 4 _≲?_ 

  -- ≲?-Prop : (env : List Ty0) -> ¬ (env ≡ []) ->  Dec (env ≲ [])
  -- ≲?-Prop [] p = ⊥-elim (p refl)
  -- ≲?-Prop (x ∷ env) p = no (λ ())
  
  data compare-res (n m : ℕ) : Set where 
    comp-equal : n ≡ m -> compare-res n m 
    comp-less  : n < m -> compare-res n m 
    comp-greater : m < n -> compare-res n m 

  mycompare : (n : ℕ) -> (m : ℕ) -> compare-res n m 
  mycompare ℕ.zero ℕ.zero = comp-equal refl
  mycompare ℕ.zero (ℕ.suc m) = comp-less (Data.Nat.s≤s (Data.Nat.z≤n))
  mycompare (ℕ.suc n) ℕ.zero = comp-greater (Data.Nat.s≤s (Data.Nat.z≤n))
  mycompare (ℕ.suc n) (ℕ.suc m) with mycompare n m 
  ... | comp-equal refl = comp-equal refl
  ... | comp-less n<m = comp-less (Data.Nat.s≤s n<m) 
  ... | comp-greater m<n = comp-greater (Data.Nat.s≤s m<n) 

  module _ where 
    open import Data.Nat.Properties 

    private
      ≲-lemma : (env env' : List Ty0) -> ¬ env ≡ env' -> List.length env ≡ List.length env' -> ¬ env ≲ env' 
      ≲-lemma env .env p _ ≲-refl = p refl
      ≲-lemma (x ∷ env) .(t ∷ _) p len-p (≲-step {t} {env'} w) = n≮n (List.length env') (subst (λ x -> x ≤ List.length env') len-p (length-≲ w)) 

      ≲-lemma2 : (env env' : List Ty0) {x : Ty0} -> ¬ (env ≲ env') -> List.length env ≤ List.length env' -> ¬ env ≲ (x ∷ env')
      ≲-lemma2 .(_ ∷ env') env' n≲ lenv≤lenv' ≲-refl = n≮n (List.length env') lenv≤lenv'
      ≲-lemma2 env env' n≲ lenv≤lenv' (≲-step env≲env') = n≲ env≲env'

      _∙_ : ∀ {n m k} -> n ≤ m -> m ≤ k -> n ≤ k 
      _∙_ = ≤-trans 
      infixr 4 _∙_

    -- The following definition of ≲? is more convenient when writing examples. For example, 
    -- a ∷ [] ≲? b ∷ a ∷ [] results in yes (≲-step ≲-refl), provided that a ≡? a is yes relf. 
    --
    -- Notice that for abstract types a and b, we do not know whether a ≡? b returns yes or no. 

    ≲?-by-length : (n n' : ℕ) -> (env env' : List Ty0) -> List.length env ≡ n -> List.length env' ≡ n' -> Dec (env ≲ env')
    ≲?-by-length n n' env env' len-ok len-ok' with mycompare n n' 
    ... | comp-equal refl with is-same-tenv env env' 
    ... | yes refl = yes (≲-refl) 
    ... | no p = no (≲-lemma env env' p (trans len-ok (sym len-ok')))  
    ≲?-by-length n n' env env' len-ok len-ok' | comp-greater n'<n = no λ w ->  n≮n n (Data.Nat.s≤s (≤-reflexive (sym len-ok) ∙ length-≲ w ∙ ≤-reflexive len-ok') ∙ n'<n) 
    ≲?-by-length n (ℕ.suc n') env (x ∷ env') len-ok len-ok' | comp-less n<n' with ≲?-by-length n n' env env' len-ok (suc-injective len-ok') 
    ... | yes w = yes (≲-step w)
    ... | no pp = no (≲-lemma2 env env' pp ( ≤-reflexive len-ok ∙ ≤-pred n<n' ∙ (≤-reflexive (sym (suc-injective len-ok')))) ) 

  _≲?_ : (env env' : List Ty0) -> Dec (env ≲ env') 
  env ≲? env' = ≲?-by-length (List.length env) (List.length env') env env' refl refl 
  -- env ≲? env' with is-same-tenv env env' 
  -- ... | yes refl = yes ≲-refl 
  -- (env ≲? []) | no p = ≲?-Prop env p
  -- (env ≲? x ∷ env') | no p with env ≲? env' 
  -- ... | yes ok = yes (≲-step ok)
  -- ... | no pp = no (λ { (≲-refl) -> p refl ; (≲-step ok) -> pp ok }) 

  module _ where
    open import Axiom.UniquenessOfIdentityProofs 
    open import Data.Nat.Properties
    private 
      lemma : ∀ w w' -> (eq : w ≡ w') -> (wk : w ≲ w') -> subst (λ v → v ≲ w') eq wk ≡ ≲-refl 
      lemma [] _ refl ≲-refl = refl
      lemma (x ∷ w) _ eq ≲-refl with Decidable⇒UIP.≡-irrelevant is-same-tenv eq refl 
      lemma (x ∷ w) .(x ∷ w) .refl ≲-refl | refl = refl

      lemma (x ∷ w) _ refl (≲-step wk) = ⊥-elim (1+n≰n (length-≲ wk))

    -- two weakening proofs are the identical 
    -- (i.e., w1 ≲ w2 is a mere proposition assuming Ty0 is a set)
    ≲-canon : ∀ w1 w2 -> (wk1 wk2 : w1 ≲ w2) -> wk1 ≡ wk2 
    ≲-canon w1 .w1 ≲-refl wk2 with lemma _ _ refl wk2 
    ... | res = sym res
    ≲-canon .(_ ∷ _) .(_ ∷ _) (≲-step wk1) ≲-refl with length-≲ wk1 
    ... | p =  ⊥-elim (1+n≰n p)
    ≲-canon w1 .(_ ∷ _) (≲-step wk1) (≲-step wk2) rewrite ≲-canon _ _ wk1 wk2 = refl 


  module _ (L : LangSig) where       
    TSem : Ty0 -> Set 
    TSem t = (tenv : List Ty0) -> Maybe (DeBruijnTerm L tenv t)


    module DeBruijn-Interp where
      makeX : ∀ (b : Ty0) (bs : List Ty0) -> TSem b
      makeX b bs tenv with (b ∷ bs) ≲? tenv 
      ... | yes diff = just (Var (weaken-var diff (here refl))) 
      ... | no _  = nothing 

      -- makeXs : ∀ (n : ℕ) (tsub : Vec Ty0 n) (tenv : List Ty0) (as : List (Ty n)) -> Env Ty0 TSem (List.map (substTy tsub) as)
      -- makeXs n _ _ [] = []
      -- makeXs n tsub tenv (a ∷ as) = makeX (substTy tsub a) (List.map (substTy tsub) as ++ tenv) ∷ makeXs n tsub tenv as
                                                                                     
--      makeArg : ∀ (n : ℕ) (tsub : Vec Ty0 n) (tenv : List Ty0) (s : Sig n) -> opArgFun tsub TSem s -> Maybe (opArgTerm tsub (DeBruijnTerm L) tenv s)
      -- makeArg n tsub tenv (as ↝ a) f = f (makeXs n tsub tenv as) (List.map (substTy tsub) as ++ tenv) 

      makeXs : (tenv : List Ty0) (as : List Ty0) -> Env Ty0 TSem as 
      makeXs tenv [] = []
      makeXs tenv (a ∷ as) = makeX a (as ++ tenv) ∷ makeXs tenv as 
    
      makeArg : (tenv : List Ty0) (a : Sig 0) -> opArgFun TSem a -> Maybe (opArgTerm (DeBruijnTerm L) tenv a)
      makeArg tenv (as ↝ a) f = f (makeXs tenv as) (as ++ tenv) 

    DeBruijn-Op : (csig : ConstructSig) -> csig ∈ L -> opFun TSem csig
    DeBruijn-Op (CSig n ss t) p tsub fs tenv with mapEnvMaybe (makeArg tenv) fs -- (makeArg n tsub tenv) fs 
      where 
        open DeBruijn-Interp
    ... | just tms = just (Con tsub p tms)
    ... | nothing  = nothing 

    DeBruijn-Alg : (LL : List ConstructSig) -> All (_∈ L) LL -> SO-Alg LL TSem -- Env ConstructSig (opFun TSem) LL 
    DeBruijn-Alg [] [] = []
    DeBruijn-Alg (csig ∷ LL) (p ∷ ps) = DeBruijn-Op csig p ∷ (DeBruijn-Alg LL ps)

    HOAS-to-deBruijn : {t : Ty0} -> Poly L t -> TSem t
    HOAS-to-deBruijn f = f TSem (DeBruijn-Alg L (All-∈-self L))
