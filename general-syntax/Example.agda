{-# OPTIONS --without-K --sized-types #-}

module Example  where

open import Level renaming (zero to lzero; suc to lsuc)
open import Data.Nat
open import Data.Fin
open import Data.Vec as V using (Vec ; [] ; _∷_ )
open import Data.List
open import Data.Empty

open import Data.Product as Prod using (_×_ ; proj₁ ; proj₂ ; _,_ )

open import Relation.Binary.PropositionalEquality as Eq using (_≡_ ; refl ; sym ; trans; cong)
open import Relation.Nullary using (Dec; yes; no ; ¬_)

data Ty : ℕ -> Set where 
  o : Ty 0 -- unit type 
  _~>_ : {n : ℕ} -> Ty n -> Ty n -> Ty n 
  TyVar : {n : ℕ} -> Fin n -> Ty n 

infixr 4 _~>_

substTy : {n : ℕ} -> Vec (Ty 0) n -> Ty n -> Ty 0 
substTy tsub o = o
substTy tsub (t ~> t') = substTy tsub t ~> substTy tsub t'
substTy tsub (TyVar x) = V.lookup tsub x

~>-injective : {s s' t t' : Ty 0} -> ((s ~> s') ≡ (t ~> t')) -> s ≡ t × s' ≡ t' 
~>-injective refl = refl , refl 

tyEq : (t t' : Ty 0) -> Dec (t ≡ t')
tyEq o o = yes refl
tyEq o (t' ~> t'') = no λ ()
tyEq (t ~> t₁) o = no λ ()
tyEq (t ~> t₁) (t' ~> t₁') with tyEq t t' 
... | no p     = no λ where refl -> ⊥-elim (p refl)  
... | yes refl with tyEq t₁ t₁' 
... | no p = no λ x → p (proj₂ (~>-injective x))
... | yes refl = yes refl 

tyEq-refl : (t : Ty 0) -> tyEq t t ≡ yes refl 
tyEq-refl o = refl
tyEq-refl (t ~> t₁) with tyEq t t | tyEq-refl t 
... | yes refl | refl with tyEq t₁ t₁ | tyEq-refl t₁ 
... | yes refl | refl = refl

0F : {n : ℕ} -> Fin (suc n)
0F = zero 

1F : {n : ℕ} -> Fin (suc (suc n))
1F = suc zero

open import Sig Ty TyVar substTy 
open import Syntax Ty TyVar substTy 
open H2D tyEq 

lamSig : ConstructSig
lamSig = CSig 2 (arg ∷ []) (a ~> b)
  where
    a = TyVar 0F
    b = TyVar 1F

    arg = (a ∷ []) ↝ b

appSig : ConstructSig
appSig = CSig 2 (arg1 ∷ arg2 ∷ []) b
  where
    a = TyVar 0F
    b = TyVar 1F 
    arg1 = [] ↝ (a ~> b)
    arg2 = [] ↝ a

STLCSig : LangSig 
STLCSig = lamSig ∷ appSig ∷ []

open import Data.List.Relation.Unary.Any 

open import Env as Env 

STLC : List (Ty 0) -> Ty 0 -> Set 
STLC = DeBruijnTerm STLCSig

appCon : {ts : List (Ty 0)} {a b : Ty 0} -> DeBruijnTerm STLCSig ts (a ~> b) -> DeBruijnTerm STLCSig ts a -> DeBruijnTerm STLCSig ts b
appCon {a = a} {b} arg1 arg2 = Con (a ∷ b ∷ []) (there (here refl)) (arg1 ∷ arg2 ∷ [])

lamCon_ : {ts : List (Ty 0)} {a b : Ty 0} -> DeBruijnTerm STLCSig (a ∷ ts) b -> DeBruijnTerm STLCSig ts (a ~> b)
lamCon_ {a = a} {b} arg = Con (a ∷ b ∷ []) (here refl) (arg ∷ [])


open import Data.List.Membership.Propositional using (_∈_)
0x : ∀ {a : Ty 0} {ts : List (Ty 0)} -> a ∈ (a ∷ ts)
0x = here refl 

1x : ∀ {a b : Ty 0} {ts : List (Ty 0)} -> a ∈ (b ∷ a ∷ ts) 
1x = there (here refl) 

2x : ∀ {a b c : Ty 0} {ts : List (Ty 0)} -> a ∈ (b ∷ c ∷ a ∷ ts) 
2x = there (there (here refl))


module Exp where 
  id : ∀ {a ts} -> DeBruijnTerm STLCSig ts (a ~> a)
  id = lamCon (Var 0x)

  K : ∀ {a b ts} -> DeBruijnTerm STLCSig ts (a ~> (b ~> a))
  K = lamCon lamCon (Var 1x)

  S : ∀ {a b c d ts} -> STLC ts ((a ~> c ~> b ~> d) ~> (a ~> c) ~> a ~> b ~> d)
  S = lamCon lamCon lamCon (appCon (appCon (Var 2x) (Var 0x)) (appCon (Var 1x) (Var 0x)))

module STLCHOAS (S : Ty 0 -> Set) (reps : SO-Alg STLCSig S) where 
  lamH_ : ∀ {a b} -> (S a -> S b) -> S (a ~> b)
  lamH_ f = abs (_ ∷ _ ∷ []) ((λ where (x ∷ _) -> f x) ∷ [])
    where
      abs = Env.lookup reps (here refl)

  appH : ∀ {a b} -> S (a ~> b) -> S a -> S b 
  appH f x = app (_ ∷ _ ∷ []) ((λ _ → f) ∷ ((λ _ -> x) ∷ []))
    where
      app = Env.lookup reps (there (here refl))

module HOASExp where 

  id : ∀ {a} -> Poly STLCSig (a ~> a)       
  id {a} Sem reps = lamH (λ x -> x)
    where open STLCHOAS Sem reps 

  K : ∀ {a b} -> Poly STLCSig (a ~> b ~> a)
  K Sem reps = lamH λ x -> lamH λ y -> x 
    where open STLCHOAS Sem reps
      
  S : ∀ {a b c d} -> Poly STLCSig ((a ~> c ~> b ~> d) ~> (a ~> c) ~> a ~> b ~> d)
  S Sem reps = lamH λ x -> lamH λ y -> lamH λ z -> appH (appH x z) (appH y z)
    where open STLCHOAS Sem reps

module EqualsD2H where   
  open import Axiom.Extensionality.Propositional 
  postulate
    ext : Extensionality lzero lzero
  open import Function using (_$_)
  open import Data.Maybe

  deBruijn⟶HOAS-id : 
    ∀ {a : Ty 0} (S : Ty 0 -> Set) (reps : SO-Alg STLCSig S) -> 
    HOASExp.id {a} S reps ≡ deBruijn-to-HOAS STLCSig (Exp.id {a}) S reps []
  deBruijn⟶HOAS-id S reps = 
    cong (Env.lookup reps (here refl) (_ ∷ _ ∷ [])) $ cong (_∷ []) (ext λ where (x ∷ []) -> refl)

  deBruijn⟶HOAS-K :
    ∀ {a b : Ty 0} (S : Ty 0 -> Set) (reps : SO-Alg STLCSig S) -> 
    HOASExp.K {a} {b} S reps ≡ deBruijn-to-HOAS STLCSig (Exp.K {a} {b}) S reps []
  deBruijn⟶HOAS-K S reps = 
    cong (Env.lookup reps _ _) 
    $ cong (_∷ []) 
    $ ext (λ where (x ∷ []) -> cong (Env.lookup reps _ _) (cong (_∷ []) (ext (λ where (y ∷ []) -> refl))))
  
  deBruijn⟶HOAS-S : 
    ∀ {a b c d : Ty 0} (S : Ty 0 -> Set) (reps : SO-Alg STLCSig S) -> 
    HOASExp.S {a} {b} {c} {d} S reps ≡ deBruijn-to-HOAS STLCSig (Exp.S {a} {b} {c} {d}) S reps []
  deBruijn⟶HOAS-S S reps = 
    cong (Env.lookup reps _ _) 
    $ cong (_∷ []) 
    $ ext λ where 
        (x ∷ []) -> 
          cong (Env.lookup reps _ _) 
          $ cong (_∷ []) 
          $ ext λ where 
             (y ∷ []) -> 
               cong (Env.lookup reps _ _) 
               $ cong (_∷ []) 
               $ ext λ where
                   (z ∷ []) -> 
                     cong (Env.lookup reps _ _) 
                     $ Eq.cong₂ (λ x y -> x ∷ y ∷ []) 
                       (ext λ where [] -> cong (Env.lookup reps _ _) 
                                           $ Eq.cong₂ (λ a b → a ∷ b ∷ []) (ext λ where [] -> refl) (ext λ where [] -> refl))
                       (ext λ where [] -> cong (Env.lookup reps _ _) 
                                           $ Eq.cong₂ (λ a b → a ∷ b ∷ []) (ext λ where [] -> refl) (ext λ where [] -> refl))




module EqualsH2D where   
  open import Data.Maybe 

  HOAS⟶deBruijn-id : 
    ∀ {a : Ty 0} 
    -> HOAS-to-deBruijn STLCSig (HOASExp.id {a}) [] ≡ just (Exp.id {a}) 
  HOAS⟶deBruijn-id {a} rewrite tyEq-refl a = refl

  HOAS⟶deBruijn-K :
    ∀ {a b : Ty 0} 
    -> HOAS-to-deBruijn STLCSig (HOASExp.K {a} {b}) [] ≡ just (Exp.K {a} {b}) 
  HOAS⟶deBruijn-K {a} {b} rewrite tyEq-refl a = refl

  -- The following code is quite slow to typecheck.
  HOAS⟶deBruijn-S : 
    ∀ {a b c d : Ty 0} 
    -> HOAS-to-deBruijn STLCSig (HOASExp.S {a} {b} {c} {d}) [] ≡ just (Exp.S {a} {b} {c} {d})
  HOAS⟶deBruijn-S {a} {b} {c} {d} rewrite 
    -- I don't know how many repetitions of tyEq-refl we need to put. 
    tyEq-refl a | tyEq-refl c | tyEq-refl b | tyEq-refl d | 
    tyEq-refl a | tyEq-refl c | tyEq-refl b | tyEq-refl d | 
    tyEq-refl a | tyEq-refl c | tyEq-refl b | tyEq-refl d | 
    tyEq-refl a | tyEq-refl c | tyEq-refl b | tyEq-refl d | 
    tyEq-refl a | tyEq-refl c | tyEq-refl b | tyEq-refl d | 
    tyEq-refl a | tyEq-refl c | tyEq-refl b | tyEq-refl d 
    = refl 

