open import Ty
open import Level

data PTerm {ℓ} (ν : Ty -> Set ℓ) : Ty -> Set ℓ where 
  Var : ∀ {τ} -> ν τ -> PTerm ν τ
  App : ∀ {σ τ} -> PTerm ν (σ ~> τ) -> PTerm ν σ -> PTerm ν τ 
  Lam : ∀ {σ τ} -> (ν σ -> PTerm ν τ) -> PTerm ν (σ ~> τ)

ClosedPTerm  : ∀ {ℓ} -> Ty -> Set (suc ℓ)
ClosedPTerm {ℓ} A = ∀ (ν : Ty -> Set ℓ) -> PTerm ν A 

interp : ∀ {ℓ ν τ} -> 
         PTerm {ℓ} ν τ ->
         (appSem : ∀ σ τ -> ν (σ ~> τ) -> ν σ -> ν τ) -> 
         (lamSem : ∀ σ τ -> (ν σ -> ν τ) -> ν (σ ~> τ)) -> 
         ν τ 
interp (Var x) appSem lamSem = x
interp (App t₁ t₂) appSem lamSem = appSem _ _ (interp t₁ appSem lamSem) (interp t₂ appSem lamSem)
interp (Lam f) appSem lamSem = lamSem _ _ (λ x -> interp (f x) appSem lamSem)

