open import Level
open import Poly 
open import PHOAS

open import Data.Sum
open import Data.Product
open import Relation.Binary
open import Relation.Binary.PropositionalEquality hiding (Extensionality)
open import Ty

open import Axiom.Extensionality.Propositional using (Extensionality)

toPoly : ∀ {ℓ} {A} -> ClosedPTerm {ℓ} A -> Poly {ℓ} A
toPoly c T lamSem appSem = interp (c T) appSem lamSem

fromPoly : ∀ {ℓ} {A} -> Poly {ℓ} A -> ClosedPTerm {ℓ} A
fromPoly h T = h (PTerm T) (λ _ _ k -> Lam (λ x -> k (Var x))) (λ _ _ -> App)  


module FT where
  _~~>_ : ∀ {ℓ₁ ℓ₂ ℓ'} {A₁ B₁ : Set ℓ₁} {A₂ B₂ : Set ℓ₂} -> 
          REL A₁ A₂ ℓ' -> REL B₁ B₂ ℓ' -> REL (A₁ -> B₁) (A₂ -> B₂) (ℓ₁ ⊔ ℓ₂ ⊔ ℓ')
  r₁ ~~> r₂ = λ f₁ f₂ -> ∀ x₁ x₂ -> r₁ x₁ x₂ -> r₂ (f₁ x₁) (f₂ x₂)
     
  infixr 90 _~~>_
        
  forallTy : ∀ {ℓ} {P₁ P₂ : Ty -> Set ℓ} -> 
             (∀ (A : Ty) -> REL (P₁ A) (P₂ A) ℓ) -> 
             REL (∀ (A : Ty) -> P₁ A) (∀ (A : Ty) -> P₂ A) ℓ
  forallTy r k₁ k₂ = ∀ A -> r A (k₁ _) (k₂ _) 

  forallTy1 : ∀{ℓ ℓ'} {P₁ P₂ : (Ty -> Set ℓ) -> Set ℓ'} -> 
               (∀ {T₁ T₂ : Ty -> Set ℓ} -> (∀ A -> REL (T₁ A) (T₂ A) ℓ) -> REL (P₁ T₁) (P₂ T₂) ℓ') -> 
               REL (∀ (T : Ty -> Set ℓ) -> P₁ T) (∀ (T : Ty -> Set ℓ) -> P₂ T) (suc ℓ ⊔ ℓ')
  forallTy1 h k₁ k₂ = ∀ {T₁ T₂} -> ∀ r -> h r (k₁ T₁) (k₂ T₂) -- h r (k₁ T₁) (k₂ T₂) 
 
  -- R : A <-> B --> List R : List R <-> List R 

  data pterm-rel {ℓ} {T₁ T₂ : Ty -> Set ℓ} (R : ∀ (A : Ty) -> REL (T₁ A) (T₂ A) ℓ) : (A : Ty) -> PTerm T₁ A -> PTerm T₂ A -> Set ℓ where
    var-rel : ∀{A} {x₁ : T₁ A} {x₂ : T₂ A} -> R A x₁ x₂ -> pterm-rel R A (Var x₁) (Var x₂)
    app-rel : ∀{A B} (f₁ : PTerm T₁ (A ~> B)) (f₂ : PTerm T₂ (A ~> B))
                     (t₁ : PTerm T₁ A) (t₂ : PTerm T₂ A) -> 
              pterm-rel R (A ~> B) f₁ f₂ -> pterm-rel R A t₁ t₂ -> 
              pterm-rel R B (App f₁ t₁) (App f₂ t₂)

    lam-rel : ∀{A B} (h₁ : T₁ A -> PTerm T₁ B) (h₂ : T₂ A -> PTerm T₂ B) ->
              (∀ x₁ x₂ -> R A x₁ x₂ -> pterm-rel R B (h₁ x₁) (h₂ x₂)) -> 
              pterm-rel R (A ~> B) (Lam h₁) (Lam h₂)
 
  free-theorem-for-poly : ∀ {ℓ} (A : Ty) -> REL (Poly {ℓ} A) (Poly {ℓ} A) (suc ℓ)
  free-theorem-for-poly A = 
    forallTy1 λ t -> 
      (forallTy λ a -> forallTy λ b -> (t a ~~> t b) ~~> t (a ~> b)) ~~>
      (forallTy λ a -> forallTy λ b -> t (a ~> b) ~~> t a ~~> t b) ~~>
      t A 
  

  free-theorem-for-phoas : ∀ {ℓ} (A : Ty) -> REL (∀ (T : Ty -> Set ℓ) -> PTerm T A) (∀ (T : Ty -> Set ℓ) -> PTerm T A) (suc ℓ) 
  free-theorem-for-phoas A = 
    forallTy1 λ t -> pterm-rel t A
    
module _ (ft : ∀ {ℓ} A x y -> FT.free-theorem-for-poly {ℓ} A x y) 
         (f-ext : ∀ {ℓ ℓ'} -> Extensionality ℓ ℓ') where 
  open FT

  toPoly∘fromPoly : ∀ {ℓ} {A} (h : Poly {ℓ} A) -> 
                    (T : Ty -> Set ℓ) -> 
                    (lamSem : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                    (appSem : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
                    toPoly (fromPoly h) T lamSem appSem ≡ h T lamSem appSem
  toPoly∘fromPoly {ℓ} {A} h T lamSem appSem = 
          ft A h h MyRel (λ _ _ k -> Lam (λ x -> k (Var x))) lamSem lam-ok (λ _ _ -> App) appSem app-ok
   
    where
      MyRel : ∀ (A : Ty) -> REL (PTerm T A) (T A) _
      MyRel _ = λ t₁ t₂ -> interp t₁ appSem lamSem ≡ t₂

      lam-ok : (A₁ A₂ : Ty) ->     
               ((MyRel A₁ ~~> MyRel A₂) ~~> MyRel (A₁ ~> A₂))
               (λ k → Lam (λ x → k (Var x))) (lamSem A₁ A₂)
      lam-ok A₁ A₂ k₁ k₂ keq = cong (lamSem A₁ A₂) eq-f  
        where
          eq : ∀ x -> interp (k₁ (Var x)) appSem lamSem ≡ k₂ x
          eq x = keq (Var x) x refl 

          eq-f : (λ x -> interp (k₁ (Var x)) appSem lamSem) ≡ k₂
          eq-f =  f-ext eq
          
      app-ok : (A₁ A₂ : Ty) -> 
               (MyRel (A₁ ~> A₂) ~~> MyRel A₁ ~~> MyRel A₂) App (appSem A₁ A₂)
      app-ok A₁ A₂ f₁ _ refl t₁ _ refl = refl 
    

module _ (ft : ∀ {ℓ} A x y -> FT.free-theorem-for-phoas {ℓ} A x y) 
         (f-ext : ∀ {ℓ ℓ'} -> Extensionality ℓ ℓ') where 
  MyRel : ∀ {ℓ} {T : Ty -> Set ℓ} -> ∀ A -> REL (PTerm T A) (T A) ℓ 
  MyRel = λ A x y -> x ≡ Var y

  lemma : ∀ {ℓ} {A} {T : Ty -> Set ℓ} (t₁ : PTerm (PTerm T) A) (t₂ : PTerm T A) -> 
          FT.pterm-rel MyRel A t₁ t₂ -> 
          interp t₁ (λ _ _ -> App) (λ _ _ k -> Lam (λ x -> k (Var x))) ≡ t₂ 
  lemma {ℓ} {A} {T} .(Var _) .(Var _) (FT.var-rel x) = x
  lemma {ℓ} {A} {T} .(App f₁ t₁) .(App f₂ t₂) (FT.app-rel f₁ f₂ t₁ t₂ relf relt) rewrite lemma f₁ f₂ relf | lemma t₁ t₂ relt = refl
  lemma {ℓ} {.(_ ~> _)} {T} .(Lam h₁) .(Lam h₂) (FT.lam-rel h₁ h₂ rel) = cong Lam eq-f 
    where
      eq : ∀ x -> interp (h₁ (Var x)) (λ _ _ -> App) (λ _ _ k -> Lam (λ y -> k (Var y))) ≡ h₂ x
      eq x rewrite lemma (h₁ (Var x)) (h₂ x) (rel (Var x) x refl) = refl

      eq-f : (λ x -> interp (h₁ (Var x)) (λ _ _ -> App) (λ _ _ k -> Lam (λ y -> k (Var y)))) ≡ h₂ 
      eq-f = f-ext eq

  fromPoly∘toPoly : ∀ {ℓ} {A T} (t : ClosedPTerm {ℓ} A) -> fromPoly (toPoly t) T ≡ t T
  fromPoly∘toPoly {A = A} {T = T} t = lemma (t (PTerm T)) (t T) (ft A t t MyRel)  
