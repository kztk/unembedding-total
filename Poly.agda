{-# OPTIONS --without-K #-}

open import Level
open import Ty 

Poly : ∀ {ℓ} (A : Ty) -> Set (suc ℓ)
Poly {ℓ} A = (T : Ty -> Set ℓ) -> 
                   (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                   (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> T A
