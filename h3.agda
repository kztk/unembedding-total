{-# OPTIONS --without-K --type-in-type #-}

open import Level
open import Relation.Binary
open import Relation.Binary.PropositionalEquality
open import Data.Maybe 
open import Data.Unit using (⊤ ; tt)
open import Data.Product
open import Data.List
open import Relation.Nullary
open import Data.Empty
open import Data.List.Properties using (≡-dec)

open import Function

open import Codata.Thunk 

open import Ty 
import KInt 

module _ where

Poly : ∀ (τ : Ty) -> Set₁
Poly A =  ∀ (T : Ty -> Set) -> 
          ∀ (M : Set -> Set) -> 
           (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
           (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
           (mark : ∀ A -> T A -> M (T A)) -> 
           (local : ∀ A -> M (T A) -> T A) -> 
           (return : ∀ (R : Set) -> R -> M R) -> 
           (bind : ∀ (R S : Set) -> M R -> (R -> M S) -> M S) -> 
           (mfix : ∀ (R : Set) -> (R -> M R) -> M R) -> 
           T A


module KInt' (Carrier : Set) (_≤'_ : Carrier -> Carrier -> Set) (trans : Transitive _≤'_ ) where 
  open KInt Carrier _≤'_ trans

  forallR : ∀ {ℓ ℓ'}
    {T : Set ℓ -> Set ℓ'} -> 
    (∀ {R} -> KRel R -> KRel (T R)) -> 
    KRel (∀ (τ : Set ℓ) -> T τ)
  forallR k = 
        record { prop = λ w i -> ∀ {τ} -> ∀ (r : KRel τ) ->  KRel.prop (k r) w (i τ) ; 
                 prop-kripke = λ w w' i w≤w' f r -> KRel.prop-kripke (k r) w w' (i _) w≤w' (f r)  } 

  forallR1 : ∀ {ℓ ℓ' ℓ''}
    {T : (Set ℓ -> Set ℓ') -> Set ℓ'' } -> 
    (∀ {M} -> (∀ {R} -> KRel R -> KRel (M R)) -> KRel (T M)) -> 
    KRel (∀ (M : Set ℓ -> Set ℓ') -> T M )
  forallR1  k = 
        record { prop = λ w i -> ∀ {M} -> ∀ (r : ∀ {R} -> KRel R -> KRel (M R)) ->  KRel.prop (k r) w (i M) ; 
                 prop-kripke = λ w w' i w≤w' f r -> KRel.prop-kripke (k r) w w' (i _) w≤w' (f r)  } 

  m-term : ∀{ℓ ℓ'} (A : Ty) -> 
           KRel (∀ (T : Ty -> Set ℓ) -> 
                   (M : Set ℓ -> Set ℓ') -> 
                   (lam : ∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                   (app : ∀ A B -> T (A ~> B) -> T A -> T B) -> 
                   (mark : ∀ A -> T A -> M (T A)) -> 
                   (local : ∀ A -> M (T A) -> T A) -> 
                   (return : ∀ R -> R -> M R) -> 
                   (bind : ∀ R S -> M R -> (R -> M S) -> M S) ->
                   (mfix : ∀ R -> (R -> M R) -> M R) -> 
                   T A)
  m-term A = forallTy1 λ t -> 
             forallR1 λ m -> 
             (forallTy λ a -> forallTy λ b -> (t a ~~> t b) ~~> t (a ~> b)) 
             ~~> (forallTy λ a -> forallTy λ b -> t (a ~> b) ~~> t a ~~> t b) 
             ~~> (forallTy λ a -> t a ~~> m (t a))
             ~~> (forallTy λ a -> m (t a) ~~> t a)
             ~~> (forallR λ r -> r ~~> m r)
             ~~> (forallR λ r -> forallR λ s -> m r ~~> (r ~~> m s) ~~> m s)
             ~~> (forallR λ r -> (r ~~> m r) ~~> m r)
             ~~> t A
  
  
module _ where
  data _∈_ : Ty -> List Ty -> Set where 
    Z : ∀ {a env}   -> a ∈ (a ∷ env)
    S : ∀ {a b env} -> a ∈ env -> a ∈ (b ∷ env)

  -- data _∈G_ {A : Set} : A -> List A -> Set where 
  --   Z : ∀ {a env}   -> a ∈G (a ∷ env)
  --   S : ∀ {a b env} -> a ∈G env -> a ∈G (b ∷ env)  

  data _≲_ : List Ty -> List Ty -> Set where 
    ≲-refl : ∀ {env} -> env ≲ env
    ≲-step : ∀ {env env' b} -> env ≲ env' -> env ≲ (b ∷ env') 

  ≲-trans : ∀ {env env' env''} -> env ≲ env' -> env' ≲ env'' -> env ≲ env''
  ≲-trans ≲-refl wit2 = wit2
  ≲-trans (≲-step wit1) ≲-refl        = ≲-step wit1
  ≲-trans (≲-step wit1) (≲-step wit2) = ≲-step (≲-trans (≲-step wit1) wit2) 

  ≲-is-preorder : IsPreorder (_≡_) (_≲_)
  ≲-is-preorder = record { isEquivalence =  isEquivalence ; reflexive = λ { refl -> ≲-refl } ; trans = ≲-trans } 

  data DTerm : (Γ : List Ty) -> Ty -> Set  where 
    Var : ∀ { Γ τ } -> τ ∈ Γ -> DTerm Γ τ 
    App : ∀ { Γ σ τ } -> DTerm Γ (σ ~> τ) -> DTerm Γ σ -> DTerm Γ τ
    Abs : ∀ { Γ σ τ } -> DTerm (σ ∷ Γ) τ -> DTerm Γ (σ ~> τ)
    GVar : ∀ {Γ τ} -> DTerm Γ τ 

  checkEqEnv : (t1 : List Ty) -> (t2 : List Ty) -> Dec (t1 ≡ t2) 
  checkEqEnv = Data.List.Properties.≡-dec checkEqTy 

  shift : ∀ {a env env'} -> a ∈ env -> env ≲ env' -> a ∈ env'
  shift var ≲-refl = var
  shift var (≲-step wit) = S (shift var wit) 

  shiftVarGen : ∀ {a env env' env0} -> env ≲ env' -> a ∈ (env0 ++ env) -> a ∈ (env0 ++ env') 
  shiftVarGen {env0 = []} w≲w' x = shift x w≲w'
  shiftVarGen {env0 = _ ∷ env0} w≲w' Z = Z
  shiftVarGen {env0 = _ ∷ env0} w≲w' (S x) = S (shiftVarGen w≲w' x) 
  
  shiftExpGen : ∀ {τ env env' env0} -> env ≲ env'
                -> DTerm (env0 ++ env) τ -> DTerm (env0 ++ env') τ
  shiftExpGen w≲w' (Var x) = Var (shiftVarGen w≲w' x) 
  shiftExpGen w≲w' (App t₁ t₂) = App (shiftExpGen w≲w' t₁) (shiftExpGen w≲w' t₂)
  shiftExpGen w≲w' (Abs t)     = Abs (shiftExpGen w≲w' t) 
  shiftExpGen w≲w' GVar        = GVar
  
  shiftExp : ∀ {τ env env'} -> env ≲ env' -> DTerm env τ -> DTerm env' τ
  shiftExp = shiftExpGen {env0 = []} 


  -- shiftGExp : ∀ {τ Δ Δ' Γ} -> Δ ≲ Δ' -> DTerm Δ Γ τ -> DTerm Δ' Γ τ
  -- shiftGExp Δ≲Δ' (Var x)     = Var x
  -- shiftGExp Δ≲Δ' (App t₁ t₂) = App (shiftGExp Δ≲Δ' t₁) (shiftGExp Δ≲Δ' t₂)
  -- shiftGExp Δ≲Δ' (Abs t)     = Abs (shiftGExp Δ≲Δ' t)
  -- shiftGExp Δ≲Δ' (GVar x)    = GVar (shift x Δ≲Δ') 

  diffEnv : (w1 : List Ty) -> (w2 : List Ty) -> Maybe (w1 ≲ w2) 
  diffEnv w1 w2 with checkEqEnv w1 w2 
  diffEnv w1 .w1 | yes refl = just ≲-refl 
  ... | no  _ with w2 
  diffEnv [] _ | no p | [] = ⊥-elim (p refl)
  diffEnv (_ ∷ _) _ | no _ | [] = nothing
  diffEnv w1 _ | no _ | _ ∷ w2' = diffEnv w1 w2' >>= λ rel -> just (≲-step rel)
  
  ≲-diff : ∀ w1 w2 -> w1 ≲ w2 -> ∃ λ w -> diffEnv w1 w2 ≡ just w 
  ≲-diff w1 w2 wk with checkEqEnv w1 w2 
  ≲-diff w1 .w1 wk | yes refl = ≲-refl , refl
  ≲-diff w1 w2 wk | no ¬p with w2 
  ≲-diff [] w2 wk | no ¬p | [] = wk , (⊥-elim (¬p refl))
  ≲-diff .(_ ∷ w2') w2 ≲-refl | no ¬p | _ ∷ w2' = ⊥-elim (¬p refl)
  ≲-diff w1 w2 (≲-step wk) | no ¬p | _ ∷ w2' with ≲-diff w1 w2' wk
  ... | w , w-ok with diffEnv w1 w2' 
  ≲-diff w1 w2 (≲-step wk) | no ¬p | _ ∷ w2' | w , refl | .(just w) = ≲-step w , refl 

  module _ where
    open import Axiom.UniquenessOfIdentityProofs 
    open import Data.Nat
    open import Data.Nat.Properties

    private 
      ≲-length : ∀ w1 w2 -> w1 ≲ w2 -> length w1 ≤ length w2 
      ≲-length _ _ ≲-refl = ≤-refl
      ≲-length _ (_ ∷ w2') (≲-step wk) with ≲-length _ _ wk 
      ... | p = ≤-trans p ( Data.Nat.Properties.n≤1+n (length w2')) 

      lemma : ∀ w w' -> (eq : w ≡ w') -> (wk : w ≲ w') -> subst (λ v → v ≲ w') eq wk ≡ ≲-refl 
      lemma [] _ refl ≲-refl = refl
      lemma (x ∷ w) _ eq ≲-refl with Decidable⇒UIP.≡-irrelevant checkEqEnv eq refl 
      lemma (x ∷ w) .(x ∷ w) .refl ≲-refl | refl = refl

      lemma (x ∷ w) _ refl (≲-step wk) = ⊥-elim (1+n≰n (≲-length _ _ wk))

    ≲-canon : ∀ w1 w2 -> (wk1 wk2 : w1 ≲ w2) -> wk1 ≡ wk2 
    ≲-canon w1 .w1 ≲-refl wk2 with lemma _ _ refl wk2 
    ... | res = sym res
    ≲-canon .(_ ∷ _) .(_ ∷ _) (≲-step wk1) ≲-refl with ≲-length _ _ wk1 
    ... | p =  ⊥-elim (1+n≰n p)
    ≲-canon w1 .(_ ∷ _) (≲-step wk1) (≲-step wk2) rewrite ≲-canon _ _ wk1 wk2 = refl 


data M : Set -> Set where
  Mark : ∀ {Γ τ} -> M (Maybe (DTerm Γ τ)) -> M (Maybe (DTerm Γ τ)) 
  Ret  : ∀ {R} -> R -> M R 
  Bind : ∀ {R S} -> M R -> (R -> M S) -> M S 
  MFix : ∀ {R} -> (R -> M R) -> M R

-- ----

Sem : ∀ A -> Set
Sem A = (w : List Ty) -> M (Maybe (DTerm w A))

MSem : Set -> Set 
MSem R = ∀ (w : List Ty) -> M R

module LamSem where 
  var : ∀ A Γ (w : List Ty) -> Maybe (DTerm w A)
  var A Γ = λ Γ' -> diffEnv Γₐ Γ' >>= λ Γₐ≲Γ' -> just (Var (shift Z Γₐ≲Γ'))
    where
      Γₐ = A ∷ Γ 

  lamSem : (A B : Ty) -> (Sem A -> Sem B) -> Sem (A ~> B)
  lamSem A _ f Γ =  
   Bind (f (λ w -> Ret (var A Γ w)) (A ∷ Γ)) λ t -> Ret (t >>= λ tt -> just (Abs tt))

open LamSem using (lamSem)


appSem : (A B : Ty) -> Sem (A ~> B) -> Sem A -> Sem B 
appSem _ _ e1 e2 Γ = 
  Bind (e1 Γ) λ m1 -> 
  Bind (e2 Γ) λ m2 -> 
   Ret (m1 >>= λ t1 -> m2 >>= λ t2 -> just (App t1 t2))
 
markSem : (A : Ty) -> Sem A -> MSem (Sem A) 
markSem _ e = λ w ->  
   Bind (Mark (e w)) λ m -> 
     Ret (λ w' -> Ret (m >>= λ t -> diffEnv w w' >>= λ w≲w' -> just (shiftExp w≲w' t)))


localSem : (A : Ty) -> MSem (Sem A) -> Sem A 
localSem _ m = λ w -> 
 Bind (m w) λ r -> r w
  
retSem : (R : Set) → R → MSem R
retSem _ x = λ _ -> Ret x -- (just x)

bindSem : (R S : Set) → MSem R → (R → MSem S) → MSem S
bindSem _ _ m k w = 
  Bind (m w) λ r -> k r w

mfixSem : (R : Set) -> (R -> MSem R) -> MSem R 
mfixSem _ f w = MFix (λ r -> f r w)  


convert : ∀ A w -> Poly A -> M (Maybe (DTerm w A))
convert A w h = h _ MSem lamSem appSem markSem localSem retSem bindSem mfixSem w    

postulate 
  Fix : 
    ∀ {A : Set} (f : A -> A) (a : A) -> Set 
  Fix-Step : 
    ∀ {A : Set} (P : A -> Set) {f} {a} 
    -> (Fix f a) -> (∀ x -> P x -> P (f x)) -> P a 

data Eval : ∀ {A} (m : M A) (a : A) -> Set where
  eval-ret : 
    ∀ {A} {a : A} 
    -> Eval (Ret a) a

  eval-bind : 
    ∀ {A B} {m : M A} {f : A -> M B} {a : A} {b : B} 
    -> (Dm  : Eval m          a)
    -> (Dfa : Eval (f a)      b)
    -> Eval (Bind m f) b 

  eval-mark : 
    ∀ {α Γ} {m : M (Maybe (DTerm Γ α))} -- {r : Maybe (DTerm Γ α)} 
    -- -> (Dm : Eval m r)
    -> Eval (Mark m) (just GVar)
    
  eval-mfix : 
    ∀ {A} {f : A -> M A} {g : A -> A} {a : A} 
    -> (Df   : ∀ x -> Eval (f x) (g x))
    -> (Pfix : Fix g a) 
    -> Eval (MFix f) a

data EvalOk : ∀ {A} {m : M A} {a : A} -> (D : Eval m a) -> (P : A -> Set) -> Set where
  eval-ret-ok : 
    ∀ {A} {a : A} {P : A -> Set} 
    -> (Pa : P a) 
    -> EvalOk (eval-ret {a = a}) P 

  eval-bind-ok : 
    ∀ {A B} {m : M A} {f : A -> M B} {a : A} {b : B} {Q : A -> Set} {P : B -> Set}
    -> (Dm  : Eval m a) 
    -> (Dfa : Eval (f a) b) 
    -> (Qm : EvalOk Dm Q) 
    -> (Pfa : Q a -> EvalOk Dfa P) 
    -> EvalOk (eval-bind {m = m} {f = f} Dm Dfa) P 

  eval-mark-ok : 
    ∀ {α Γ} {m : M (Maybe (DTerm Γ α))} {P} 
    -> (h : ∀ {r} -> (d : Eval m r) -> EvalOk d P) 
    -> (PGVar : P (just GVar))
    -> EvalOk (eval-mark {m = m}) P 

  eval-mfix-ok : 
    ∀ {A} {f : A -> M A} {g : A -> A} {a : A} {P} 
    -> (Df   : ∀ x -> Eval (f x) (g x))
    -> (Pfix : Fix g a) 
    -> (Pa : P a) 
    -> EvalOk (eval-mfix {a = a} Df Pfix) P 

okok : ∀ {A} {m : M A} {r : A} {P : A -> Set} -> (d : Eval m r) -> EvalOk d P -> P r 
okok eval-ret (eval-ret-ok Pa) = Pa
okok (eval-bind dm df) (eval-bind-ok .dm .df Qm Pfa) = 
  let Qa =  okok dm Qm 
  in okok df (Pfa Qa) 
okok eval-mark (eval-mark-ok x PGVar) = PGVar
okok (eval-mfix Df Pfix) (eval-mfix-ok .Df .Pfix Pa) = Pa 

prove_by_ : (A : Set) -> A -> A 
prove A by x = x 

non-nothing : ∀ {A} -> Maybe A -> Set 
non-nothing r = ∃[ t ] (r ≡ just t) 

module _ where 
  World = List Ty 

  _≤w_ : World -> World -> Set 
  _≤w_ = _≲_ 
  open KInt  World _≤w_ ≲-trans
  open KInt' World _≤w_ ≲-trans

  open KInt.KRel 

  Good : ∀ {A} -> M A -> (P : A -> Set) -> Set
  Good m P = ∀ r -> (D : Eval m r) -> EvalOk D P 

  MyRel : (A : Ty) -> KRel (Sem A)
  prop (MyRel A) = λ w f -> ∀ w' -> w ≤w w' -> Good (f w') non-nothing -- EvalOk ∞ (f w') (λ x -> ⊤)
  prop-kripke (MyRel _) = λ { w  w' f w≲w' k w'' w'≲w'' -> k w'' (≲-trans w≲w' w'≲w'') }

  MyMRel : ∀ {R} -> KRel R -> KRel (MSem R)
  prop (MyMRel R) = λ w f -> ∀ w' -> w ≤w w' -> Good (f w') (prop R w') -- EvalOk ∞ (f w') (prop R w') 
  prop-kripke (MyMRel _) = λ { w  w' f w≲w' k w'' w'≲w'' -> k w'' (≲-trans w≲w' w'≲w'') }
  

  module _ where 
    private
      var-ok : ∀ {α : Ty} (w₂ w₃ : World) 
               -> (α ∷ w₂) ≲ w₃ -> (r : Maybe (DTerm w₃ α))
               -> (D : Eval (Ret (LamSem.var α w₂ w₃)) r) -> EvalOk D (λ r' -> ∃[ t ] (r' ≡ just t)) 
      var-ok {α} w₂ w₃ wit .(diffEnv (_ ∷ w₂) w₃ >>= (λ Γₐ≲Γ' → just (Var (shift Z Γₐ≲Γ')))) eval-ret 
         with diffEnv (α ∷ w₂) w₃ | ≲-diff (α ∷ w₂) w₃ wit 
      ... | _ | wit' , refl = eval-ret-ok (Var (shift Z wit') , refl)

    lam-ok : ∀ w -> KRel.prop (forallTy λ a -> forallTy λ b -> (MyRel a ~~> MyRel b) ~~> MyRel (a ~> b)) w lamSem
    lam-ok w α β w₁ w≲w₁ h h-ok w₂ w₁≲w₂ _ (eval-bind eval-h eval-ret)
      with h-ok (α ∷ w₂) (≲-step w₁≲w₂) (λ w₃ -> Ret (LamSem.var α w₂ w₃)) (var-ok w₂) (α ∷ w₂) ≲-refl _ eval-h
    ... | eval-h-ok  = eval-bind-ok eval-h eval-ret eval-h-ok λ { (t , refl) -> eval-ret-ok (Abs t , refl) } 

  app-ok : ∀ w -> KRel.prop (forallTy λ a -> forallTy λ b -> MyRel (a ~> b) ~~> MyRel a ~~> MyRel b) w appSem
  app-ok w α β w₁ w≲w₁ e₁ e₁-ok w₂ w₁≲w₂ e₂ e₂-ok w₃ w₂≲w₃ _ (eval-bind eval-e₁ (eval-bind eval-e₂ eval-ret)) 
    with e₁-ok w₃ (≲-trans w₁≲w₂ w₂≲w₃) _ eval-e₁
       | e₂-ok w₃ w₂≲w₃                  _ eval-e₂ 
  ... | p1 | p2 = eval-bind-ok eval-e₁ _        (prove EvalOk eval-e₁ non-nothing by p1) λ { (t₁ , refl) -> 
                  eval-bind-ok eval-e₂ eval-ret (prove EvalOk eval-e₂ non-nothing by p2) λ { (t₂ , refl) -> 
                    prove EvalOk eval-ret non-nothing by eval-ret-ok (App t₁ t₂ , refl)
                  }}  

  mark-ok : ∀ w -> KRel.prop (forallTy λ a -> MyRel a ~~> MyMRel (MyRel a)) w markSem 
  mark-ok w α w₁ w≲w₁ a a-ok w₂ w₁≲w₂ _ (eval-bind eval-mark eval-ret) 
    = eval-bind-ok eval-mark eval-ret (eval-mark-ok (λ eval-aw₂ -> a-ok w₂ w₁≲w₂ _ eval-aw₂) (GVar , refl)) λ { (_ , refl) -> 
      eval-ret-ok lemma}
    where
      lemma : ∀ {α} {t : DTerm w₂ α} -> ∀ w₃ -> w₂ ≲ w₃ -> Good (Ret (diffEnv w₂ w₃ >>= (λ wit -> just (shiftExp wit t)))) non-nothing 
      lemma w₃ w₂≲w₃ with diffEnv w₂ w₃ | ≲-diff w₂ w₃ w₂≲w₃ 
      ... | _ | wit' , refl = λ { _ eval-ret → eval-ret-ok (shiftExp wit' _ , refl) } 

  local-ok : ∀ w -> KRel.prop (forallTy λ a -> MyMRel (MyRel a) ~~> MyRel a) w localSem
  local-ok w α w₁ w≲w₁ a a-ok w₂ w₁≲w₂ r (eval-bind eval-aw₂-f eval-fw₂-r) = 
    eval-bind-ok eval-aw₂-f eval-fw₂-r (prove EvalOk eval-aw₂-f (KRel.prop (MyRel α) w₂) by a-ok w₂ w₁≲w₂ _ eval-aw₂-f) λ P -> 
      prove EvalOk eval-fw₂-r non-nothing 
      by    P w₂ ≲-refl _ eval-fw₂-r 

  ret-ok : ∀ w -> KRel.prop (forallR λ r -> r ~~> MyMRel r) w retSem
  ret-ok w R w₁ w≲w₁ a a-ok w₂ w₁≲w₂ _ eval-ret = eval-ret-ok (prove KRel.prop R w₂ a by KRel.prop-kripke R w₁ w₂ a w₁≲w₂ a-ok) 

  bind-ok : ∀ w -> KRel.prop (forallR λ r -> forallR λ s -> MyMRel r ~~> (r ~~> MyMRel s) ~~> MyMRel s) w bindSem
  bind-ok w R₁ R₂ w₁ w≲w₁ m m-ok w₂ w₁≲w₂ f f-ok w₃ w₂≲w₃ _ (eval-bind eval-mw₃-a eval-faw₃-r) = 
     let P : EvalOk eval-mw₃-a (KRel.prop R₁ w₃) 
         P = m-ok w₃ (≲-trans w₁≲w₂ w₂≲w₃) _ eval-mw₃-a 
     in eval-bind-ok eval-mw₃-a eval-faw₃-r (prove EvalOk eval-mw₃-a (KRel.prop R₁ w₃) by P) λ a-ok -> 
         prove EvalOk eval-faw₃-r (KRel.prop R₂ w₃) 
         by f-ok w₃ w₂≲w₃ _ a-ok w₃ ≲-refl _ eval-faw₃-r

  mfix-ok : ∀ w -> KRel.prop (forallR λ r -> (r ~~> MyMRel r) ~~> MyMRel r) w mfixSem 
  mfix-ok w R w₁ w≲w₁ f f-ok w₂ w₁≲w₂ r (eval-mfix {g = g} Df Pfix) = eval-mfix-ok Df Pfix (Fix-Step (KRel.prop R w₂) Pfix lemma) 
    where
      lemma : ∀ x -> prop R w₂ x -> prop R w₂ (g x) 
      lemma x xok = okok (Df x) (f-ok w₂ w₁≲w₂ x xok w₂ ≲-refl _ (Df x)) 

  module _ (free-theorem : ∀ A w f -> KInt.KRel.prop (m-term A) w f ) where 

    convert-succeeds : ∀ A w -> (h : Poly A) -> ∀ r -> (d : Eval (convert A w h) r) -> EvalOk d non-nothing -- ∃[ t ] (r ≡ just t)
    convert-succeeds A w h =  
      free-theorem A w h MyRel MyMRel w ≲-refl lamSem (lam-ok w)
                                      w ≲-refl appSem (app-ok w) 
                                      w ≲-refl markSem (mark-ok w) 
                                      w ≲-refl localSem (local-ok w) 
                                      w ≲-refl retSem (ret-ok w) 
                                      w ≲-refl bindSem (bind-ok w) 
                                      w ≲-refl mfixSem (mfix-ok w) 
                                      w ≲-refl 
