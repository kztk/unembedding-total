{-# OPTIONS --without-K #-}

open import Level 
open import Data.List
open import Data.List.Properties using (≡-dec; ∷-injectiveʳ)
open import Data.Nat hiding (_⊔_)
open import Data.Nat.Properties 
open import Data.Maybe 

open import Data.Product
open import Data.Sum
open import Data.Empty 
open import Category.Monad using (RawMonad)

open import Relation.Nullary
open import Relation.Binary
open import Relation.Binary.PropositionalEquality hiding (Extensionality)

open import Axiom.UniquenessOfIdentityProofs 

open import Axiom.Extensionality.Propositional using (Extensionality)

open import Ty
open import DeBruijn 
open import Poly

Sem : ∀ A -> Set
Sem A = (w : List Ty) -> Maybe (DTerm w A)

module LamSem where 
  var : ∀ A Γ -> Sem A -- (w : List Ty) -> Maybe (DTerm w A)
  var A Γ = λ Γ' -> diffEnv Γₐ Γ' >>= λ Γₐ≲Γ' -> just (Var (shift Z Γₐ≲Γ'))
    where
      Γₐ = A ∷ Γ 

  lamSem : (A B : Ty) -> (Sem A -> Sem B) -> Sem (A ~> B)
  lamSem A _ f Γ = 
    f (var A Γ) (A ∷ Γ) >>= λ t -> just (Abs t)   

open LamSem using (lamSem)

appSem : (A B : Ty) -> Sem (A ~> B) -> Sem A -> Sem B 
appSem _ _ e1 e2 Γ = 
  e1 Γ >>= λ t1 -> 
  e2 Γ >>= λ t2 -> just (App t1 t2) 

-- ϕ : finaly-tagless representations -> de Bruijn terms 
ϕ : ∀ A -> ∀ w -> 
     ∀ (f : Poly A) -> 
     Maybe (DTerm w A)
ϕ A w f = f Sem lamSem appSem w 


data VEnv (T : Ty -> Set) : (w : List Ty) -> Set where
  εᵥ  : VEnv T [] 
  _∷ᵥ_ : ∀ {A w} -> T A -> VEnv T w -> VEnv T (A ∷ w) 

map-VEnv : ∀ {T T' w} -> (∀ A -> T A -> T' A) -> VEnv T w -> VEnv T' w 
map-VEnv f εᵥ = εᵥ
map-VEnv f (x ∷ᵥ venv) = f _ x ∷ᵥ map-VEnv f venv 

lookup-VEnv : ∀ {T A Γ} -> A ∈ Γ -> VEnv T Γ -> T A 
lookup-VEnv Z (x ∷ᵥ _) = x
lookup-VEnv (S x) (_ ∷ᵥ env) = lookup-VEnv x env 

data VExt T : (w1 w2 : List Ty) -> w1 ≲ w2 -> VEnv T w1 -> VEnv T w2 -> Set where 
  vext-refl : ∀ {w1 venv} -> VExt T w1 w1 ≲-refl venv venv 
  vext-step : ∀ {w1 w2 w1≲w2 venv1 venv2 A a} -> VExt T w1 w2 w1≲w2 venv1 venv2 -> VExt T w1 (A ∷ w2) (≲-step w1≲w2) venv1 (a ∷ᵥ venv2)
 
VExt-trans : ∀ {w1 w2 w3 w1≲w2 w2≲w3 T venv1 venv2 venv3} -> VExt T w1 w2 w1≲w2 venv1 venv2 -> VExt T w2 w3 w2≲w3 venv2 venv3 -> VExt T w1 w3 (≲-trans w1≲w2 w2≲w3) venv1 venv3 
VExt-trans vext-refl rel2 = rel2
VExt-trans (vext-step rel1) vext-refl = vext-step rel1
VExt-trans (vext-step rel1) (vext-step rel2) = vext-step (VExt-trans (vext-step rel1) rel2) 


diff-lookup : ∀ {w w' A T}
              -> (env  : VEnv T (A ∷ w))  
              -> (env' : VEnv T w')
              -> ∀ {wk} (vext : VExt T (A ∷ w) w' wk env env') 
              -> diffEnv (A ∷ w) w' ≡ just wk
              -> lookup-VEnv (shift Z wk) env' ≡ lookup-VEnv Z env
diff-lookup {w} {w'} {A = A} venv venv' vext diff-ok with checkEqEnv (A ∷ w) w' 
diff-lookup {w} {.(A ∷ w)} {A} venv .venv vext-refl refl | yes refl = refl
diff-lookup {w} {w'} {A} venv venv' vext diff-ok | no ¬p with w' 
diff-lookup {w} {w'} {A} venv venv' vext diff-ok | no ¬p | x ∷ w'' with diffEnv (A ∷ w) w'' | inspect (diffEnv (A ∷ w)) w''
diff-lookup {w} {w'} {A} venv .(_ ∷ᵥ _) (vext-step vext) refl | no ¬p | x ∷ w'' | just wk' | [ eq ] = diff-lookup venv _ vext eq 
  

-- ψ is a generalized version of ϕ⁻¹
ψ : ∀ A -> ∀ w -> 
     DTerm w A -> 
     (T : Ty -> Set) -> 
     (∀ A B -> (T A -> T B) -> T (A ~> B)) ->
     (∀ A B -> T (A ~> B) -> T A -> T B) -> 
    VEnv T w -> T A 
ψ A w (Var x) T lam app env = lookup-VEnv x env
ψ A w (App tm₁ tm₂) T lam app env = app _ _ (ψ _ w tm₁ T lam app env) (ψ _ w tm₂ T lam app env)
ψ (A ~> B) w (Abs tm) T lam app env = lam A B (λ v -> ψ _ (A ∷ w) tm T lam app (v ∷ᵥ env))

-- ϕ⁻¹ : de Brujin terms -> finally-Tagless representations 
ϕ⁻¹ : ∀ A -> ∀ tm -> Poly A 
ϕ⁻¹ A tm T lam app = ψ A [] tm T lam app εᵥ


env-ok : ∀ (w : List Ty) -> ∀ (env : VEnv (λ B -> ∀ w' -> Maybe (DTerm w' B)) w) -> Set 
env-ok w env = ∀ {A} (x : A ∈ w) -> ∀ w' -> (wk : w ≲ w') -> 
      ∀ {t} ->  lookup-VEnv x env w' ≡ just t -> t ≡ (Var (shift x wk))

ϕψ : 
  ∀ (A : Ty) w tm env -> 
  env-ok w env -> 
  ∀ {t} -> 
  ψ A w tm Sem lamSem appSem env w ≡ just t -> t ≡ tm 
ϕψ A w (Var x) env envok r = envok x w ≲-refl r 
ϕψ A w (App tm₁ tm₂) env envok r with split-r r 
  where split-r : ∀ {t} -> 
                  appSem _ _ (ψ _ w tm₁ Sem lamSem appSem env) (ψ _ w tm₂ Sem lamSem appSem env) w ≡ just t -> 
                  ∃ λ t₁ -> ∃ λ t₂ -> ψ _ w tm₁ Sem lamSem appSem env w ≡ just t₁ × ψ _ w tm₂ Sem lamSem appSem env w ≡ just t₂ × t ≡ App t₁ t₂
        split-r r with ψ _ w tm₁ Sem lamSem appSem env w | ψ _ w tm₂ Sem lamSem appSem env w
        split-r refl | just t1 | just t2 = t1 , t2 , refl , refl , refl
... | t₁ , t₂ , r₁ , r₂ , eq with ϕψ _ w tm₁ env envok r₁ | ϕψ _ w tm₂ env envok r₂ 
ϕψ A w (App tm₁ tm₂) env envok r | .(tm₁) , .(tm₂) , r₁ , r₂ , eq | refl | refl = eq 
ϕψ (A ~> B) w (Abs tm) env envok r with split-r  r
  where
    h = λ v -> ψ B (A ∷ w) tm Sem lamSem appSem (v ∷ᵥ env)

    split-r : 
      ∀ {t} -> 
      lamSem A B h w ≡ just t 
      -> ∃ λ t' -> h (LamSem.var _ w) (A ∷ w) ≡ just t' × t ≡ Abs t' 
    split-r r with h (LamSem.var _ w) (A ∷ w)
    split-r refl | just t' = t' , refl , refl 
ϕψ (A ~> B) w (Abs tm) env envok r | t , rt , refl 
   with ϕψ B (A ∷ w) tm (v ∷ᵥ env) new-env-ok rt
  where
    h = λ v -> ψ B (A ∷ w) tm Sem lamSem appSem (v ∷ᵥ env)
    v = LamSem.var _ w 
  
    unVar : ∀ {w A a b} -> Var {w} {A} a ≡ Var {w} {A} b -> a ≡ b
    unVar refl = refl 

    lemma-Var : ∀ w w' {A B} -> ∀ (x : B ∈ w)  -> (wk : (A ∷ w) ≲ w') -> 
                Var (shift x (≲-trans (≲-step ≲-refl) wk)) ≡ Var (shift (S x) wk)
    lemma-Var w .(_ ∷ w) x ≲-refl = refl
    lemma-Var w (_ ∷ w') x (≲-step wk) rewrite unVar (lemma-Var _ _ x wk) = refl 
  
    new-env-ok : env-ok (A ∷ w) (v ∷ᵥ env) 
    new-env-ok Z w' wk vok with diffEnv (A ∷ w) w' | split-vok _ vok 
      where
        split-vok : ∀ w' {t} -> v w' ≡ just t -> 
                    ∃ λ w'' -> diffEnv (A ∷ w) w' ≡ just w'' × t ≡ Var (shift Z w'')
        split-vok w' vok with diffEnv (A ∷ w) w' 
        split-vok w' refl | just w'' = w'' , refl , refl 
    ... | just w-diff | w'' , refl , refl with ≲-canon _ _ wk w-diff 
    new-env-ok Z w' .w-diff vok | just w-diff | w-diff , refl , refl | refl = refl 
    new-env-ok (S x) w' wk lookup-ok with envok x w' (≲-trans (≲-step ≲-refl) wk) lookup-ok
    new-env-ok (S x) w' wk lookup-ok | refl = lemma-Var _ _ x wk 
ϕψ (A ~> B) w (Abs .t) env envok r | t , rt , refl | refl = refl 

-- ϕ after ϕ⁻¹ results in a partial identity transformation. 
ϕϕ⁻¹ : ∀ A (tm : DTerm [] A) {t} -> ϕ A [] (ϕ⁻¹ A tm) ≡ just t -> t ≡ tm 
ϕϕ⁻¹ A tm r = ϕψ A [] tm εᵥ empty-env-ok r 
  where
    empty-env-ok : env-ok [] εᵥ
    empty-env-ok () w' wk 



module _ (A : Ty) 
         (f : Poly A)
         (T' : Ty -> Set)
         (lam : ∀ A B -> (T' A -> T' B) -> T' (A ~> B))
         (app : ∀ A B -> T' (A ~> B) -> T' A -> T' B) where

  -- Variable environments with unknown Γ 
  UkVEnv : Set
  UkVEnv = ∃ λ w -> VEnv T' w 

  _≲v_ : UkVEnv -> UkVEnv -> Set
  (w₁ , venv₁) ≲v (w₂ , venv₂) with w₁ ≲? w₂
  ... | yes w₁≲w₂ = VExt T' w₁ w₂ w₁≲w₂ venv₁ venv₂  
  ... | no _      = Lift _ ⊥ 

  ≲v-step : ∀ {venv venv' : UkVEnv} {A} {a : T' A} -> venv ≲v venv' -> venv ≲v (A ∷ proj₁ venv' , a ∷ᵥ proj₂ venv') 
  ≲v-step { (w , venv) } { (w' , venv') } {A} rel with w ≲? w'
  ... | yes wrel with w ≲? (A ∷ w')
  ≲v-step {w , venv} {w' , venv'} {A} rel | yes wrel | yes p with ≲-canon _ _ p (≲-step wrel) 
  ... | refl = vext-step rel
  ≲v-step {w , venv} {w' , venv'} {A} rel | yes wrel | no ¬p = lift (¬p (≲-step wrel)) 
  ≲v-step rel | no  _    = ⊥-elim (lower rel)

  ≲v-refl : ∀ {venv} -> venv ≲v venv 
  ≲v-refl { (w , venv) } with w ≲? w 
  ≲v-refl {w , venv} | yes p with ≲-canon _ _ p ≲-refl
  ... | refl =  vext-refl
  ≲v-refl {w , venv} | no ¬p = lift (¬p ≲-refl) 


  split-w≲w' : ∀ w w' -> w ≲v w' 
                 -> ∃ λ wk -> VExt T' (proj₁ w) (proj₁ w') wk (proj₂ w) (proj₂ w')
  split-w≲w' (w , venv) (w' , venv') w≲w' with w ≲? w' 
  split-w≲w' (w , venv) (w' , venv') w≲w' | yes p = p , w≲w' 


  v-extend : ∀ {A} (a : T' A) -> (w : UkVEnv) -> Σ[ w' ∈  UkVEnv ] w ≲v w' 
  v-extend {A} a (w , venv) =  (A ∷ w , a ∷ᵥ venv) , ≲v-step ≲v-refl  


  ≲v-trans : ∀ { venv₁ venv₂ venv₃ } -> venv₁ ≲v venv₂ -> venv₂ ≲v venv₃ -> venv₁ ≲v venv₃ 
  ≲v-trans { (w₁ , venv₁) } { (w₂ , venv₂) } { (w₃ , venv₃) } 1≲2 2≲3 with w₁ ≲? w₂ 
  ≲v-trans {w₁ , venv₁} {w₂ , venv₂} {w₃ , venv₃} 1≲2 2≲3 | yes w₁≲w₂ with w₂ ≲? w₃ 
  ... | yes w₂≲w₃ with VExt-trans 1≲2 2≲3 
  ... | 1≲3 with w₁ ≲? w₃ 
  ≲v-trans {w₁ , venv₁} {w₂ , venv₂} {w₃ , venv₃} 1≲2 2≲3 | yes w₁≲w₂ | yes w₂≲w₃ | 1≲3 | yes p with ≲-canon _ _ (≲-trans w₁≲w₂ w₂≲w₃) p
  ... | refl = 1≲3
  ≲v-trans {w₁ , venv₁} {w₂ , venv₂} {w₃ , venv₃} 1≲2 2≲3 | yes w₁≲w₂ | yes w₂≲w₃ | 1≲3 | no ¬p = lift (¬p (≲-trans w₁≲w₂ w₂≲w₃)) 

  
  ≲v-is-preorder : IsPreorder _≡_ _≲v_ 
  ≲v-is-preorder = record { isEquivalence = isEquivalence ; reflexive = λ {refl -> ≲v-refl} ; trans = ≲v-trans } 

  VWorld : Preorder Level.zero Level.zero Level.zero
  VWorld = record { Carrier = UkVEnv ; _≈_ = _≡_ ; _∼_ = _≲v_ ; isPreorder = ≲v-is-preorder }

  import KInt 
  open KInt UkVEnv (_≲v_) (≲v-trans)
  
  MyRel2 : (A : Ty) -> KRel₂ (Sem A) (T' A) -- KRel₂ (∀ (w : List Ty) -> Maybe (DTerm w A)) (T' A)
  MyRel2 A = record { prop = λ w x₁ x₂ -> ∀ w' -> w ≲v w' -> ∀ tm -> x₁ (proj₁ w') ≡ just tm -> (ψ A (proj₁ w') tm T' lam app (proj₂ w') ≡ x₂)
                    ; prop-kripke = λ w w' _ _ w≲w' k w'' w'≲w'' -> k w'' (≲v-trans w≲w' w'≲w'') }

  postulate
    -- Binary version. 
    free-theorem₂ : ∀ {ℓ} A w f₁ f₂ -> KInt.KRel₂.prop (KInt.lam-term₂ UkVEnv _≲v_ ≲v-trans {ℓ} A) w f₁ f₂


  -- Lemmas
  split-eq : ∀ A tenv3 te tm₂ -> LamSem.var A tenv3 te ≡ just tm₂ -> 
               ∃ λ wk -> diffEnv (A ∷ tenv3) te ≡ just wk × tm₂ ≡ Var (shift Z wk)
  split-eq A tenv3 te tm eq with diffEnv (A ∷ tenv3) te 
  split-eq A tenv3 te .(Var (shift Z x)) refl | just x = x , refl , refl 

  lemma : ∀ (A : Ty) (tenv3 : List Ty) (venv3 : VEnv T' tenv3) (sx : T' A) -> 
          ∀ (w' : UkVEnv) 
          -> (A ∷ tenv3 , sx ∷ᵥ venv3) ≲v w' 
          -> ∀ tm₂ -> LamSem.var A tenv3 (proj₁ w') ≡ just tm₂ 
          -> ψ A (proj₁ w') tm₂ T' lam app (proj₂ w') ≡ sx
  lemma A tenv3 venv3 sx (te , ve) w≲w' tm eq with split-w≲w' _ _ w≲w'
  ... | wk , vext with split-eq A tenv3 te tm eq
  lemma A tenv3 venv3 sx (te , ve) w≲w' .(Var (shift Z wk')) eq | wk , vext | wk' , eq-d , refl with ≲-canon _ _ wk wk' 
  lemma A tenv3 venv3 sx (te , ve) w≲w' .(Var (shift Z wk')) eq | .wk' , vext | wk' , eq-d , refl | refl 
    with diff-lookup (sx ∷ᵥ venv3) ve vext eq-d 
  lemma A tenv3 venv3 .(lookup-VEnv (shift Z wk') ve) (te , ve) w≲w' .(Var (shift Z wk')) eq | .wk' , vext | wk' , eq-d , refl | refl | refl = refl

  ϕ⁻¹ϕ-lemma : ∀ {tm} -> f Sem lamSem appSem [] ≡ just tm -> ψ A [] tm T' lam app εᵥ ≡ f T' lam app
  ϕ⁻¹ϕ-lemma {tm} eq = free-theorem₂ A w0 f f MyRel2 w0 (≲v-refl {w0}) lamSem lam lamSem-lam w0 (≲v-refl {w0}) appSem app appSem-app w0 (≲v-refl {w0}) tm eq  
    where
      w0 : UkVEnv
      w0 = ([] , εᵥ)

      lamSem-lam : (A B : Ty) -> KRel₂.prop ((MyRel2 A ~~>₂ MyRel2 B) ~~>₂ MyRel2 (A ~> B)) w0 (lamSem A B) (lam A B)
      lamSem-lam A B w2 w0≲w2 t1 s1 k1 w3 w2≲w3 tm r-eq = ll A B w2 w0≲w2 t1 s1 k1 w3 w2≲w3 tm r-eq 
        where
          ll : (A B : Ty) -> KRel₂.prop ((MyRel2 A ~~>₂ MyRel2 B) ~~>₂ MyRel2 (A ~> B)) w0 (lamSem A B) (lam A B)
          ll A B w2 w0≲w2 t1 s1 k1 (tenv3 , venv3) w2≲w3 tm r-eq with t1 (LamSem.var _ tenv3) (A ∷ tenv3) | inspect (t1 (LamSem.var _ tenv3)) (A ∷ tenv3)
          ll A B w2 w0≲w2 t1 s1 k1 (tenv3 , venv3) w2≲w3 .(Abs t) refl | just t | [ t1-eq ] with arg-eq'
            where 
              arg-eq : ∀ {t} -> t1 (LamSem.var _ tenv3) (A ∷ tenv3) ≡ just t ->  ∀ v -> ψ B (A ∷ tenv3) t T' lam app (v ∷ᵥ venv3) ≡ s1 v 
              arg-eq eq sx with t1 (LamSem.var _ tenv3) (A ∷ tenv3) | inspect (t1 (LamSem.var _ tenv3)) (A ∷ tenv3) 
              arg-eq refl sx | just t | [ eq-t ] 
                       = k1 ww (≲v-trans w2≲w3 wk) (LamSem.var _ tenv3) sx (lemma A tenv3 venv3 sx) ww ≲v-refl t eq-t 
                where
                  ww = (A ∷ tenv3 , sx ∷ᵥ venv3)
                  wk : (tenv3 , venv3) ≲v ww
                  wk with tenv3 ≲? (A ∷ tenv3) 
                  wk | yes p with ≲-canon _ _ p (≲-step ≲-refl) 
                  ... | refl =  vext-step vext-refl
                  wk | no ¬p = lift (¬p (≲-step ≲-refl)) 

              arg-eq' : (λ v -> ψ B (A ∷ tenv3) t T' lam app (v ∷ᵥ venv3)) ≡ s1
              arg-eq' = f-ext (arg-eq t1-eq)
                where
                   postulate f-ext : Extensionality _ _ 
          ... |  refl = refl 


      appSem-app : (A B : Ty) -> KRel₂.prop (MyRel2 (A ~> B) ~~>₂ MyRel2 A ~~>₂ MyRel2 B) w0 (appSem A B) (app A B)
      appSem-app A B w2 w0≲w2 fₜ fₛ f-ok w3 w2≲w3 aₜ aₛ a-ok w4 w3≲w4 tm-a r-eq with fₜ (proj₁ w4) | inspect fₜ (proj₁ w4) | aₜ (proj₁ w4) | inspect aₜ (proj₁ w4)
      appSem-app A B w2 w0≲w2 fₜ fₛ f-ok w3 w2≲w3 aₜ aₛ a-ok w4 w3≲w4 _ refl | just tm₁ | [ f-eq ] | just tm₂ | [ a-eq ]  with f-ok w4 (≲v-trans w2≲w3 w3≲w4) tm₁ f-eq
      ... | f-corr with a-ok w4 w3≲w4 tm₂ a-eq 
      ... | a-corr rewrite f-corr | a-corr = refl 

  
ϕ⁻¹ϕ : (A : Ty) 
       -> (f : (T : Ty -> Set) -> 
             (∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
             (∀ A B -> T (A ~> B) -> T A -> T B) -> T A) 
       -> ∀ {tm} -> ϕ A [] f ≡ just tm -> ∀ T' lam app -> ϕ⁻¹ A tm T' lam app ≡ f T' lam app 
ϕ⁻¹ϕ A f eq T' lam app = ϕ⁻¹ϕ-lemma A f T' lam app eq  
        

module _ where 
  World : Preorder Level.zero Level.zero Level.zero 
  World = record { Carrier = List Ty ; _≈_ = _≡_ ; _∼_ = _≲_ ; isPreorder = ≲-is-preorder }


  import KInt 
  open KInt (List Ty) _≲_ ≲-trans


  postulate
    free-theorem : ∀ {ℓ} A w f -> KInt.KRel.prop (KInt.lam-term (List Ty) _≲_ ≲-trans {ℓ} A) w f


  MyRel : (A : Ty) -> KRel (Sem A) -- ((w : List Ty) -> Maybe (DTerm w A))
  MyRel A = record { prop = λ w f -> ∀ w' -> w ≲ w' -> ∃ λ t -> f w' ≡ just t 
                   ; prop-kripke = λ w w' _ w≲w' k w'' w'≲w'' -> k w'' (≲-trans w≲w' w'≲w'') }

  unembed-succeeds : ∀ {A : Ty} (w : List Ty) -> 
                   -- ∀ (f : (T : Ty -> Set) -> 
                   --        (∀ A B -> (T A -> T B) -> T (A ~> B)) -> 
                   --        (∀ A B -> T (A ~> B) -> T A -> T B) -> T A) -> 
                   ∀ (f : Poly A) -> 
                   ∃ λ t -> ϕ A w f ≡ just t
  unembed-succeeds {A} w f = free-theorem A w f MyRel w ≲-refl lamSem lamSem-ok w ≲-refl appSem appSem-ok w ≲-refl 
    where
      lamSem-ok : KRel.prop (forallTy λ a -> forallTy λ b -> (MyRel a ~~> MyRel b) ~~> MyRel (a ~> b)) w lamSem
      lamSem-ok A _ w₁ w≲w₁ h h-ok w₂ w₁≲w₂ with 
        h (LamSem.var _ w₂) (A ∷ w₂) | h-ok (A ∷ w₂) (≲-step w₁≲w₂) (LamSem.var _ w₂ ) (var-ok w₂) (A ∷ w₂) ≲-refl 
        where
          var-ok : ∀ w w' -> (A ∷ w) ≲ w' -> ∃ λ t -> LamSem.var A w w' ≡ just t
          var-ok w w' w≲w' with checkEqEnv (A ∷ w) w' 
          var-ok w .(A ∷ w) w≲w' | yes refl = Var Z , refl
          var-ok w .(A ∷ w) ≲-refl | no ¬p = ⊥-elim (¬p refl)
          var-ok w (_ ∷ w') (≲-step w≲w') | no ¬p with diffEnv (A ∷ w) w' | var-ok w _ w≲w' 
          ... | just A∷w≲w' | _ , refl = Var (S (shift Z A∷w≲w')) , refl 

      ... | .(just arg) | arg , refl = Abs arg , refl 

      appSem-ok : KRel.prop (forallTy λ a -> forallTy λ b -> MyRel (a ~> b) ~~> MyRel a ~~> MyRel b) w appSem
      appSem-ok _ _ w₁ _ e1 e1-ok w₂ w₁≲w₂ e2 e2-ok w₃ w₂≲w₃ with e1 w₃ | e1-ok w₃ (≲-trans w₁≲w₂ w₂≲w₃) 
      ... | _ | t1 , refl with e2 w₃ | e2-ok w₃ w₂≲w₃
      ... | _ | t2 , refl =  App t1 t2 , refl 


